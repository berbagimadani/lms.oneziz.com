$(function() {

    $('#side-menu').metisMenu();

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }

});

//change password validation
function checkOldPass(){
    var dpass = $('#asdf').val();
    var oldpass = $('#oldpass').val();
    if(oldpass.trim() != '') {
        if (md5(oldpass) != dpass) {
            alert('Old password incorrect!');
            $('#oldpass').val('');
        }
    }
}

function beforeSubmitChpass(){
    var n = $('#newpass').val();
    var r = $('#repass').val();
    if(n==r){
        $("#f_chpass").submit()
    }else{
        alert('Confirm password not match !');
    }
}

function beforeSubmitUser(){
    var n = $('#pass').val();
    var r = $('#repass').val();
    if(n==r){
        $("#f_user").submit()
    }else{
        alert('Confirm password not match !');
    }
}

//chain select province & city & school
$(function() {
    $("#city").chained("#province");
});

$(function() {
    $("#school").chained("#city");
});

//chain selected modul siswa
$(function() {
    $("#user_school_city").chained("#user_school_province");
});

$(function() {
    $("#user_school").chained("#user_school_city");
});

//chain selected materi bimbel
$(function() {
    $("#subject_lesson").chained("#subject_course");
});

//chain exam
$(function() {
    $("#subject_exam").chained("#subject_lesson");
});

//chain exam quiz
$(function() {
    $("#subject_exam_ori").chained("#subject_exam");
});

// filter subject
$(function() {
    $("#lesson").chained("#grade");
});