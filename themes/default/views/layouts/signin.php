<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

   <title><?php echo $site_title; ?> | Oneziz LMS</title>

    <link href="<?php echo $this->template->get_theme_path(); ?>/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $this->template->get_theme_path(); ?>/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo $this->template->get_theme_path(); ?>/dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo $this->template->get_theme_path(); ?>/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>

	<?php echo $template['body']; ?>

    <script src="<?php echo $this->template->get_theme_path(); ?>/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="<?php echo $this->template->get_theme_path(); ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="<?php echo $this->template->get_theme_path(); ?>/bower_components/metisMenu/dist/metisMenu.min.js"></script>
	<script src="<?php echo $this->template->get_theme_path(); ?>/dist/js/sb-admin-2.js"></script>

</body>

</html>
