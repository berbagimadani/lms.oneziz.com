<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>LMS | Oneziz</title>

    <link href="<?php echo $this->template->get_theme_path(); ?>/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo $this->template->get_theme_path(); ?>/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo $this->template->get_theme_path(); ?>/dist/css/timeline.css" rel="stylesheet">
	<link href="<?php echo $this->template->get_theme_path(); ?>/dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo $this->template->get_theme_path(); ?>/bower_components/morrisjs/morris.css" rel="stylesheet">
	<link href="<?php echo $this->template->get_theme_path(); ?>/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/bower_components/datepicker/datepicker.css" type="text/css" cache="false" /> 
    <script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/bower_components/tinymce/tinymce/plugins/tiny_mce_wiris/core/display.js"></script>
    <script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/bower_components/tinymce/tinymce/plugins/tiny_mce_wiris/integration/WIRISplugins.js?viewer=image"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>/bower_components/tinymce/tinymce/tinymce.min.js"></script> 

        <script type="text/javascript">
            // Retrieve language from either URL or POST parameters
            function getParameter(name,dflt) {
                var value = new RegExp(name+"=([^&]*)","i").exec(window.location);
                if (value!=null && value.length>1) return decodeURIComponent(value[1].replace(/\+/g,' ')); else return dflt;
            }
            var lang = getParameter("language","");
            if (lang.length==0) lang="en";
        </script>
        
        <style>
            #iconsDiv {display:inline-block;}
            #langFormDiv { display:inline-block; margin-left:435px;}
        </style>
        <script type="text/javascript">
            /*<!--*/
            var dir = 'ltr';
            if (lang == 'ar' || lang == 'he'){
                dir = 'rtl';
            }
            
            tinymce.init({
                mode : "textareas",
                editor_selector : "textarea",
                width : 635,
                height : 200,
                
                language: lang, 
                directionality : dir,
                
                menubar : true,
                
                //plugins: 'tiny_mce_wiris',
                //toolbar: 'code,|,bold,italic,underline,|,cut,copy,paste,|,search,|,undo,redo,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,fontselect,fontsizeselect,|,tiny_mce_wiris_formulaEditor,tiny_mce_wiris_CAS,|,fullscreen',

                plugins:[ 
                    "table advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "tiny_mce_wiris"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | tiny_mce_wiris_formulaEditor,tiny_mce_wiris_CAS"

            });
            
            /*-->*/
        </script>

</head>



<body onload="JavaScript:document.body.focus();" onkeydown="return showKeyCode(event)">

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url();?>/dashboard"><img src="<?php echo $this->template->get_theme_path(); ?>/images/logo.png" alt="Oneziz LMS" height="100%" /></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 1</strong>
                                        <span class="pull-right text-muted">40% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 2</strong>
                                        <span class="pull-right text-muted">20% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                            <span class="sr-only">20% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 3</strong>
                                        <span class="pull-right text-muted">60% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 4</strong>
                                        <span class="pull-right text-muted">80% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Tasks</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <?php echo $this->session->userdata('user_fullname'); ?> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="<?php echo site_url(); ?>/user/edit"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(); ?>/user/chpass"><i class="fa fa-edit fa-fw"></i> Change Password</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url(); ?>/login/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       
                        <li>
                            <a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>						
                        <li>
                            <a href="<?php echo site_url(); ?>/course"><i class="fa fa-book fa-fw"></i> Daftar Paket Bimbel</a>
                        </li>						
                        <li>
                            <a href="<?php echo site_url(); ?>/lesson"><i class="fa fa-book fa-fw"></i> Mata Pelajaran</a>
                        </li>
<!--						
                        <li>
                            <a href="<?php echo site_url(); ?>/teacher"><i class="fa fa-book fa-fw"></i> Pengajar</a>
                        </li>-->
                        <li>
                            <a href="<?php echo site_url(); ?>/student"><i class="fa fa-book fa-fw"></i> Pelajar</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(); ?>/school"><i class="fa fa-book fa-fw"></i> Sekolah</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(); ?>/subject"><i class="fa fa-book fa-fw"></i> Materi Bimbel</a>

                            <ul class="nav nav-second-level collapse">
                                <li><a href="<?php echo site_url(); ?>/subject/browse/slta-ipa">Materi Bimbel SLTA IPA</a></li>
                                <li><a href="<?php echo site_url(); ?>/subject/browse/slta-ips">Materi Bimbel SLTA IPS</a></li>
                                <li><a href="<?php echo site_url(); ?>/subject/browse/smp">Materi Bimbel SMP</a></li>
                            </ul>

                        </li>
                        <li>
							<a href="<?php echo site_url(); ?>/media"><i class="fa fa-gear fa-fw"></i> Media</a>
							<ul class="nav nav-second-level collapse">
                                <li><a href="<?php echo site_url(); ?>/media/sumberdaya">Sumber Daya</a></li>
                                <li><a href="<?php echo site_url(); ?>/media/materi">Materi</a></li>
                            </ul>
						</li> 
                        <li>
							<a href="<?php echo site_url(); ?>/quiz_create"><i class="fa fa-book fa-fw"></i> Kumpulan Soal Latihan</a>
							<ul class="nav nav-second-level collapse">
                                <li><a href="<?php echo site_url(); ?>/quiz_create/browse/slta-ipa">SLTA IPA/MIA</a></li>
                                <li><a href="<?php echo site_url(); ?>/quiz_create/browse/slta-ips">SLTA IPS/IIS</a></li>
                                <li><a href="<?php echo site_url(); ?>/quiz_create/browse/sltp">SLTP</a></li>
                            </ul>
						</li>   
						<li>
                            <?php 
                                $question_a = getUriCompare($_SERVER[REQUEST_URI], array(1) );
                                $qa = null;
                                $in = null;
                                if ( $question_a == 'quiz_questionslta-ips' || $question_a == 'quiz_questionslta-ipa' || $question_a == 'quiz_questionsltp'){
                                    $qa = 'active';
                                    $in = 'in';
                                }
                            ?>
							<a href="<?php echo site_url(); ?>/quiz_question" class="<?php echo $qa; ?>"><i class="fa fa-book fa-fw"></i> Pembuatan Soal Latihan</a>
							<ul class="nav nav-second-level collapse <?php echo $in; ?>">
                                <li><a href="<?php echo site_url(); ?>/quiz_question/browse/slta-ipa" class="<?php if ( $question_a == 'quiz_questionslta-ipa') { echo 'active'; }?>">SLTA IPA/MIA</a></li>
                                <li><a href="<?php echo site_url(); ?>/quiz_question/browse/slta-ips" class="<?php if ( $question_a == 'quiz_questionslta-ips') { echo 'active'; }?>">SLTA IPS/IIS</a></li>
                                <li><a href="<?php echo site_url(); ?>/quiz_question/browse/sltp" class="<?php if ( $question_a == 'quiz_questionsltp') { echo 'active'; }?>">SLTP</a></li>
                            </ul>
						</li>      					
                        
                        <li>
                            <a href="<?php echo site_url(); ?>/jwbquiz"><i class="fa fa-book fa-fw"></i> Soal Jawab</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(); ?>/exam"><i class="fa fa-book fa-fw"></i> Ujian</a>
                        </li> 
                        <li>
                            <a href="<?php echo site_url(); ?>/formexam"><i class="fa fa-book fa-fw"></i> Form Exam</a>
                        </li> 
						<li>
							<a href="<?php echo site_url(); ?>/order"><i class="fa fa-book fa-fw"></i> Pendaftaran</a>
							<ul class="nav nav-second-level collapse">
                                <li><a href="<?php echo site_url(); ?>/order/slta">SLTA</a></li>
                                <li><a href="<?php echo site_url(); ?>/order/sltp">SLTP</a></li>
                            </ul>
						</li> 		
						<li>
							<a href="<?php echo site_url(); ?>/news"><i class="fa fa-book fa-fw"></i> Pengumuman</a>
							<ul class="nav nav-second-level collapse">
                                <li><a href="<?php echo site_url(); ?>/news/slta">SLTA</a></li>
                                <li><a href="<?php echo site_url(); ?>/news/sltp">SLTP</a></li>
                            </ul>
						</li> 	
						<li> 
							<a href="<?php echo site_url(); ?>/report"><i class="fa fa-gear fa-fw"></i> Laporan</a>
							<ul class="nav nav-second-level collapse">
                                <li><a href="<?php echo site_url(); ?>/report/">Laporan Daftar Pelajar</a></li>
                                <li><a href="<?php echo site_url(); ?>/reportexam/">Laporan Daftar Ujian</a></li>
                            </ul>
						</li>			
						<li>
							<a href="<?php echo site_url(); ?>/setting"><i class="fa fa-gear fa-fw"></i> Pengaturan</a>
							<ul class="nav nav-second-level collapse">
                                <li><a href="<?php echo site_url(); ?>/environment/province">Pengaturan Provinsi</a></li>
                                <li><a href="<?php echo site_url(); ?>/environment/city">Pengaturan Kota</a></li>
                                <li><a href="<?php echo site_url(); ?>/environment/school">Pengaturan Tingkat Pendidikan</a></li>
                                <li><a href="<?php echo site_url(); ?>/user">Pengaturan Pemakai</a></li>
                                <li><a href="<?php echo site_url(); ?>/usergroup">Pengaturan Kelompok Pemakai</a></li>
                            </ul>
						</li>
						
                        <li>
                            <a href="<?php echo site_url(); ?>/finance"><i class="fa fa-money fa-fw"></i> Transaksi Keuangan</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(); ?>/help"><i class="fa fa-flag fa-fw"></i> Bantuan</a>
							
                        </li>	
                        
                    </ul>
					
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
			<?php echo $template['body']; ?>
        </div>
        <!-- /#page-wrapper -->

    </div>
    
	<script src="<?php echo $this->template->get_theme_path(); ?>/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="<?php echo $this->template->get_theme_path(); ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
     <script src="<?php echo $this->template->get_theme_path(); ?>/bower_components/datepicker/bootstrap-datepicker.js" cache="false"></script> 

	<script src="<?php echo $this->template->get_theme_path(); ?>/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript 
    <script src="<?php echo $this->template->get_theme_path(); ?>/bower_components/raphael/raphael-min.js"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>/bower_components/morrisjs/morris.min.js"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>/js/morris-data.js"></script>
-->
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo $this->template->get_theme_path(); ?>/dist/js/sb-admin-2.js"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>/dist/js/md5.min.js"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>/js/jquery.chained.min.js"></script>
 
    

	<script>
		function confirmDialog() {
			return confirm("Are you sure you want to delete this record?")
		}
	</script>

    <script type="text/javascript">
        $('.datepicker').datepicker()
    </script>

    <!-- firts true or false -->
    <script type="text/javascript"> 
        $(document).ready(function() { 
            
            $('#quiz_next').hide();

            $('input[id="answer_jwbquiz"]').click(function() { 

                if (confirm("Apakah anda yakin dengan jawaban "+$(this).val()+ "?" )) {
        
                 var ans = $(this).val();
                 var quiz_id = $('#quiz_id').val();
                 //alert(ans);
                 
                    $.ajax({
                        url: '<?php echo site_url(); ?>/jwbquiz/answer',
                        type: 'POST',
                        data: { answer: ans, quiz_id:quiz_id},
                        success: function(resp){ 
                            //console.log("Log >> " + resp);
                            $('input[name$="answer"]').attr('disabled', true);
                            $('#output_answer').html(resp);
                            $('#output_answer_end').html(' Jawaban Anda '+ans);
                        }
                    }); 

                    $('#quiz_next').show();

                }
                return false;


            });
         });
    </script>

    <script type="text/javascript"> 
        $(document).ready(function() { 

            $('input[id="answer_exam"]').change(function() { 

                var ans = $(this).attr('data');
                var quiz_id = $(this).attr('content');
                var key = $(this).attr('key');

                //if (confirm("Apakah anda yakin dengan jawaban "+quiz_id+ "?" )) {
                 //alert(ans);
                 
                    $.ajax({
                        url: '<?php echo site_url(); ?>/exam/answer',
                        type: 'POST',
                        data: { answer: ans, quiz_id:quiz_id, key:key},
                        success: function(resp){ 
                            //console.log("Log >> " + resp);
                            $($this).attr('disabled', true);
                            $('#output_answer').html(resp);
                            $('#output_answer_end').html(' Jawaban Anda '+ans);
                        }
                    }); 

                    $('#quiz_next').show();

               // }
                return false;


            });
         });
    </script>


</body>

</html>
