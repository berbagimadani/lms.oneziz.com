	 <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
				 
				<div class="logo-login"><img src="<?php echo $this->template->get_theme_path(); ?>/images/logo.png" alt="Oneziz LMS"  /></div>
				
				<?php if(validation_errors()){ ?>
				<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo validation_errors(); ?></div>
				<?php } ?>
				<?php if($this->session->flashdata('message')){ ?>
					<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo $this->session->flashdata('message'); ?></div>
				<?php } ?>
				
				<div class="login-panel panel panel-default">
					
                    <div class="panel-heading">
                        <h3 class="panel-title">Reset Password</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" action="">							
                            <fieldset class="text-center">
								<p><?php echo $msg; ?></p>
                                <br/>
								<a href="http://lms.oneziz.com" title="Kembali ke LMS Oneziz">Kembali ke LMS Oneziz</a>
                            </fieldset>
                        </form>
                    </div>
                </div>
				<p class="text-center"> <small>&copy; <?php echo date("Y");?>. Oneziz LMS. All Rights Reserved</small></p>
            </div>
        </div>
    </div>