
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							
							<div class="form-group">
								<label for="lesson_name" class="col-sm-3 control-label">Nama Mata Pelajaran *</label>
								<div class="col-sm-4">
									<input type="text" name="lesson_name" class="form-control" id="" placeholder="" value="<?php echo set_value('lesson_name', isset($data->lesson_name) ? $data->lesson_name : ''); ?>" />
								</div>
							</div>
							
							<div class="form-group">
								<label for="lesson_course" class="col-sm-3 control-label">Paket Bimbel *</label>
								<div class="col-sm-4">
									<select name="lesson_course" class="form-control" id="" >
										<?php if(!$data->lesson_course_id){ ?><option value="">Pilih Paket Bimbel</option><?php } ?>
										<?php foreach($course as $row):  ?>
											<option value="<?php echo $row->course_id; ?>" <?php if($data->lesson_course_id == $row->course_id) { echo "selected";} ?> ><?php echo $row->course_name; ?></option>
										<?php endforeach; ?>										
									</select>
								</div>
							</div>
						
							
							<div class="form-group">
								<label for="lesson_description" class="col-sm-3 control-label">Deskripsi Pelajaran</label>
								<div class="col-sm-6">
									<textarea name="lesson_description" class="form-control" id="" rows="5" ><?php echo preg_replace('#<br\s*/?>#i', "", $data->lesson_description);; ?></textarea>
								</div>
							</div>
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<input type="hidden" name="lesson_id" class="form-control" id="" placeholder="" value="<?php echo set_value('lesson_id', isset($data->lesson_id) ? $data->lesson_id : ''); ?>">
							<button type="submit" class="btn btn-success">Simpan Data</button>
						</div>
					</div>
				</div>
			</div>
		</form>
