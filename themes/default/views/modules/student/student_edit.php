

<div class="row">
	<div class="col-lg-12">
		<h2 class="page-header"><?php echo $menu_title; ?></h2>
	</div>
</div>

<form class="form-horizontal" enctype="multipart/form-data" id="f_user" action="" method="post">
	<div class="panel panel-default">

		<!-- Panel Head -->
		<div class="panel-heading">
			<!-- Nav tabs -->
			<ul class="nav nav-pills">
				<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
			</ul>
		</div>

		<!-- Panel Body -->
		<div class="panel-body">
			<!-- Tab panes -->
			<div class="tab-content">
				<div class="tab-pane active" id="personalinfo">
					<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>

					<div class="form-group">
						<label for="customer_company" class="col-sm-3 control-label">Nama Lengkap Pelajar*</label>
						<div class="col-sm-4">
							<input type="text" name="fullname" class="form-control" id="" required="" placeholder="" value="<?php echo set_value('fullname', isset($data->fullname) ? $data->fullname : ''); ?>" />
						</div>
					</div>

					<div class="form-group">
						<label for="customer_email" class="col-sm-3 control-label">Email *</label>
						<div class="col-sm-4">
							<input type="email" name="user_email" class="form-control" id=""  required="" placeholder="" value="<?php echo set_value('user_email', isset($data->user_email) ? $data->user_email : ''); ?>"  />
						</div>
					</div>

					<div class="form-group">
						<label for="customer_mobile" class="col-sm-3 control-label">No HP</label>
						<div class="col-sm-4">
							<input type="text" name="user_mobile" class="form-control" id="" placeholder="" value="<?php echo set_value('user_mobile', isset($data->user_mobile) ? $data->user_mobile : ''); ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="customer_mobile" class="col-sm-3 control-label">No Telp</label>
						<div class="col-sm-4">
							<input type="text" name="user_phone" class="form-control" id="" placeholder="" value="<?php echo set_value('user_phone', isset($data->user_phone) ? $data->user_phone : ''); ?>" />
						</div>
					</div>
					<hr/>
					<div class="form-group">
						<label for="" class="col-sm-3 control-label">Provinsi</label>
						<div class="col-sm-4">
							<select name="user_province" id="province" class="form-control">
								<?php if(!$data->user_provinve){ ?><option value="">Pilih Provinsi</option><?php } ?>
								<?php foreach($province as $row):  ?>
									<option class="<?php echo $row->province_id; ?>" value="<?php echo $row->province_id; ?>" <?php if($data->user_province == $row->province_id) { echo "selected";} ?> ><?php echo $row->province_name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-3 control-label">Kota</label>
						<div class="col-sm-4">
							<select name="user_city" id="city" class="form-control">
								<?php if(!$data->user_city){ ?><option value="">Pilih Kota</option><?php } ?>
								<?php foreach($city as $row):  ?>
									<option class="<?php echo $row->city_province_id; ?>" value="<?php echo $row->city_id; ?>" <?php if($data->user_city == $row->city_id) { echo "selected";} ?> ><?php echo $row->city_name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="customer_address" class="col-sm-3 control-label">Alamat</label>
						<div class="col-sm-4">
							<textarea name="user_address" class="form-control" ><?php echo $data->user_address; ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="customer_zipcode" class="col-sm-3 control-label">Kodepos</label>
						<div class="col-sm-4">
							<input type="text" name="user_zipcode" class="form-control" maxlength="5" id="" placeholder="" value="<?php echo set_value('user_zipcode', isset($data->user_zipcode) ? $data->user_zipcode : ''); ?>" />
						</div>
					</div>
					<hr/>

					<div class="form-group">
						<label for="customer_phone" class="col-sm-3 control-label">Tingkatan Pendidikan</label>
						<div class="col-sm-4">
							<select name="user_school_grade" id="user_school_grade" class="form-control">
								<?php if(!$data->user_school_grade){ ?><option value="0">Pilih Tingkatan Pendidikan</option><?php } ?>
								<?php foreach($school_grade as $row):  ?>
									<option class="<?php echo $row->school_grade_id; ?>" value="<?php echo $row->school_grade_id; ?>" <?php if($data->user_school_grade == $row->school_grade_id) { echo "selected";} ?> ><?php echo $row->school_grade_name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-3 control-label">Provinsi Alamat Sekolah</label>
						<div class="col-sm-4">
							<select name="user_school_province" id="user_school_province" class="form-control">
								<?php if(!$user_school_province){ ?><option value="">Pilih Provinsi</option><?php } ?>
								<?php foreach($province as $row):  ?>
									<option class="<?php echo $row->province_id; ?>" value="<?php echo $row->province_id; ?>" <?php if($user_school_province == $row->province_id) { echo "selected";} ?> ><?php echo $row->province_name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-3 control-label">Kota Alamat Sekolah</label>
						<div class="col-sm-4">
							<select name="user_school_city" id="user_school_city" class="form-control">
								<?php if(!$user_school_city){ ?><option value="">Pilih Kota</option><?php } ?>
								<?php foreach($city as $row):  ?>
									<option class="<?php echo $row->city_province_id; ?>" value="<?php echo $row->city_id; ?>" <?php if($user_school_city == $row->city_id) { echo "selected";} ?> ><?php echo $row->city_name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="customer_phone" class="col-sm-3 control-label">Nama Sekolah</label>
						<div class="col-sm-4">
							<select name="user_school" id="user_school"  class="form-control">
								<?php if(!$data->user_school){ ?><option value="">Pilih Sekolah</option><?php } ?>
								<?php foreach($school as $row):  ?>
									<option  class="<?php echo $row->school_city; ?>" value="<?php echo $row->school_id; ?>" <?php if($data->user_school == $row->school_id) { echo "selected";} ?> ><?php echo $row->school_name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<hr/>

					<div class="form-group">
						<label for="" class="col-sm-3 control-label">Nama Orang Tua</label>
						<div class="col-sm-4">
							<input type="text" name="user_parent_name" class="form-control" id="" required="" placeholder="" value="<?php echo set_value('user_parent_name', isset($data->user_parent_name) ? $data->user_parent_name : ''); ?>" />
						</div>
					</div>

					<div class="form-group">
						<label for="" class="col-sm-3 control-label">No HP Orang Tua</label>
						<div class="col-sm-4">
							<input type="text" name="user_parent_mobile" class="form-control" id="" required="" placeholder="" value="<?php echo set_value('user_parent_mobile', isset($data->user_parent_mobile) ? $data->user_parent_mobile : ''); ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-3 control-label">Alamat Email Orang Tua</label>
						<div class="col-sm-4">
							<input type="text" name="user_parent_email" class="form-control" id="" required="" placeholder="" value="<?php echo set_value('user_parent_email', isset($data->user_parent_email) ? $data->user_parent_email : ''); ?>" />
						</div>
					</div>

					<!--<div class="form-group">
								<label for="customer_remark" class="col-sm-3 control-label">Remark</label>
								<div class="col-sm-4">
									<textarea name="customer_remark" class="form-control" ><?php /*echo $data->customer_remark; */?></textarea>
								</div>
							</div>-->


				</div>


			</div>

		</div>

		<!-- Panel Footer -->
		<div class="panel-footer">
			<div class="form-group">
				<div class="col-sm-3 ">
					<input type="hidden" name="user_id" class="form-control" id="" placeholder="" value="<?php echo set_value('user_id', isset($data->user_id) ? $data->user_id : ''); ?>">
					<button type="submit" class="btn btn-success">Simpan Data</button>
				</div>
			</div>
		</div>
	</div>

</form>