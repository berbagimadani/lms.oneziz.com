
	
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="f_user" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							<div class="form-group">
								<label for="customer_company" class="col-sm-3 control-label">Nama Sekolah *</label>
								<div class="col-sm-4">
									<input type="text" name="school_name" class="form-control" id="" required="" placeholder="" value="<?php echo set_value('school_name', isset($data->school_name) ? $data->school_name : ''); ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Province *</label>
								<div class="col-sm-4">
									<select name="school_province" id="province" class="form-control">
										<?php if(!$data->school_provinve){ ?><option value="">Select Province</option><?php } ?>
										<?php foreach($province as $row):  ?>
											<option class="<?php echo $row->province_id; ?>" value="<?php echo $row->province_id; ?>" <?php if($data->school_province == $row->province_id) { echo "selected";} ?> ><?php echo $row->province_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">City *</label>
								<div class="col-sm-4">
									<select name="school_city" id="city" class="form-control">
										<?php if(!$data->school_city){ ?><option value="">Select City</option><?php } ?>
										<?php foreach($city as $row):  ?>
											<option class="<?php echo $row->city_province_id; ?>" value="<?php echo $row->city_id; ?>" <?php if($data->school_city == $row->city_id) { echo "selected";} ?> ><?php echo $row->city_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="customer_address" class="col-sm-3 control-label">Alamat *</label>
								<div class="col-sm-4">
									<textarea name="school_address" class="form-control" ><?php echo $data->school_address; ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="customer_phone" class="col-sm-3 control-label">Nama Kepala Sekolah </label>
								<div class="col-sm-4">
									<input type="text" name="school_head_name" class="form-control" id="" required="" placeholder="" value="<?php echo set_value('school_head_name', isset($data->school_head_name) ? $data->school_head_name : ''); ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_email" class="col-sm-3 control-label">Email Kepala Sekolah </label>
								<div class="col-sm-4">
									<input type="text" name="school_head_email" class="form-control" id=""  required="" placeholder="" value="<?php echo set_value('school_head_email', isset($data->school_head_email) ? $data->school_head_email : ''); ?>"  />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_mobile" class="col-sm-3 control-label">Handphone Kepala Sekolah</label>
								<div class="col-sm-4">
									<input type="text" name="school_head_phone" class="form-control" id="" placeholder="" value="<?php echo set_value('school_head_phone', isset($data->school_head_phone) ? $data->school_head_phone : ''); ?>" />
								</div>
							</div>

						</div>		
						
						
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<input type="hidden" name="school_id" class="form-control" id="" placeholder="" value="<?php echo set_value('school_id', isset($data->school_id) ? $data->school_id : ''); ?>">
							<button type="submit" class="btn btn-success">Simpan Data</button>
						</div>
					</div>
				</div>
			</div>
			
		</form>