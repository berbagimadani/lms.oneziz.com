<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
	<li><a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-home"></i> Home</a></li>
	<li><a href="<?php echo site_url(); ?>/customer">Customer</a></li>
</ul>

<section class="main">
	<div class="container">
	
		<div id="module_title">
			<div class="m-b-md"><h3 class="m-b-none"><?php echo $menu_title; ?></h3></div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							
							<div class="form-group">
								<label for="customer_company" class="col-sm-3 control-label">Company Name</label>
								<div class="col-sm-4">
									<input type="text" name="customer_company" class="form-control" id="" placeholder="" value="<?php echo set_value('customer_company', isset($data->customer_company) ? $data->customer_company : ''); ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_name" class="col-sm-3 control-label">Supplier Name</label>
								<div class="col-sm-4">
									<input type="text" name="customer_name" class="form-control" id="" placeholder="" value="<?php echo set_value('customer_name', isset($data->customer_name) ? $data->customer_name : ''); ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_email" class="col-sm-3 control-label">Email</label>
								<div class="col-sm-4">
									<input type="text" name="customer_email" class="form-control" id="" placeholder="" value="<?php echo set_value('customer_email', isset($data->customer_email) ? $data->customer_email : ''); ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_phone" class="col-sm-3 control-label">Phone</label>
								<div class="col-sm-4">
									<input type="text" name="customer_phone" class="form-control" id="" placeholder="" value="<?php echo set_value('customer_phone', isset($data->customer_phone) ? $data->customer_phone : ''); ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_mobile" class="col-sm-3 control-label">Mobile</label>
								<div class="col-sm-4">
									<input type="text" name="customer_mobile" class="form-control" id="" placeholder="" value="<?php echo set_value('customer_mobile', isset($data->customer_mobile) ? $data->customer_mobile : ''); ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_fax" class="col-sm-3 control-label">Fax</label>
								<div class="col-sm-4">
									<input type="text" name="customer_fax" class="form-control" id="" placeholder="" value="<?php echo set_value('customer_fax', isset($data->customer_fax) ? $data->customer_fax : ''); ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_address" class="col-sm-3 control-label">Address</label>
								<div class="col-sm-4">									
									<textarea name="customer_address" class="form-control" readonly ><?php echo $data->customer_address; ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="customer_city" class="col-sm-3 control-label">City</label>
								<div class="col-sm-4">
									<input type="text" name="customer_city" class="form-control" id="" placeholder="" value="<?php echo set_value('customer_city', isset($data->customer_city) ? $data->customer_city : ''); ?>" readonly  />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_state" class="col-sm-3 control-label">State</label>
								<div class="col-sm-4">
									<input type="text" name="customer_state" class="form-control" id="" placeholder="" value="<?php echo set_value('customer_state', isset($data->customer_state) ? $data->customer_state : ''); ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_country" class="col-sm-3 control-label">Country</label>
								<div class="col-sm-4">
									<input type="text" name="customer_country" class="form-control" id="" placeholder="" value="<?php echo set_value('customer_country', isset($data->customer_country) ? $data->customer_country : ''); ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_zipcode" class="col-sm-3 control-label">Zipcode</label>
								<div class="col-sm-4">
									<input type="text" name="customer_zipcode" class="form-control" id="" placeholder="" value="<?php echo set_value('customer_zipcode', isset($data->customer_zipcode) ? $data->customer_zipcode : ''); ?>" readonly />
								</div>
							</div>
							
							<div class="form-group">
								<label for="customer_remark" class="col-sm-3 control-label">Remark</label>
								<div class="col-sm-4">
									<textarea name="customer_remark" class="form-control" readonly ><?php echo $data->customer_remark; ?></textarea>
								</div>
							</div>
							
							
						</div>		
						
						
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<a href="<?php echo site_url()."/customer/browse"; ?>" class="btn btn-primary"><i class="fa  fa-chevron-left icon"> </i> Back to Customer List</a>
						</div>
					</div>
				</div>
			</div>
			
			
			

			
			
			
		</form>
	</div>
</div>	