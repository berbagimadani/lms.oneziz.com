		

		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							 
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Nama Materi *</label>
								<div class="col-sm-4">
									<select name="quiz_subject_id" class="form-control" id="" >
										<?php if(!$data->quiz_subject_id){ ?><option value="">Pilih Nama Materi</option><?php } ?>
										<?php foreach($subject as $row):  ?>
											<option value="<?php echo $row->subject_id; ?>" <?php if($data->quiz_subject_id == $row->subject_id) { echo "selected";} ?> ><?php echo $row->subject_name; ?></option>
										<?php endforeach; ?>										
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="quiz_question" class="col-sm-3 control-label">Kuis Pertanyaan *</label>
								<div class="col-sm-6">
									<textarea name="quiz_question" class="form-control textarea" rows="5"><?php echo set_value('quiz_question', isset($data->quiz_question) ? $data->quiz_question : ''); ?></textarea> 
								</div>
							</div> 

							<div class="form-group">
								<label for="quiz_answer_a" class="col-sm-3 control-label">Jawaban A *</label>
								<div class="col-sm-6">
									<textarea name="quiz_answer_a" class="form-control textarea" rows="5"><?php echo set_value('quiz_answer_a', isset($data->quiz_answer_a) ? $data->quiz_answer_a : ''); ?></textarea> 
								</div>
							</div>
							<div class="form-group">
								<label for="quiz_answer_b" class="col-sm-3 control-label">Jawaban B *</label>
								<div class="col-sm-6">
									<textarea name="quiz_answer_b" class="form-control textarea" rows="5"><?php echo set_value('quiz_answer_b', isset($data->quiz_answer_b) ? $data->quiz_answer_b : ''); ?></textarea> 
								</div>
							</div>
							<div class="form-group">
								<label for="quiz_answer_c" class="col-sm-3 control-label">Jawaban C *</label>
								<div class="col-sm-6">
									<textarea name="quiz_answer_c" class="form-control textarea" rows="5"><?php echo set_value('quiz_answer_c', isset($data->quiz_answer_c) ? $data->quiz_answer_c : ''); ?></textarea> 
								</div>
							</div>
							<div class="form-group">
								<label for="quiz_answer_d" class="col-sm-3 control-label">Jawaban D *</label>
								<div class="col-sm-6">
									<textarea name="quiz_answer_d" class="form-control textarea" rows="5"><?php echo set_value('quiz_answer_d', isset($data->quiz_answer_d) ? $data->quiz_answer_d : ''); ?></textarea> 
								</div>
							</div>
							<div class="form-group">
								<label for="quiz_answer_e" class="col-sm-3 control-label">Jawaban E *</label>
								<div class="col-sm-6">
									<textarea name="quiz_answer_e" class="form-control textarea" rows="5"><?php echo set_value('quiz_answer_e', isset($data->quiz_answer_e) ? $data->quiz_answer_e : ''); ?></textarea> 
								</div>
							</div>


							<div class="form-group">
								<label for="quiz_answer" class="col-sm-3 control-label">Jawaban Benar *</label>
								<div class="col-sm-6">
									
									<label>A</label><input type="radio" value="a" name="quiz_answer" <?php if($data->quiz_answer == 'a') { echo 'checked=""';} ?>>
									
									<label>B</label><input type="radio" value="b" name="quiz_answer" <?php if($data->quiz_answer == 'b') { echo 'checked=""';} ?>>
									<label>C</label> <input type="radio" value="c" name="quiz_answer" <?php if($data->quiz_answer == 'c') { echo 'checked=""';} ?>>
									<label>D</label> <input type="radio" value="d" name="quiz_answer" <?php if($data->quiz_answer == 'd') { echo 'checked=""';} ?>>
									<label>E</label> <input type="radio" value="e" name="quiz_answer" <?php if($data->quiz_answer == 'e') { echo 'checked=""';} ?>>
								</div>
							</div>

							<div class="form-group">
								<label for="quiz_answer_e" class="col-sm-3 control-label">Pembahasan</label>
								<div class="col-sm-6">
									<textarea name="quiz_solution" class="form-control textarea" rows="5"><?php echo set_value('quiz_solution', isset($data->quiz_solution) ? $data->quiz_solution : ''); ?></textarea> 
								</div>
							</div>
							 
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<input type="hidden" name="quiz_id" class="form-control" id="" placeholder="" value="<?php echo set_value('quiz_id', isset($data->quiz_id) ? $data->quiz_id : ''); ?>">
							<button type="submit" class="btn btn-success">Save Data</button>
						</div>
					</div>
				</div>
			</div>
		</form>
