 
    <!-- Bootstrap PDF Viewer -->
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>bower_components/pdfjs/css/bootstrap-pdf-viewer.css">
  

    <!-- Bootstrap PDF Viewer -->
    <div id="viewer" class="pdf-viewer" data-url="<?php echo base_url()."media/material/".$filename ?>"></div>

    <!-- jQuery -->
    <script src="<?php echo $this->template->get_theme_path(); ?>bower_components/pdfjs/lib/jquery-1.9.1.js"></script>
 

    <!-- Bootstrap PDF Viewer -->
    <script src="<?php echo $this->template->get_theme_path(); ?>bower_components/pdfjs/lib/pdf.js"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>bower_components/pdfjs/js/bootstrap-pdf-viewer.js"></script>

    <script>
      var viewer;

      $(function() {
        viewer = new PDFViewer($('#viewer'));
      });
    </script>
 
