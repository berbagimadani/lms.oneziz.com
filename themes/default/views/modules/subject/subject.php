
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header"><?php echo $menu_title; ?></h2>
        </div>
    </div>
			
	<?php if($this->session->flashdata('message')){ ?>
			<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
	<?php } ?>
		
	<section class="panel panel-default">
			<!-- TABLE HEADER -->
			<div class="panel-heading">Daftar Materi Bimbel
			
				
				
			</div>
			<div class="panel-body">

			
				<div class="row">
					<div class="col-lg-4">				
						<?php if(!$this->ion_auth->is_siswa()){ ?>
							<a href="<?php echo site_url();?>/subject/create" title="" class="btn btn-success">Tambah Data Baru</a><br/><br/>
						<?php } ?>
					</div>
					<div class="col-lg-8 text-right">
						<form enctype="multipart/form-data" action="" method="post"> 				
							<select name="grade" class="form-controls" id="grade">
								<?php if(!$q_grade){ ?><option value="">Pilih Tingkat Pendidikan</option><?php } ?> 
								<?php foreach($grade as $row):  ?>
									<option  class="<?php echo $row->course_id; ?>" value="<?php echo $row->course_id; ?>" <?php if($q_grade == $row->course_id) { echo "selected";} ?> ><?php echo $row->school_grade_name; ?></option>
								<?php endforeach; ?>
							</select>
							<select name="lesson" class="form-controls" id="lesson">
								<?php if(!$q_lesson){ ?><option value="">Mata Pelajaran</option><?php } ?> 
								<?php foreach($lesson as $row):  ?>
									<option class="<?php echo $row->lesson_course_id; ?>" value="<?php echo $row->lesson_course_id; ?>" <?php if($q_lesson == $row->lesson_course_id) { echo "selected";} ?> ><?php echo $row->lesson_name; ?></option>
								<?php endforeach; ?>
								</select>
								<button type="submit" class="">Filter</button>
							
						</form>
					</div>
			</div>
			
			<div class="table-responsive">  
				
				<table class="table table-striped table-bordered table-hover">              
					<thead>                  
					  <tr>
						<th>Tingkatan Pendidikan</th>
						<th>Mata Pelajaran</th>
						<th>Bab</th>
						<th>Judul Bab</th>
						<th>Sub Bab</th>
						<th>Judul Sub Bab</th>
						<th>Subject Code</th>
						<th>Jenis Materi</th>
						<?php if($this->ion_auth->is_siswa()){ ?>
						<th width="80px" class="text-center">Belajar</th>
						<?php } else{ ?>
						<th width="80px" class="text-center">Ubah/Hapus</th>
						<?php } ?>
					  </tr>                  
					</thead> 
					<tbody>	
					<?php
						$total_rows=$config['total_rows'];
						if(empty($no)){ 
							$no=1; 
							$nostart=1;
							$noend=$config['per_page'];
							if($noend>$total_rows){ $noend=$total_rows; }
						}else{ 
							$no=$no+1;
							$nostart=$no;
							$noend=$nostart+$config['per_page']-1;
							if($noend>$total_rows){ $noend=$total_rows; }
						} 
					?>
					<?php foreach($data as $row):  ?>
						<tr>     
							<td><?php echo $row->school_grade_name; ?></td>
							<td><?php echo $row->lesson_name; ?></td>
							<td><?php echo $row->subject_section_no; ?></td>
							<td><?php echo $row->subject_section_name; ?></td>
							<td><?php echo $row->subject_subsection_no; ?></td>
							<td><?php echo $row->subject_subsection_name; ?></td>
							<td><?php echo $row->subject_code; ?></td>

							<!--<td><?php echo $row->lesson_name; ?></td>-->

							<td><?php echo $row->subject_category; ?></td>		
							<td class="text-center">
								<?php if($this->ion_auth->is_siswa()){ ?>
<!--									<a href="--><?php //echo base_url()."themes/default/js/viewerjs/source/#../../../../../media/material/".$row->subject_material; ?><!--" title="Materi" target="_blank"><i class="fa fa-book"></i></a>-->
									<a href="<?php echo site_url()."/subject/viewer/".$row->subject_id; ?>" title="Materi"><i class="fa fa-book"></i></a>
								<?php }else{ ?>
<!--									<a href="--><?php //echo base_url()."themes/default/js/viewerjs/source/#../../../../../media/material/".$row->subject_material; ?><!--" title="Materi" target="_blank"><i class="fa fa-book"></i></a>-->
									<a href="<?php echo site_url()."/subject/viewer/".$row->subject_id; ?>" title="Materi"><i class="fa fa-book"></i></a>
									<a href="<?php echo site_url()."/subject/edit/".$row->subject_id; ?>" title="Ubah"><i class="fa fa-pencil"></i></a>
								<a href="<?php echo site_url()."/subject/delete/".$row->subject_id; ?>" title="Hapus" onclick="return confirmDialog();" ><i class="fa fa-trash-o"></i></a>
								<?php } ?>
							</td>
							
						</tr>
					<?php $no++; endforeach; ?>
					</tbody>	
				</table>  
			</div>
			<footer class="panel-footer">
				<div class="row">
					<div class="col-sm-4 text-left"> 
						<?php if($total_rows != 0){ ?>
						<small class="text-muted inline m-t-sm m-b-sm"> Ditunjukkan  <?php echo $nostart; ?>-<?php echo $noend; ?> dari <?php echo $config['total_rows']; ?> data</small>
						<?php }else{ ?>
						<small class="text-muted inline m-t-sm m-b-sm"> Data tidak ditemukan</small>
						<?php } ?>
					</div>
					<div class="col-sm-5 text-right text-center-xs pull-right">
						<ul class="pagination pagination-sm m-t-none m-b-none">
							<?php echo $this->pagination->create_links(); ?>
						</ul>
					</div>
				</div>
			</footer>
		</div>
	</section>