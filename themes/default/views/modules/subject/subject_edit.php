
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							
							<!--
							<div class="form-group">
								<label for="subject_name" class="col-sm-3 control-label">Nama Materi *</label>
								<div class="col-sm-4">
									<input type="text" name="subject_name" class="form-control" id="" placeholder="" value="<?php echo set_value('subject_name', isset($data->subject_name) ? $data->subject_name : ''); ?>" />
								</div>
							</div>
							-->

							<div class="form-group">
								<label for="subject_name" class="col-sm-3 control-label">Bab *</label>
								<div class="col-sm-4">
									<input type="text" name="subject_section_no" class="form-control" id="" placeholder="" value="<?php echo set_value('subject_section_no', isset($data->subject_section_no) ? $data->subject_section_no : ''); ?>" />
								</div>
							</div>

							<div class="form-group">
								<label for="subject_name" class="col-sm-3 control-label">Judul Bab *</label>
								<div class="col-sm-4">
									<input type="text" name="subject_section_name" class="form-control" id="" placeholder="" value="<?php echo set_value('subject_section_name', isset($data->subject_section_name) ? $data->subject_section_name : ''); ?>" />
								</div>
							</div>

							<div class="form-group">
								<label for="subject_name" class="col-sm-3 control-label">Sub Bab *</label>
								<div class="col-sm-4">
									<input type="text" name="subject_subsection_no" class="form-control" id="" placeholder="" value="<?php echo set_value('subject_subsection_no', isset($data->subject_subsection_no) ? $data->subject_subsection_no : ''); ?>" />
								</div>
							</div>

							<div class="form-group">
								<label for="subject_name" class="col-sm-3 control-label">Judul Sub Bab *</label>
								<div class="col-sm-4">
									<input type="text" name="subject_subsection_name" class="form-control" id="" placeholder="" value="<?php echo set_value('subject_subsection_name', isset($data->subject_subsection_name) ? $data->subject_subsection_name : ''); ?>" />
								</div>
							</div>

							<div class="form-group">
								<label for="subject_name" class="col-sm-3 control-label">Subject Code *</label>
								<div class="col-sm-4">
									<input type="text" name="subject_code" class="form-control" id="" placeholder="" value="<?php echo set_value('subject_code', isset($data->subject_code) ? $data->subject_code : ''); ?>" />
								</div>
							</div>

							<div class="form-group">
								<label for="subject_lesson" class="col-sm-3 control-label">Paket Bimbel *</label>
								<div class="col-sm-4">
									<select name="subject_course" class="form-control" id="subject_course" >
										<?php if(!$q_course_id){ ?><option value="">Pilih Paket Bimbel</option><?php } ?>
										<?php foreach($course as $row):  ?>
											<option value="<?php echo $row->course_id; ?>" <?php if($q_course_id == $row->course_id) { echo "selected";} ?> ><?php echo $row->course_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="subject_lesson" class="col-sm-3 control-label">Mata Pelajaran *</label>
								<div class="col-sm-4">
									<select name="subject_lesson" class="form-control" id="subject_lesson" >
										<?php if(!$data->subject_lesson_id){ ?><option value="">Pilih Mata Pelajaran</option><?php } ?>
										<?php foreach($lesson as $row):  ?>
											<option class="<?php echo $row->lesson_course_id; ?>" value="<?php echo $row->lesson_id; ?>" <?php if($data->subject_lesson_id == $row->lesson_id) { echo "selected";} ?> ><?php echo $row->lesson_name; ?></option>
										<?php endforeach; ?>										
									</select>
								</div>
							</div>
						
							<div class="form-group">
								<label for="subject_category" class="col-sm-3 control-label">Jenis Materi</label>
								<div class="col-sm-4">
									<select name="subject_category" class="form-control" id="" >
										<?php if(!$data->subject_lesson_id){ ?><option value="">Pilih Jenis Materi</option><?php } ?>
											<option value="Materi" <?php if($data->subject_category == "Materi") { echo "selected";} ?> >Materi</option>
											<option value="Latihan Soal Materi" <?php if($data->subject_category == "Latihan Soal Materi") { echo "selected";} ?> >Latihan Soal Materi</option>
											<option value="Latihan Soal Akhir Materi" <?php if($data->subject_category == "Latihan Soal Akhir Materi") { echo "selected";} ?> >Latihan Soal Akhir Materi</option>
											<option value="Latihan Soal UAN" <?php if($data->subject_category == "Latihan Soal UAN") { echo "selected";} ?> >Latihan Soal UAN</option>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="subject_description" class="col-sm-3 control-label">Deskripsi Materi</label>
								<div class="col-sm-6">
									<textarea name="subject_description" class="form-control" id="" rows="5" ><?php echo preg_replace('#<br\s*/?>#i', "", $data->subject_description);; ?></textarea>
								</div>
							</div>
							
							<div class="form-group">
								<label for="subject_material" class="col-sm-3 control-label">File Materi (.pdf) *</label>
								<div class="col-sm-4">
									<input type="file" name="subject_material" class="form-control"  />
								</div>
							</div>
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<input type="hidden" name="subject_id" class="form-control" id="" placeholder="" value="<?php echo set_value('subject_id', isset($data->subject_id) ? $data->subject_id : ''); ?>">
							<button type="submit" class="btn btn-success">Simpan Data</button>
						</div>
					</div>
				</div>
			</div>
		</form>
