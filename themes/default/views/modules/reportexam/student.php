
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header"><?php echo $menu_title; ?></h2>
        </div>
    </div>
			
	<?php if($this->session->flashdata('message')){ ?>
			<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
	<?php } ?>
		
	<section class="panel panel-default">
			<!-- TABLE HEADER -->
			<div class="panel-heading">Daftar Pelajar</div>
			<div class="panel-body">

			<form enctype="multipart/form-data" action="" method="post">

				<?php if($tipe == 'province' || $tipe == 'all'){ ?>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Provinsi</label>
					<div class="col-sm-4">
						<select name="province" id="province"  class="form-control">
							<?php if(!$q_provinve){ ?><option value="">Pilih Provinsi</option><?php } ?>
							<!--<option --><?php //if($q_province == $row->province_id) { echo "selected";} ?><!-- value="">All Province</option>-->
							<?php foreach($province as $row):  ?>
								<option  class="<?php echo $row->province_id; ?>" value="<?php echo $row->province_id; ?>" <?php if($q_province == $row->province_id) { echo "selected";} ?> ><?php echo $row->province_name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<br/><br/><br/>
				<?php }
				if($tipe=='city' || $tipe == 'all'){
				?>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Kota</label>
					<div class="col-sm-4">
						<select name="city" id="city" class="form-control">
							<?php if(!$q_city){ ?><option value="">Pilih Kota</option><?php } ?>
							<!--<option --><?php //if($q_city == $row->city_id) { echo "selected";} ?><!-- value="">All City</option>-->
							<?php foreach($city as $row):  ?>
								<option  class="<?php echo $row->city_province_id; ?>" value="<?php echo $row->city_id; ?>" <?php if($q_city == $row->city_id) { echo "selected";} ?> ><?php echo $row->city_name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<?php } ?>
				<br/><br/>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Sekolah</label>
					<div class="col-sm-4">
						<select name="school" id="school"  class="form-control">
							<?php if(!$q_school){ ?><option value="">Pilih Sekolah</option><?php } ?>
							<?php foreach($school as $row):  ?>
								<option  class="<?php echo $row->school_city; ?>" value="<?php echo $row->school_id; ?>" <?php if($q_school == $row->school_id) { echo "selected";} ?> ><?php echo $row->school_name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<br/><br/>

				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Paket Bimbel</label>
					<div class="col-sm-4">
						<select name="paket" id="paket"  class="form-control">
							<?php if(!$q_paket){ ?><option value="">Pilih Paket BImbel</option><?php } ?>
							<?php foreach($paket as $row):  ?>
								<option  class="<?php echo $row->subject_id; ?>" value="<?php echo $row->subject_id; ?>" <?php if($q_paket == $row->subject_id) { echo "selected";} ?> ><?php echo $row->subject_name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<br><br>

				<div class="form-group">
					<label for="news_content" class="col-sm-2 control-label">Score</label>
					<div class="col-sm-2">
						<input type="text" name="score_start" class="form-control" value="<?php echo set_value('score_start', isset($q_score_start) ? $q_score_start : ''); ?>" placeholder="Score Start" />
					</div> 
					<div class="col-sm-2">
						<input type="text" name="score_end" class="form-control" value="<?php echo set_value('score_end', isset($q_score_end) ? $q_score_end : ''); ?>" placeholder="Score End" />
					</div> 
				</div>
				
				<br/><br/>
				<div class="form-group">
					<div class="col-sm-3 ">
						<button type="submit" class="btn btn-success">Filter</button>
					</div>
				</div>
				<br/><br/>

			</form>

			<div class="table-responsive">
				
				<table class="table table-striped table-bordered table-hover">              
					<thead>                  
					  <tr>
						<th width="30px">No</th>
						<th>Nama Lengkap</th>
						<th>Alamat Tempat Tinggal</th> 
						<th>Kota Tempat Tinggal</th>
						<th>Nama Sekolah</th> 
						<th>Kota Sekolah</th>
						<th>Tingkatan Pendidikan</th>
						<th>Mata Pelajaran</th>  
						<th>Nama Quiz</th>  
						<th>Nilai</th>
					  </tr>                  
					</thead> 
					<tbody>	
					<?php 

						$total_rows=$config['total_rows'];
						if(empty($no)){ 
							$no=1; 
							$nostart=1;
							$noend=$config['per_page'];
							if($noend>$total_rows){ $noend=$total_rows; }
						}else{ 
							$no=$no+1;
							$nostart=$no;
							$noend=$nostart+$config['per_page']-1;
							if($noend>$total_rows){ $noend=$total_rows; }
						} 
					?>
					<?php foreach($user as $row):  ?>
						<tr>     
							<td align="center"><?php echo $no; ?></td>
							<td><?php echo $row->fullname; ?></td>
							<td><?php echo $row->user_address; ?></td> 
							<td><?php echo $row->city_name; ?></td> 
							<td><?php echo $row->school_name; ?></td> 
							<td><?php echo $row->city_name; ?></td> 
							<td><?php echo $row->school_grade_name; ?></td> 
							<td><?php echo $row->subject_name; ?></td>
							<td>

							<?php //echo "Soal Jawab ".$row->subject_name; ?> 
							<?php $name = $this->subject_model->get_subject( $row->subject_id )->result(); //echo $name[0]->subject_lesson_id; ?>
							 
							<?php $name2 = $this->lesson_model->get_lesson(  $name[0]->subject_lesson_id )->result(); echo $name2[0]->lesson_name; ?>

							</td>
							<td><?php echo $row->score_value; ?></td>
							
							
						</tr>
					<?php $no++; endforeach; ?>
					</tbody>	
				</table>  
			</div>
			<footer class="panel-footer">
				<div class="row">
					<div class="col-sm-4 text-left"> 
						<!--
						<?php if($total_rows != 0){ ?>
						<small class="text-muted inline m-t-sm m-b-sm"> Ditunjukkan  <?php echo $nostart; ?>-<?php echo $noend; ?> dari <?php echo $config['total_rows']; ?> data</small>
						<?php }else{ ?>
						<small class="text-muted inline m-t-sm m-b-sm"> Data tidak ditemukan</small>
						<?php } ?>-->
					</div>
					<div class="col-sm-5 text-right text-center-xs pull-right">
						<ul class="pagination pagination-sm m-t-none m-b-none">
							<?php echo $this->pagination->create_links(); ?>
						</ul>
					</div>
				</div>
			</footer>
			<br/>
			<div class="row">
				<div class="col-md-2">
					<form enctype="multipart/form-data" method="post" action="<?php echo site_url();?>/reportexam/download_excel">
						<input type="hidden" name="report_city" value="<?php echo $this->input->post('city'); ?>" />
						<input type="hidden" name="report_province" value="<?php echo $this->input->post('province'); ?>" />
						<input type="hidden" name="report_school" value="<?php echo $this->input->post('school'); ?>" />
						<input type="hidden" name="report_score_start" value="<?php echo $this->input->post('score_start'); ?>" />
						<input type="hidden" name="report_score_end" value="<?php echo $this->input->post('score_end'); ?>" />
						<input type="submit" class="btn btn-primary" value="Download Excel" />
					</form>
				</div>
				<div class="col-md-2">
					<form target="_blank" enctype="multipart/form-data" method="post" action="<?php echo site_url();?>/reportexam/download_pdf">
						<input type="hidden" name="report_city" value="<?php echo $this->input->post('city'); ?>" />
						<input type="hidden" name="report_province" value="<?php echo $this->input->post('province'); ?>" />
						<input type="hidden" name="report_school" value="<?php echo $this->input->post('school'); ?>" />
						<input type="hidden" name="report_score_start" value="<?php echo $this->input->post('score_start'); ?>" />
						<input type="hidden" name="report_score_end" value="<?php echo $this->input->post('score_end'); ?>" />
						<input type="submit" class="btn btn-primary" value="Download PDF" />
					</form>
				</div>
				
			</div>
		</div>
	</section>