<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
	<li><a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-home"></i> Home</a></li>
	<li><a href="<?php echo site_url(); ?>/report/student">Report Pelajar</a></li>
</ul>

<div class="main">
	<div class="container">
	
		<div id="module_title">
			<div class="m-b-md"><h3 class="m-b-none"><?php echo $menu_title; ?></h3></div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							
							<div class="form-group">
								<label for="fullname" class="col-sm-3 control-label">Nama Lengkap Pelajar</label>
								<div class="col-sm-4">
									<input type="text" name="fullname" class="form-control" id="" placeholder="" value="<?php echo set_value('fullname', isset($data->fullname) ? $data->fullname : ''); ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label for="username" class="col-sm-3 control-label">Username</label>
								<div class="col-sm-4">
									<input type="text" name="username" class="form-control" id="" placeholder="" value="<?php echo set_value('username', isset($data->username) ? $data->username : ''); ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label for="user_email" class="col-sm-3 control-label">Email</label>
								<div class="col-sm-4">
									<input type="text" name="user_email" class="form-control" id="" placeholder="" value="<?php echo set_value('user_email', isset($data->user_email) ? $data->user_email : ''); ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label for="user_phone" class="col-sm-3 control-label">Nama Orang Tua</label>
								<div class="col-sm-4">
									<input type="text" name="user_parent_name" class="form-control" id="" placeholder="" value="<?php echo set_value('user_parent_name', isset($data->user_parent_name) ? $data->user_parent_name : ''); ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label for="user_phone" class="col-sm-3 control-label">No Telepon</label>
								<div class="col-sm-4">
									<input type="text" name="user_phone" class="form-control" id="" placeholder="" value="<?php echo set_value('user_phone', isset($data->user_phone) ? $data->user_phone : ''); ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label for="user_mobile" class="col-sm-3 control-label">No HP</label>
								<div class="col-sm-4">
									<input type="text" name="user_mobile" class="form-control" id="" placeholder="" value="<?php echo set_value('user_mobile', isset($data->user_mobile) ? $data->user_mobile : ''); ?>" readonly />
								</div>
							</div>
							<!--<div class="form-group">
								<label for="customer_fax" class="col-sm-3 control-label">Fax</label>
								<div class="col-sm-4">
									<input type="text" name="customer_fax" class="form-control" id="" placeholder="" value="<?php /*echo set_value('customer_fax', isset($data->customer_fax) ? $data->customer_fax : ''); */?>" readonly />
								</div>
							</div>-->
							<div class="form-group">
								<label for="customer_address" class="col-sm-3 control-label">Alamat</label>
								<div class="col-sm-4">									
									<textarea name="user_address" class="form-control" readonly ><?php echo $data->user_address; ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="user_city" class="col-sm-3 control-label">Kota</label>
								<div class="col-sm-4">
									<input type="text" name="user_city" class="form-control" id="" placeholder="" value="<?php echo $data->city_name; ?>" readonly  />
								</div>
							</div>
							<div class="form-group">
								<label for="user_provinve" class="col-sm-3 control-label">Provinsi</label>
								<div class="col-sm-4">
									<input type="text" name="user_provinve" class="form-control" id="" placeholder="" value="<?php echo $data->province_name; ?>" readonly />
								</div>
							</div>
							<!--<div class="form-group">
								<label for="customer_country" class="col-sm-3 control-label">Country</label>
								<div class="col-sm-4">
									<input type="text" name="customer_country" class="form-control" id="" placeholder="" value="<?php /*echo set_value('customer_country', isset($data->customer_country) ? $data->customer_country : ''); */?>" readonly />
								</div>
							</div>-->
							<div class="form-group">
								<label for="user_zipcode" class="col-sm-3 control-label">Kodepos</label>
								<div class="col-sm-4">
									<input type="text" name="user_zipcode" class="form-control" id="" placeholder="" value="<?php echo set_value('user_zipcode', isset($data->user_zipcode) ? $data->user_zipcode : ''); ?>" readonly />
								</div>
							</div>
							
							<!--<div class="form-group">
								<label for="customer_remark" class="col-sm-3 control-label">Remark</label>
								<div class="col-sm-4">
									<textarea name="customer_remark" class="form-control" readonly ><?php /*echo $data->customer_remark; */?></textarea>
								</div>
							</div>-->
							
							
						</div>		
						
						
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<a href="<?php echo site_url()."/report/student"; ?>" class="btn btn-primary"><i class="fa  fa-chevron-left icon"> </i> Back to Report Pelajar</a>
						</div>
					</div>
				</div>
			</div>
			
			
			

			
			
			
		</form>
	</div>
</div>	