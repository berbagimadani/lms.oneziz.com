
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header"><?php echo $menu_title; ?></h2>
        </div>
    </div>
			
	<?php if($this->session->flashdata('message')){ ?>
			<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
	<?php } ?>
		
	<section class="panel panel-default">
			<!-- TABLE HEADER -->
			<div class="panel-heading"><?php echo $submenu_title; ?></div>
			<div class="panel-body">
			
			<div class="table-responsive">  
				
				<table class="table table-striped table-bordered table-hover">              
					<thead>                  
					  <tr>
						<th width="30px">No </th>
						<th>Pertanyaan</th>
						<th>Jawaban A</th>
						<th>Jawaban B</th>
						<th>Jawaban C</th>
						<th>Jawaban D</th>
						<th>Jawaban E</th>
						<th width="80px" class="text-center">Jawaban Benar</th>
						<th>Ulasan Jawaban</th>
						<th width="80px">Ubah / Hapus</th>
					  </tr>                  
					</thead> 
					<tbody>	
					<?php
						$total_rows=$config['total_rows'];
						if(empty($no)){ 
							$no=1; 
							$nostart=1;
							$noend=$config['per_page'];
							if($noend>$total_rows){ $noend=$total_rows; }
						}else{ 
							$no=$no+1;
							$nostart=$no;
							$noend=$nostart+$config['per_page']-1;
							if($noend>$total_rows){ $noend=$total_rows; }
						} 
					?>
					<?php 
						foreach($data as $row): 
					?>
						<tr>     
							<td align="center"><?php echo $no; ?></td>
							<td><?php echo $row->quiz_question; ?></td>	
							<td><?php echo $row->quiz_answer_a; ?></td>	
							<td><?php echo $row->quiz_answer_b; ?></td>	
							<td><?php echo $row->quiz_answer_c; ?></td>	
							<td><?php echo $row->quiz_answer_d; ?></td>	
							<td><?php echo $row->quiz_answer_e; ?></td>	
							<td class="text-center"><?php echo strtoupper($row->quiz_answer); ?></td> 
							<td><?php echo $row->quiz_solution; ?></td>	
							<td class="text-center">
								<a href="<?php echo site_url()."/quiz/edit/".$row->quiz_id; ?>" title="Ubah"><i class="fa fa-pencil"></i></a>
								<a href="<?php echo site_url()."/quiz/delete/".$row->quiz_id; ?>" title="Hapus" onclick="return confirmDialog();" ><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
					<?php $no++; endforeach; ?>
					</tbody>	
				</table>  
			</div>
			<footer class="panel-footer">
				<div class="row">
					<div class="col-sm-4 text-left"> 
						<?php if($total_rows != 0){ ?>
						<small class="text-muted inline m-t-sm m-b-sm"> Ditunjukkan  <?php echo $nostart; ?>-<?php echo $noend; ?> dari <?php echo $config['total_rows']; ?> data</small>
						<?php }else{ ?>
						<small class="text-muted inline m-t-sm m-b-sm"> Data tidak ditemukan</small>
						<?php } ?>
					</div>
					<div class="col-sm-5 text-right text-center-xs pull-right">
						<ul class="pagination pagination-sm m-t-none m-b-none">
							<?php echo $this->pagination->create_links(); ?>
						</ul>
					</div>
				</div>
			</footer>
		</div>
	</section>