
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header"><?php echo $total_rows.' Pertanyaan dari soal '.$data[0]->subject_name; ?></h2>
        </div>
    </div> 
		
	<section class="panel panel-default">
			<!-- TABLE HEADER -->
			<div class="panel-heading">

			Quiz &nbsp; <label> <div id="output_answer"></div></label>


			</div>
			<div class="panel-body"> 
			<div class="table-responsive">  
				
				<table class="table table-striped table-bordered table-hover">              
					<thead>                  
					  <tr>  
						<th>Pertanyaan</th>
					  </tr>                 
					</thead> 
					<tbody>	
					<?php
						$total_rows=$config['total_rows'];
						if(empty($no)){ 
							$no=1; 
							$nostart=1;
							$noend=$config['per_page'];
							if($noend>$total_rows){ $noend=$total_rows; }
						}else{ 
							$no=$no+1;
							$nostart=$no;
							$noend=$nostart+$config['per_page']-1;
							if($noend>$total_rows){ $noend=$total_rows; }
						} 
					?>
					<?php foreach($data as $row):  ?>
						<tr>
							<td>
								<?php echo $row->quiz_question; ?>
							</td>
						</tr>
						<tr>       
							<td> 
								<form method="post" action="#" id="button">
								<input type="hidden" value="<?php echo $row->playquiz_quiz_id; ?>" name="quiz_id" id="quiz_id">
								<ul>

									<li>A <input type="radio" value="a" name="answer" id="answer_jwbquiz"> <?php echo $row->quiz_answer_a; ?></li>
									<li>B <input type="radio" value="b" name="answer" id="answer_jwbquiz"><?php echo $row->quiz_answer_b; ?></li>
									<li>C <input type="radio" value="c" name="answer" id="answer_jwbquiz"><?php echo $row->quiz_answer_c; ?></li>
									<li>D <input type="radio" value="d" name="answer" id="answer_jwbquiz"><?php echo $row->quiz_answer_d; ?></li>
									<li>E <input type="radio" value="e" name="answer" id="answer_jwbquiz"><?php echo $row->quiz_answer_e; ?></li>

								</ul>  
        
								</form> 
							</td>	 
						</tr>
					<?php $no++; endforeach; ?>
					</tbody>	
				</table>  
			</div>
			<footer class="panel-footer">
				<div class="row">
					 
					<div class="col-sm-5 text-right text-center-xs pull-right">
						<a href="" id="quiz_next">NEXT</a>
					</div>
				</div>
			</footer>
		</div>
	</section>

	
