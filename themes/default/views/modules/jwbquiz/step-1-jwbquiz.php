	
		 
		
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>

		<div class="table-responsive">  
				
				<table class="table table-striped table-bordered table-hover">              
					<thead>                  
					  <tr>
						<th width="30px">No</th>
						<th width="180px">Jenis Quiz</th> 
						<th width="180px">Quiz Name</th> 
						<th width="80px" class="text-center">Action</th>
					  </tr>                  
					</thead> 
					<tbody>	
					<?php
						$total_rows=$config['total_rows'];
						if(empty($no)){ 
							$no=1; 
							$nostart=1;
							$noend=$config['per_page'];
							if($noend>$total_rows){ $noend=$total_rows; }
						}else{ 
							$no=$no+1;
							$nostart=$no;
							$noend=$nostart+$config['per_page']-1;
							if($noend>$total_rows){ $noend=$total_rows; }
						} 
					?>
					<?php foreach($subject as $row):  ?>
						<tr>     
							<td align="center"><?php echo $no; ?></td>
							<td>
							<?php echo $row->subject_name; ?>
							<?php 
							echo ' ( '.$this->jwbquiz_model->count_quiz($row->subject_id).' ) ';
							?>
							</td>
							<td>
								<?php $name = $this->lesson_model->get_lesson( $row->subject_lesson_id )->result(); echo $name[0]->lesson_name; ?>
							</td> 
							<td class="text-center">
								<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
									<input type="hidden" name="random" value="<?php echo $key; ?>" />
									<input type="hidden" name="quiz" value="<?php echo $row->subject_id; ?>" />
									<?php if($this->jwbquiz_model->count_quiz($row->subject_id) > 0 ){ ?>
										<button type="submit" class="btn btn-success">Start</button>
									<?php } ?>
								</form>
							</td>
						</tr>
					<?php $no++; endforeach; ?>
					</tbody>	
				</table>  
			</div> 

		 
