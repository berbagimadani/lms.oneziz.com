
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Nilai <?php echo $total_rows.' Pertanyaan dari soal '.$data[0]->subject_name;; ?></h2>
        </div>
    </div> 
		
	<section class="panel panel-default">
			<!-- TABLE HEADER -->
			<div class="panel-heading">

			Quiz &nbsp; <label> <div id="output_answer"></div></label>

			</div>
			<div class="panel-body"> 
			<div class="table-responsive">  
				
				<table class="table table-striped table-bordered table-hover">              
					<thead>                  
					  <tr>  
						<th>Result</th>
					  </tr>                 
					</thead> 
					<tbody>	  
						<tr>
							<td>
								Jawaban Benar : <?php echo $true; ?>
							</td>
						</tr>  
						<tr>
							<td>
								Jawaban Salah : <?php echo $false; ?>
							</td>
						</tr>  
					</tbody>	
				</table>  
			</div>
			<footer class="panel-footer">
				<div class="row">
					 
					<div class="col-sm-5 text-right text-center-xs pull-right">
						<a href="" id="quiz_next">NEXT</a>
					</div>
				</div>
			</footer>
		</div>
	</section>

	
