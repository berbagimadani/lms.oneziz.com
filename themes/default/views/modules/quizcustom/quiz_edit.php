		

		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							 
							<div class="form-group">
								<label for="quiz_question" class="col-sm-3 control-label">Custom Correct *</label>
								<div class="col-sm-6">
									<textarea name="quiz_custom_correct" class="form-control textarea" rows="5"><?php echo set_value('quiz_custom_correct', isset($data->quiz_custom_correct) ? $data->quiz_custom_correct : ''); ?></textarea>
								</div>
							</div> 

							<div class="form-group">
								<label for="quiz_answer_a" class="col-sm-3 control-label">Custom Wrong *</label>
								<div class="col-sm-6">
									<textarea name="quiz_custom_wrong" class="form-control textarea" rows="5"><?php echo set_value('quiz_custom_wrong', isset($data->quiz_custom_wrong) ? $data->quiz_custom_wrong : ''); ?></textarea>
								</div>
							</div>

							<div class="form-group">
								<label for="quiz_answer" class="col-sm-3 control-label">Status *</label>
								<div class="col-sm-6">
									
									<label>Active  </label> <input type="radio" value="1" name="quiz_custom_status" <?php if($data->quiz_custom_status == '1') { echo 'checked=""';} ?>>
									
									<label>Not Active  </label> <input type="radio" value="0" name="quiz_custom_status" <?php if($data->quiz_custom_status == '0') { echo 'checked=""';} ?>>
								</div>
							</div>


					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<input type="hidden" name="quiz_custom_id" class="form-control" id="" placeholder="" value="<?php echo set_value('quiz_custom_id', isset($data->quiz_custom_id) ? $data->quiz_custom_id : ''); ?>">
							<button type="submit" class="btn btn-success">Save Data</button>
						</div>
					</div>
				</div>
			</div>
		</form>
