
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header"><?php echo $menu_title; ?></h2>
        </div>
    </div>
			
	<?php if($this->session->flashdata('message')){ ?>
			<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
	<?php } ?>
		
	<section class="panel panel-default">
			<!-- TABLE HEADER -->
			<div class="panel-heading">Daftar Custom Quiz</div>
			<div class="panel-body">
			
			<a href="<?php echo site_url();?>/quizcustom/create" title="" class="btn btn-success">Tambah Custom Quiz</a><br/><br/>
			<div class="table-responsive">  
				
				<table class="table table-striped table-bordered table-hover">              
					<thead>                  
					  <tr>
						<th width="30px">No</th>
						<th>Correct</th>
						<th>Wrong</th>
						<th>Status</th>
						<!--<th>Kuis Jawaban</th>
						<th>Kuis Jawaban Benar</th>
						<th>Kuis Pembahasan</th>-->
						<th width="80px" class="text-center">Manage</th>
					  </tr>                  
					</thead> 
					<tbody>	
					<?php
					   $status = array(0=>'Not Active', 1=>'Active');
						$total_rows=$config['total_rows'];
						if(empty($no)){ 
							$no=1; 
							$nostart=1;
							$noend=$config['per_page'];
							if($noend>$total_rows){ $noend=$total_rows; }
						}else{ 
							$no=$no+1;
							$nostart=$no;
							$noend=$nostart+$config['per_page']-1;
							if($noend>$total_rows){ $noend=$total_rows; }
						} 
					?>
					<?php foreach($data as $row):  ?>
						<tr>     
							<td align="center"><?php echo $no; ?></td>
							<td><?php echo strlen($row->quiz_custom_correct)>50?substr($row->quiz_custom_correct,0,50).'...':$row->quiz_custom_correct; ?></td>
							<td><?php echo strlen($row->quiz_custom_wrong)>50?substr($row->quiz_custom_wrong,0,50).'...':$row->quiz_custom_wrong; ?></td>
							<td><?php echo $status[$row->quiz_custom_status]; ?></td>
							<td class="text-center">
								<a href="<?php echo site_url()."/quizcustom/edit/".$row->quiz_custom_id; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
								<a href="<?php echo site_url()."/quizcustom/delete/".$row->quiz_custom_id; ?>" title="Delete" onclick="return confirmDialog();" ><i class="fa fa-trash-o"></i></a></td>
						</tr>
					<?php $no++; endforeach; ?>
					</tbody>	
				</table>  
			</div>
			<footer class="panel-footer">
				<div class="row">
					<div class="col-sm-4 text-left"> 
						<?php if($total_rows != 0){ ?>
						<small class="text-muted inline m-t-sm m-b-sm"> Showing <?php echo $nostart; ?>-<?php echo $noend; ?> of <?php echo $config['total_rows']; ?> items</small>
						<?php }else{ ?>
						<small class="text-muted inline m-t-sm m-b-sm"> No records found</small>
						<?php } ?>
					</div>
					<div class="col-sm-5 text-right text-center-xs pull-right">
						<ul class="pagination pagination-sm m-t-none m-b-none">
							<?php echo $this->pagination->create_links(); ?>
						</ul>
					</div>
				</div>
			</footer>
		</div>
	</section>