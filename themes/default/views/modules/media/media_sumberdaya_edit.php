
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							
							<div class="form-group">
								<label for="media_name" class="col-sm-3 control-label">Nama Media *</label>
								<div class="col-sm-4">
									<input type="text" name="media_name" class="form-control" id="" placeholder="" value="<?php echo set_value('media_name', isset($data->media_name) ? $data->media_name : ''); ?>" />
								</div>
							</div>

							<div class="form-group">
								<label for="media_type" class="col-sm-3 control-label">Jenis Media *</label>
								<div class="col-sm-4">
									<?php 
										$type =  array(
											''		=> 'Pilih Jenis Media',
											'Sumber Daya - Pemasaran'	=> 'Sumber Daya - Pemasaran',
											'Sumber Daya - CRM'	=> 'Sumber Daya - CRM',
										);
									?>
									<?php 
									echo form_dropdown('media_type', $type, $data->media_type, 'class="form-control"');
									?> 
								</div>
							</div>
							
							
							<?php if (!empty($data->media_url)) { ?>
							<div class="form-group">
								<label for="subject_course" class="col-sm-3 control-label">Uploaded File*</label>
								<div class="col-sm-4">
									<input type="hidden" name="media_url" class="form-control" value="<?php echo set_value('filename', isset($data->filename) ? $data->filename : ''); ?>"/> 
									<a href="<?php echo $data->media_url; ?>" target="_blank" download ><?php echo $data->media_url; ?></a> 
								</div>
							</div> 
							<?php } ?>

							<div class="form-group">
								<label for="subject_course" class="col-sm-3 control-label">File Upload *</label>
								<div class="col-sm-4">
									<input name="images" type="file" class="input-file uniform_on"/> 
								</div>
							</div> 
							 
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<input type="hidden" name="media_id" class="form-control" id="" placeholder="" value="<?php echo set_value('media_id', isset($data->media_id) ? $data->media_id : ''); ?>">
							<button type="submit" class="btn btn-success">Simpan Data</button>
						</div>
					</div>
				</div>
			</div>
		</form>
