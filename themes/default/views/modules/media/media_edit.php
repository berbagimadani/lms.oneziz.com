
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							
							<div class="form-group">
								<label for="media_name" class="col-sm-3 control-label">Nama Media *</label>
								<div class="col-sm-4">
									<input type="text" name="media_name" class="form-control" id="" placeholder="" value="<?php echo set_value('media_name', isset($data->media_name) ? $data->media_name : ''); ?>" />
								</div>
							</div>

							<div class="form-group">
								<label for="media_type" class="col-sm-3 control-label">Media Type *</label>
								<div class="col-sm-4">
									<?php 
										$type =  array(
											''		=> 'Pilih Media Type',
											'file'	=> 'File',
											'video'	=> 'Video',
											'audio'	=> 'Audio',
										);
									?>
									<?php 
									echo form_dropdown('media_type', $type, $data->media_type, 'class="form-control"');
									?> 
								</div>
							</div>
							
							<div class="form-group">
								<label for="media_subject" class="col-sm-3 control-label">Media Subjek *</label>
								<div class="col-sm-4">
									<select name="media_subject" class="form-control" id="" >
										<?php if(!$data->subject_lesson_id){ ?><option value="">Pilih Media Subjek</option><?php } ?>
										<?php foreach($subject as $row):  ?>
											<option value="<?php echo $row->subject_id; ?>" <?php if($data->media_subject == $row->subject_id) { echo "selected";} ?> ><?php echo $row->subject_name; ?></option>
										<?php endforeach; ?>										
									</select>
								</div>
							</div>
							<?php if (!empty($data->media_url)) { ?>
							<div class="form-group">
								<label for="subject_course" class="col-sm-3 control-label">File Name*</label>
								<div class="col-sm-4">
									<input type="hidden" name="media_url" class="form-control" value="<?php echo set_value('filename', isset($data->filename) ? $data->filename : ''); ?>"/> 
									<label><?php echo $data->media_url; ?></label> 
								</div>
							</div> 
							<?php } ?>

							<div class="form-group">
								<label for="subject_course" class="col-sm-3 control-label">File Upload *</label>
								<div class="col-sm-4">
									<input name="images" type="file" class="input-file uniform_on"/> 
								</div>
							</div> 
							 
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<input type="hidden" name="media_id" class="form-control" id="" placeholder="" value="<?php echo set_value('media_id', isset($data->media_id) ? $data->media_id : ''); ?>">
							<button type="submit" class="btn btn-success">Save Data</button>
						</div>
					</div>
				</div>
			</div>
		</form>
