	 <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
				 
				<div class="logo-login"><img src="<?php echo $this->template->get_theme_path(); ?>/images/logo.png" alt="Oneziz LMS"  /></div>
				
				<?php if(validation_errors()){ ?>
				<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo validation_errors(); ?></div>
				<?php } ?>
				<?php if($this->session->flashdata('message')){ ?>
					<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo $this->session->flashdata('message'); ?></div>
				<?php } ?>
				
				<div class="login-panel panel panel-default">
					
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" action="<?php echo site_url(); ?>/login/checklogin">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="username" type="username" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
								<button type="submit" class="btn btn-success btn-block"><i class="fa fa-lock pull-left"></i> Sign in</button>
                            </fieldset>
                        </form>
						<div class="text-center"><br/><a href="<?php echo site_url(); ?>/resetpwd" title="Klik untuk recovery password">Lupa Password ?</a></div>
                    </div>
                </div>
				<p class="text-center"> <small>&copy; <?php echo date("Y");?>. Oneziz LMS. All Rights Reserved</small></p>
            </div>
        </div>
    </div>