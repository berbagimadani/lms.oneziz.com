
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							
							<div class="form-group">
								<label for="course_name" class="col-sm-3 control-label">Nama Paket Bimbel</label>
								<div class="col-sm-4">
									<input type="text" name="course_name" class="form-control" id="" placeholder="" value="<?php echo set_value('course_name', isset($data->course_name) ? $data->course_name : ''); ?>" />
								</div>
							</div>
							
							<div class="form-group">
								<label for="course_school_grade" class="col-sm-3 control-label">Tingkatan Pendidikan</label>
								<div class="col-sm-4">
									<select name="course_school_grade" class="form-control" id="" >
										<?php if(!$data->course_school_grade){ ?><option value="">Pilih Tingkatan Pendidikan</option><?php } ?>
										<?php foreach($course_school_grade as $row):  ?>
											<option value="<?php echo $row->school_grade_id; ?>" <?php if($data->course_school_grade == $row->school_grade_id) { echo "selected";} ?> ><?php echo $row->school_grade_name; ?></option>
										<?php endforeach; ?>										
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="course_price" class="col-sm-3 control-label">Harga Paket</label>
								<div class="col-sm-4">
									<input type="text" name="course_price" class="form-control" id="" placeholder="" value="<?php echo set_value('course_price', isset($data->course_price) ? $data->course_price : ''); ?>" />
								</div>
							</div>
						
						
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<input type="hidden" name="course_id" class="form-control" id="" placeholder="" value="<?php echo set_value('course_id', isset($data->course_id) ? $data->course_id : ''); ?>">
							<button type="submit" class="btn btn-success">Simpan Data</button>
						</div>
					</div>
				</div>
			</div>
		</form>
