
	
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="f_user" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							
							<div class="form-group">
								<label for="customer_name" class="col-sm-3 control-label">Username *</label>
								<div class="col-sm-4">
									<input type="text" name="username" class="form-control" id="" required="" placeholder="" value="<?php echo set_value('username', isset($data->username) ? $data->username : ''); ?>" <?php if($action=='edit') echo 'readonly'; ?> />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_company" class="col-sm-3 control-label">Nama Lengkap *</label>
								<div class="col-sm-4">
									<input type="text" name="fullname" class="form-control" id="" required="" placeholder="" value="<?php echo set_value('fullname', isset($data->fullname) ? $data->fullname : ''); ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_email" class="col-sm-3 control-label">Email *</label>
								<div class="col-sm-4">
									<input type="text" name="user_email" class="form-control" id=""  required="" placeholder="" value="<?php echo set_value('user_email', isset($data->user_email) ? $data->user_email : ''); ?>"  />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_email" class="col-sm-3 control-label">Password *</label>
								<div class="col-sm-4">
									<input type="password"  name="password" class="form-control" id="pass"  required="" placeholder="" value="<?php echo set_value('password', isset($data->password) ? $data->password : ''); ?>"  />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_email" class="col-sm-3 control-label">Konfirmasi Password *</label>
								<div class="col-sm-4">
									<input type="password" name="repassword" class="form-control" id="repass"  required="" placeholder="" value=""  />
								</div>
							</div>
<!--							<div class="form-group">-->
<!--								<label for="customer_email" class="col-sm-3 control-label">User Level *</label>-->
<!--								<div class="col-sm-4">-->
<!--									<select name="user_level" class="form-control">-->
<!--										--><?php //if(is_array($user_level))foreach($user_level as $row){ ?>
<!--										<option --><?php //if($data->user_level ==  $row['id']) echo 'selected'; ?><!-- value="--><?php //echo $row['id']?><!--">--><?php //echo $row['name'] ?><!--</option>-->
<!--										--><?php //} ?>
<!--									</select>-->
<!--								</div>-->
<!--							</div>-->
							<div class="form-group">
								<label for="customer_phone" class="col-sm-3 control-label">Telp *</label>
								<div class="col-sm-4">
									<input type="text" name="user_phone" class="form-control" id="" required="" placeholder="" value="<?php echo set_value('user_phone', isset($data->user_phone) ? $data->user_phone : ''); ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_mobile" class="col-sm-3 control-label">HP</label>
								<div class="col-sm-4">
									<input type="text" name="user_mobile" class="form-control" id="" placeholder="" value="<?php echo set_value('user_mobile', isset($data->user_mobile) ? $data->user_mobile : ''); ?>" />
								</div>
							</div>
							<!--<div class="form-group">
								<label for="customer_fax" class="col-sm-3 control-label">Fax</label>
								<div class="col-sm-4">
									<input type="text" name="customer_fax" class="form-control" id="" placeholder="" value="<?php /*echo set_value('customer_fax', isset($data->customer_fax) ? $data->customer_fax : ''); */?>" />
								</div>
							</div>-->
							<div class="form-group">
								<label for="customer_address" class="col-sm-3 control-label">Alamat</label>
								<div class="col-sm-4">									
									<textarea name="user_address" class="form-control" ><?php echo $data->user_address; ?></textarea>
								</div>
							</div>
							<!--<div class="form-group">
								<label for="customer_city" class="col-sm-3 control-label">Kota</label>
								<div class="col-sm-4">
									<input type="text" name="user_city" class="form-control" id="" placeholder="" value="<?php /*echo set_value('user_city', isset($data->user_city) ? $data->user_city : ''); */?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="customer_state" class="col-sm-3 control-label">Propinsi</label>
								<div class="col-sm-4">
									<input type="text" name="user_province" class="form-control" id="" placeholder="" value="<?php /*echo set_value('user_province', isset($data->user_province) ? $data->user_province : ''); */?>" />
								</div>
							</div>-->
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Province</label>
								<div class="col-sm-4">
									<select name="user_province" id="province" class="form-control">
										<?php if(!$data->user_provinve){ ?><option value="">Select Province</option><?php } ?>
										<?php foreach($province as $row):  ?>
											<option class="<?php echo $row->province_id; ?>" value="<?php echo $row->province_id; ?>" <?php if($data->user_province == $row->province_id) { echo "selected";} ?> ><?php echo $row->province_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">City</label>
								<div class="col-sm-4">
									<select name="user_city" id="city" class="form-control">
										<?php if(!$data->user_city){ ?><option value="">Select City</option><?php } ?>
										<?php foreach($city as $row):  ?>
											<option class="<?php echo $row->city_province_id; ?>" value="<?php echo $row->city_id; ?>" <?php if($data->user_city == $row->city_id) { echo "selected";} ?> ><?php echo $row->city_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="customer_zipcode" class="col-sm-3 control-label">Kodepos</label>
								<div class="col-sm-4">
									<input type="text" name="user_zipcode" class="form-control" maxlength="5" id="" placeholder="" value="<?php echo set_value('user_zipcode', isset($data->user_zipcode) ? $data->user_zipcode : ''); ?>" />
								</div>
							</div>
							
							<!--<div class="form-group">
								<label for="customer_remark" class="col-sm-3 control-label">Remark</label>
								<div class="col-sm-4">
									<textarea name="customer_remark" class="form-control" ><?php /*echo $data->customer_remark; */?></textarea>
								</div>
							</div>-->
							
							
						</div>		
						
						
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<input type="hidden" name="user_id" class="form-control" id="" placeholder="" value="<?php echo set_value('user_id', isset($data->user_id) ? $data->user_id : ''); ?>">
							<input type="hidden" name="user_level" class="form-control" id="" placeholder="" value="<?php echo set_value('user_level', isset($user_level) ? $user_level : '3'); ?>">
							<button type="button" onclick="beforeSubmitUser()" class="btn btn-success">Save Data</button>
						</div>
					</div>
				</div>
			</div>
			
		</form>