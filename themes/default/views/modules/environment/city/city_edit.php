
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							
							<div class="form-group">
								<label for="city_name" class="col-sm-3 control-label">Nama Kota *</label>
								<div class="col-sm-4">
									<input type="text" name="city_name" class="form-control" id="" placeholder="" value="<?php echo set_value('city_name', isset($data->city_name) ? $data->city_name : ''); ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="city_province" class="col-sm-3 control-label">Nama Provinsi *</label>
								<div class="col-sm-4">
									<select name="city_province" class="form-control" id="" >
										<?php if(!$data->city_province_id){ ?><option value="">Pilih Provinsi</option><?php } ?>
										<?php foreach($province as $row):  ?>
											<option value="<?php echo $row->province_id; ?>" <?php if($data->city_province_id == $row->province_id) { echo "selected";} ?> ><?php echo $row->province_name; ?></option>
										<?php endforeach; ?>										
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="city_province" class="col-sm-3 control-label">Tipe</label>
								<div class="col-sm-4">
									<select name="city_type" class="form-control" id="" >										
											<option value="Kota" <?php if($data->city_type == "Kota") { echo "selected";} ?> >Kota</option>
											<option value="Kabupaten" <?php if($data->city_type == "Kabupaten") { echo "selected";} ?> >Kabupaten</option>																			
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="city_zipcode" class="col-sm-3 control-label">Kodepos</label>
								<div class="col-sm-4">
									<input type="text" name="city_zipcode" class="form-control" id="" placeholder="" value="<?php echo set_value('city_zipcode', isset($data->city_zipcode) ? $data->city_zipcode : ''); ?>" />
								</div>
							</div>
						</div>		
						
						
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<input type="hidden" name="city_id" class="form-control" id="" placeholder="" value="<?php echo set_value('city_id', isset($data->city_id) ? $data->city_id : ''); ?>">
							<button type="submit" class="btn btn-success">Save Data</button>
						</div>
					</div>
				</div>
			</div>
		</form>
