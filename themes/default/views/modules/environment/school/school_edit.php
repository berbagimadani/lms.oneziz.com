
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							
							<div class="form-group">
								<label for="school_name" class="col-sm-3 control-label">Tingkatan Pendidikan *</label>
								<div class="col-sm-4">
									<input type="text" name="school_grade_name" class="form-control" id="" placeholder="" value="<?php echo set_value('school_grade_name', isset($data->school_grade_name) ? $data->school_grade_name : ''); ?>" />
								</div>
							</div>
							
						
						
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<input type="hidden" name="school_grade_id" class="form-control" id="" placeholder="" value="<?php echo set_value('school_grade_id', isset($data->school_grade_id) ? $data->school_grade_id : ''); ?>">
							<button type="submit" class="btn btn-success">Simpan Data</button>
						</div>
					</div>
				</div>
			</div>
		</form>
