
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header"><?php echo $menu_title; ?></h2>
        </div>
    </div>
			
	<?php if($this->session->flashdata('message')){ ?>
			<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
	<?php } ?>
		
	<section class="panel panel-default">
			<!-- TABLE HEADER -->
			<div class="panel-heading"><?php echo $submenu_title; ?></div>
			<div class="panel-body">
			
			<a href="<?php echo site_url();?>/quiz_create/create" title="" class="btn btn-success">Tambah Daftar Soal Latihan Baru</a><br/><br/>
			<div class="table-responsive">  
				
				<table class="table table-striped table-bordered table-hover">              
					<thead>                  
					  <tr>
						<th width="30px">No </th>
						<th>Tingkatan Pendidikan</th>
						<th>Mata Pelajaran</th>
						<th>Jenis Materi</th>
						<th>Kode Materi</th>
						<th class="text-right">Nilai<br/>Lulus<br/>(% )</th>
						<th class="text-right">Frek<br/>Latihan</th>
						<th class="text-right">Waktu<br/>Penyelesaian<br/>(Menit)</th>
						<th class="text-right">Bel Sisa<br/>Penyelesaian<br/>(Menit)</th>
						<th width="50px" class="text-center">Tambah<br/>Soal</th>
						<th width="80px" class="text-center">Ubah / Hapus</th>
					  </tr>                  
					</thead> 
					<tbody>	
					<?php
						$total_rows=$config['total_rows'];
						if(empty($no)){ 
							$no=1; 
							$nostart=1;
							$noend=$config['per_page'];
							if($noend>$total_rows){ $noend=$total_rows; }
						}else{ 
							$no=$no+1;
							$nostart=$no;
							$noend=$nostart+$config['per_page']-1;
							if($noend>$total_rows){ $noend=$total_rows; }
						} 
					?>
					<?php 
						foreach($data as $row):  
							//Get Specific Data
							$exam = $this->quiz_model->get_exam_by_subject($row->subject_id)->result();
							$exam = $exam[0];
					?>
						<tr>     
							<td align="center"><?php echo $no; ?></td>
							<td><?php echo $row->school_grade_name; ?></td>	
							<td><?php echo $row->lesson_name; ?></td>	
							<td><?php echo $row->subject_category; ?></td>	
							<td><?php echo $row->subject_code; ?></td>	
							<td align="right"><?php echo $exam->exam_pass; ?></td>	
							<td align="right"><?php echo $exam->exam_frequency; ?></td>	
							<td align="right"><?php echo $exam->exam_time; ?></td>	
							<td align="right"><?php echo $exam->exam_time_notification; ?></td>	
							<td class="text-center">
								<a href="<?php echo site_url()."/quiz/create/".$exam->exam_id; ?>" title="Add"><i class="fa fa-plus"></i></a>
								</td>
							<td class="text-center">
								<a href="<?php echo site_url()."/quiz_create/edit/".$exam->exam_id; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
								<a href="<?php echo site_url()."/quiz_create/delete/".$exam->exam_id; ?>" title="Delete" onclick="return confirmDialog();" ><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
					<?php $no++; endforeach; ?>
					</tbody>	
				</table>  
			</div>
			<footer class="panel-footer">
				<div class="row">
					<div class="col-sm-4 text-left"> 
						<?php if($total_rows != 0){ ?>
						<small class="text-muted inline m-t-sm m-b-sm"> Ditunjukkan  <?php echo $nostart; ?>-<?php echo $noend; ?> dari <?php echo $config['total_rows']; ?> data</small>
						<?php }else{ ?>
						<small class="text-muted inline m-t-sm m-b-sm"> Data tidak ditemukan</small>
						<?php } ?>
					</div>
					<div class="col-sm-5 text-right text-center-xs pull-right">
						<ul class="pagination pagination-sm m-t-none m-b-none">
							<?php echo $this->pagination->create_links(); ?>
						</ul>
					</div>
				</div>
			</footer>
		</div>
	</section>