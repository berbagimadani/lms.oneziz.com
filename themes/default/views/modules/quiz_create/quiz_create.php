
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							
 
							<div class="form-group">
								<label for="subject_lesson" class="col-sm-3 control-label">Paket Bimbel *</label>
								<div class="col-sm-4">
									<select name="subject_course" class="form-control" id="subject_course" >
										<?php if(!$q_course_id){ ?><option value="">Pilih Paket Bimbel</option><?php } ?>
										<?php foreach($course as $row):  ?>
											<option value="<?php echo $row->course_id; ?>" <?php if($q_course_id == $row->course_id) { echo "selected";} ?> ><?php echo $row->course_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="subject_exam" class="col-sm-3 control-label">Mata Pelajaran *</label>
								<div class="col-sm-4">
									<select name="subject_lesson" class="form-control" id="subject_lesson" >
										<?php if(!$data->subject_lesson_id){ ?><option value="">Pilih Mata Pelajaran</option><?php } ?>
										<?php foreach($lesson as $row):  ?>
											<option class="<?php echo $row->lesson_course_id; ?>" value="<?php echo $row->lesson_id; ?>" <?php if($subject_lesson->subject_lesson_id == $row->lesson_id) { echo "selected";} ?> ><?php echo $row->lesson_name; ?></option>
										<?php endforeach; ?>										
									</select>
								</div>
							</div>
						
							<div class="form-group">
								<label for="subject_exam" class="col-sm-3 control-label">Kode Materi</label>
								<div class="col-sm-4">
									<select name="subject_exam" class="form-control" id="subject_exam" >
										<?php if(!$data->exam_subject_id){ ?><option value="">Pilih Materi</option><?php } ?>
										<?php foreach($subject as $row):  ?>
											<option class="<?php echo $row->subject_lesson_id; ?>" value="<?php echo $row->subject_id; ?>" <?php if($data->exam_subject_id == $row->subject_lesson_id) { echo "selected";} ?> ><?php echo $row->subject_code; ?></option>
										<?php endforeach; ?>										
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="subject_name" class="col-sm-3 control-label">Nilai Lulus (%)</label>
								<div class="col-sm-4">
									<input type="text" name="exam_pass" class="form-control" id="" placeholder="" value="<?php echo set_value('exam_pass', isset($data->exam_pass) ? $data->exam_pass : ''); ?>" />
								</div>
							</div>
							
							<div class="form-group">
								<label for="subject_name" class="col-sm-3 control-label">Frekuensi Latihan</label>
								<div class="col-sm-4">
									<select name="exam_frequency" class="form-control" >
										<?php if(!$data->exam_frequency){ ?><option value="">Pilih Frekuensi Latihan</option><?php } ?>
											<option value="2" <?php if($data->exam_frequency == 2) { echo "selected";} ?> >2 kali</option>
											<option value="3" <?php if($data->exam_frequency == 3) { echo "selected";} ?> >3 kali</option>
											<option value="4" <?php if($data->exam_frequency == 4) { echo "selected";} ?> >4 kali</option>
											<option value="5" <?php if($data->exam_frequency == 5) { echo "selected";} ?> >5 kali</option>
											<option value="0" <?php if($data->exam_frequency == 0) { echo "selected";} ?> >Tak terbatas</option>
									</select>								
								</div>
							</div>
							
							<div class="form-group">
								<label for="subject_name" class="col-sm-3 control-label">Waktu Penyelesaiaan (Menit)</label>
								<div class="col-sm-4">
									<select name="exam_time" class="form-control" >
										<?php if(!$data->exam_time){ ?><option value="">Pilih Waktu Penyelesaiaan</option><?php } ?>
											<option value="60" <?php if($data->exam_time == 60) { echo "selected";} ?> >60 menit</option>
											<option value="90" <?php if($data->exam_time == 90) { echo "selected";} ?> >90 menit</option>
											<option value="120" <?php if($data->exam_time == 120) { echo "selected";} ?> >120 menit</option>
											<option value="150" <?php if($data->exam_time == 150) { echo "selected";} ?> >150 menit</option>
											<option value="180" <?php if($data->exam_time == 180) { echo "selected";} ?> >180 menit</option>
											<option value="0" <?php if($data->exam_time == 0) { echo "selected";} ?> >Tak Terbatas</option>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="subject_name" class="col-sm-3 control-label">Bel Sisa Penyelesaiaan (Menit)</label>
								<div class="col-sm-4">
									<select name="exam_time_notification" class="form-control" >
										<?php if(!$data->exam_time){ ?><option value="">Pilih Waktu Penyelesaiaan</option><?php } ?>
											<option value="0" <?php if($data->exam_time_notification == 0) { echo "selected";} ?> >0 menit</option>
											<option value="15" <?php if($data->exam_time_notification == 15) { echo "selected";} ?> >15 menit</option>
											<option value="30" <?php if($data->exam_time_notification == 30) { echo "selected";} ?> >30 menit</option>
											<option value="45" <?php if($data->exam_time_notification == 45) { echo "selected";} ?> >45 menit</option>
											<option value="60" <?php if($data->exam_time_notification == 60) { echo "selected";} ?> >60 menit</option>
																			
									</select>
								</div>
							</div>
						 
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<input type="hidden" name="exam_id" class="form-control" id="" placeholder="" value="<?php echo set_value('exam_id', isset($data->exam_id) ? $data->exam_id : ''); ?>">
							<button type="submit" class="btn btn-success">Simpan Daftar Spesifikasi Soal Latihan</button>
						</div>
					</div>
				</div>
			</div>
		</form>
