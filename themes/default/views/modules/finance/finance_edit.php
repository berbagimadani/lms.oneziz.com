<section class="main">
	<div class="container">
	
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							
							<div class="form-group">
								<label for="cash_date" class="col-sm-3 control-label">Tanggal Transaksi *</label>
								<div class="col-sm-4">
									<input type="text" name="cash_date" class="form-control datepicker" data-date="2015-02-15" data-date-format="yyyy-mm-dd" id="" placeholder="2015-02-12" value="<?php echo set_value('cash_date', isset($data->cash_date) ? $data->cash_date : ''); ?>" />
								</div>
							</div>
							
							<div class="form-group">
								<label for="customer_company" class="col-sm-3 control-label">Keterangan *</label>
								<div class="col-sm-4">
									<input type="text" name="cash_remark" class="form-control" id="" placeholder="" value="<?php echo set_value('cash_remark', isset($data->cash_remark) ? $data->cash_remark : ''); ?>" />
								</div>
							</div>
							
							<div class="form-group">
								<label for="customer_name" class="col-sm-3 control-label">Nominal *</label>
								<div class="col-sm-4">
									<input type="text" name="cash_total" class="form-control" id="" placeholder="" value="<?php echo set_value('cash_total', isset($data->cash_total) ? $data->cash_total : ''); ?>" />
								</div>
							</div>
							
							<div class="form-group">
								<label for="customer_email" class="col-sm-3 control-label">Arus Transaksi *</label>
								<div class="col-sm-4">
									<select name="cash_flow" class="form-control">
										<option value="in" <?php if($data->cash_flow == "in"){ echo "selected"; } ?> >Pemasukan</option>
										<option value="out" <?php if($data->cash_flow == "out"){ echo "selected"; } ?> >Pengeluaran</option>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="customer_name" class="col-sm-3 control-label">Nomor Referensi</label>
								<div class="col-sm-4">
									<input type="text" name="cash_reference" class="form-control" id="" placeholder="" value="<?php echo set_value('cash_reference', isset($data->cash_reference) ? $data->cash_reference : ''); ?>" />
								</div>
							</div>
							
							
						</div>		
						
						
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<input type="hidden" name="cash_id" class="form-control" id="" placeholder="" value="<?php echo set_value('cash_id', isset($data->cash_id) ? $data->cash_id : ''); ?>">
							<button type="submit" class="btn btn-success">Simpan Data</button>
						</div>
					</div>
				</div>
			</div>
			
			
			

			
			
			
		</form>
	</div>
</div>	