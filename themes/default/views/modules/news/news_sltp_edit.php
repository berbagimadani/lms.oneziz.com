
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							
							<div class="form-group">
								<label for="news_title" class="col-sm-3 control-label">Judul Pengumuman</label>
								<div class="col-sm-4">
									<input type="text" name="news_title" class="form-control" id="" placeholder="" value="<?php echo set_value('news_title', isset($data->news_title) ? $data->news_title : ''); ?>" />
								</div>
							</div>
							
							<div class="form-group">
								<label for="news_content" class="col-sm-3 control-label">Isi Pengumuman</label>
								<div class="col-sm-4">
									<textarea name="news_content" class="form-control textarea" ><?php echo set_value('news_content', isset($data->news_content) ? $data->news_content : ''); ?></textarea>
								</div>
							</div>

							<div class="form-group">
								<label for="news_content" class="col-sm-3 control-label">Tanggal Pengumuman</label>
								<div class="col-sm-4">
									<input type="text" name="news_date" class="form-control datepicker" data-date="2012-02-12" data-date-format="yyyy-mm-dd" id="" placeholder="" value="<?php echo set_value('news_date', isset($data->news_date) ? $data->news_date : ''); ?>" />
								</div>
							</div>
						
						
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<input type="hidden" name="news_id" class="form-control" id="" placeholder="" value="<?php echo set_value('news_id', isset($data->news_id) ? $data->news_id : ''); ?>">
							<input type="hidden" name="news_category" class="form-control" id="" placeholder="" value="2">
							<button type="submit" class="btn btn-success">Simpan Data</button>
						</div>
					</div>
				</div>
			</div>
		</form>
