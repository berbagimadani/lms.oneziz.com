
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header"><?php echo $menu_title; ?></h2>
        </div>
    </div>
			
	<?php if($this->session->flashdata('message')){ ?>
			<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
	<?php } ?>
		
	<section class="panel panel-default">
			<!-- TABLE HEADER -->
			<div class="panel-heading"><?php echo $menu_subtitle; ?></div>
			<div class="panel-body">
			
			<a href="<?php echo site_url();?>/news/create" title="" class="btn btn-success">Tambah Data Baru</a><br/><br/>
			<div class="table-responsive">  
				
				<table class="table table-striped table-bordered table-hover">              
					<thead>                  
					  <tr>
						<th width="30px">No</th>
						<th>Judul Pengumuman</th>
						<th>Isi Pengumuman</th>
						<th>Tanggal Pengumuman</th>
						<th width="80px" class="text-center">Ubah/Hapus</th>
					  </tr>                  
					</thead> 
					<tbody>	
					<?php
						$total_rows=$config['total_rows'];
						if(empty($no)){ 
							$no=1; 
							$nostart=1;
							$noend=$config['per_page'];
							if($noend>$total_rows){ $noend=$total_rows; }
						}else{ 
							$no=$no+1;
							$nostart=$no;
							$noend=$nostart+$config['per_page']-1;
							if($noend>$total_rows){ $noend=$total_rows; }
						} 
					?>
					<?php foreach($data as $row):  ?>
						<tr>     
							<td align="center"><?php echo $no; ?></td>
							<td><?php echo $row->news_title; ?></td>	
							<td><?php echo $row->news_content; ?></td>
							<td><?php echo $row->news_date; ?></td>	
							<td class="text-center">
								<a href="<?php echo site_url()."/news/edit/".$row->news_id; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
								<a href="<?php echo site_url()."/news/delete/".$row->news_id; ?>" title="Delete" onclick="return confirmDialog();" ><i class="fa fa-trash-o"></i></a></td>
						</tr>
					<?php $no++; endforeach; ?>
					</tbody>	
				</table>  
			</div>
			<footer class="panel-footer">
				<div class="row">
					<div class="col-sm-4 text-left"> 
						<?php if($total_rows != 0){ ?>
						<small class="text-muted inline m-t-sm m-b-sm"> Showing <?php echo $nostart; ?>-<?php echo $noend; ?> of <?php echo $config['total_rows']; ?> items</small>
						<?php }else{ ?>
						<small class="text-muted inline m-t-sm m-b-sm"> No records found</small>
						<?php } ?>
					</div>
					<div class="col-sm-5 text-right text-center-xs pull-right">
						<ul class="pagination pagination-sm m-t-none m-b-none">
							<?php echo $this->pagination->create_links(); ?>
						</ul>
					</div>
				</div>
			</footer>
		</div>
	</section>