

<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header"><?php echo $menu_title; ?></h2>
    </div>
</div>

<form class="form-horizontal" enctype="multipart/form-data" id="f_chpass" action="" method="post">
    <div class="panel panel-default">

        <!-- Panel Head -->
        <div class="panel-heading">
            <!-- Nav tabs -->
            <ul class="nav nav-pills">
                <li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
            </ul>
        </div>

        <!-- Panel Body -->
        <div class="panel-body">
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="personalinfo">
                    <?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
                    <?php if($this->session->flashdata('message')){ ?>
                        <div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo $this->session->flashdata('message'); ?></div>
                    <?php } ?>
                    <!--<div class="form-group">
                        <label for="customer_name" class="col-sm-3 control-label">Old Password *</label>
                        <div class="col-sm-4">
                            <input type="password" name="oldpass" onblur="checkOldPass()" class="form-control" id="oldpass" required="" placeholder="" />
                            <input type="hidden" name="asdf_p" class="form-control" id="asdf" value="<?php echo set_value('password', isset($data->password) ? $data->password : ''); ?>" />
                        </div>
                    </div>-->

                    <div class="form-group">
                        <label for="customer_name" class="col-sm-3 control-label">New Password *</label>
                        <div class="col-sm-4">
                            <input type="password" name="password" class="form-control" id="newpass" required="" placeholder="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="customer_name" class="col-sm-3 control-label">Confirm New Password *</label>
                        <div class="col-sm-4">
                            <input type="password" name="repassword" class="form-control" id="repass" required="" placeholder="" />
                        </div>
                    </div>
                </div>


            </div>

        </div>

        <!-- Panel Footer -->
        <div class="panel-footer">
            <div class="form-group">
                <div class="col-sm-3 ">
                    <input type="hidden" name="user_id" class="form-control" id="" placeholder="" value="<?php echo set_value('user_id', isset($data->user_id) ? $data->user_id : ''); ?>">
                    <button type="button" onclick="beforeSubmitChpass()" class="btn btn-success">Simpan Data</button>
                </div>
            </div>
        </div>
    </div>

</form>