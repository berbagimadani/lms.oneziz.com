
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							
							<div class="form-group">
								<label for="order_student" class="col-sm-3 control-label">Nama Lengkap Pelajar</label>
								<div class="col-sm-4">
									<select name="order_student" class="form-control" id="" >
										<?php if(!$data->order_student){ ?><option value="">Pilih Pelajar</option><?php } ?>
										<?php foreach($user as $row):  ?>
											<option value="<?php echo $row->user_id; ?>" <?php if($data->order_student == $row->user_id) { echo "selected";} ?> ><?php echo $row->fullname; ?></option>
										<?php endforeach; ?>										
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="order_course" class="col-sm-3 control-label">Nama Paket Bimbel</label>
								<div class="col-sm-4">
									<select name="order_course" class="form-control" id="" >
										<?php if(!$data->order_course){ ?><option value="">Pilih Paket Bimbel</option><?php } ?>
										<?php foreach($course as $row):  ?>
											<option value="<?php echo $row->course_id; ?>" <?php if($data->order_course == $row->course_id) { echo "selected";} ?> ><?php echo $row->course_name; ?></option>
										<?php endforeach; ?>										
									</select>
								</div>
							</div>
							
							
							<div class="form-group">
								<label for="course_price" class="col-sm-3 control-label">Tanggal Pembayaran Ke-1</label>
								<div class="col-sm-4">
									<input type="text" name="order_pay_date" class="form-control datepicker" data-date="2012-02-12" data-date-format="yyyy-mm-dd" id="" placeholder="2012-02-12" value="<?php echo set_value('order_pay_date', isset($data->order_pay_date) ? $data->order_pay_date : ''); ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="course_price" class="col-sm-3 control-label">Jumlah Pembayaran Ke-1</label>
								<div class="col-sm-4">
									<input type="text" name="order_price" class="form-control" id="" placeholder="" value="<?php echo set_value('order_price', isset($data->order_price) ? $data->order_price : ''); ?>" />
								</div>
							</div>
							
							<div class="form-group">
								<label for="course_price" class="col-sm-3 control-label">Tanggal Awal Aktif Paket</label>
								<div class="col-sm-4">
									<input type="text" name="order_date_start" class="form-control datepicker" data-date="2012-02-12" data-date-format="yyyy-mm-dd" id="" placeholder="2012-02-12" value="<?php echo set_value('order_date_start', isset($data->order_date_start) ? $data->order_date_start : ''); ?>" />
								</div>
							</div>

							<div class="form-group">
								<label for="course_price" class="col-sm-3 control-label">Tanggal Akhir Aktif Paket</label>
								<div class="col-sm-4">
									<input type="text" name="order_date_end" class="form-control datepicker" data-date="2012-02-12" data-date-format="yyyy-mm-dd" id="" placeholder="2012-02-12" value="<?php echo set_value('order_date_end', isset($data->order_date_end) ? $data->order_date_end : ''); ?>" />
								</div>
							</div>


							<div class="form-group">
								<label for="course_name" class="col-sm-3 control-label">Status</label>
								<div class="col-sm-4">
									<select name="order_status" class="form-control" id="" >
										<?php if(!$data->order_status){ ?><option value="">Pilih Status</option><?php } ?>
										<option value="1" <?php if($data->order_status == 1) echo "selected"?> >Aktif</option>
										<option value="0" <?php if($data->order_status == 0) echo "selected"?> >Tidak Aktif</option>									
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="order_teller" class="col-sm-3 control-label">Petugas Pendaftaran</label>
								<div class="col-sm-4">
									<select name="order_teller" class="form-control" id="" >
										<?php if(!$data->order_teller){ ?><option value="">Pilih Petugas Pendaftaran</option><?php } ?>
										<?php foreach($teller as $row):  ?>
											<option value="<?php echo $row->user_id; ?>" <?php if($data->order_teller == $row->user_id) { echo "selected";} ?> ><?php echo $row->fullname; ?></option>
										<?php endforeach; ?>										
									</select>
								</div>
							</div>
							
						
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<input type="hidden" name="order_id" class="form-control" id="" placeholder="" value="<?php echo set_value('order_id', isset($data->order_id) ? $data->order_id : ''); ?>">
							<button type="submit" class="btn btn-success">Simpan Data</button>
						</div>
					</div>
				</div>
			</div>
		</form>
