
	<?php $time = $this->exam_model->get_exam_pass( $exam_id );  ?>
	<script language="JavaScript"> 

		var version = navigator.appVersion; 

		function showKeyCode(e) { 
		var keycode = (window.event) ? event.keyCode : e.keyCode; 

		if ((version.indexOf('MSIE') != -1)) { 
		if (keycode == 116) { 
		event.keyCode = 0; 
		event.returnValue = false; 
		return false; 
		} 
		} 
		else { 
		if (keycode == 116) { 
		return false; 
		} 
		} 
		} 

	</script> 
	<script type="text/javascript">
		var counter = <?php echo $time[0]->exam_time * 60;   ?>;
		var intervalId = null;

		function action()
		{
		  clearInterval(intervalId);
		  window.location='<?php echo site_url(); ?>/exam/raport/<?php echo $key; ?>';
		  //document.getElementById("bip").innerHTML = "THE END!";	
		}
		function bip()
		{
		  document.getElementById("bip").innerHTML = counter + " seconds remaining";
		  counter--;
		}
		function start()
		{
		  intervalId = setInterval(bip, 1000);
		  setTimeout(action, counter * 1000); 
		}	

		start();

	</script>

	<div class="row" id="demo">
		<div class="col-lg-12">
			<h2 class="page-header"><?php echo $total_rows.' Pertanyaan dari soal '.$data[0]->subject_id; ?></h2>
time:<?php echo $time[0]->exam_time; ?> minutes
<div id="bip"></div>
        </div>
    </div> 
		
	<section class="panel panel-default">
			<!-- TABLE HEADER -->
			<div class="panel-heading">

			Quiz &nbsp; <label> <div id="output_answer_end"></div></label>


			</div>
			<div class="panel-body"> 
			<div class="table-responsive">  
				
				<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs " role="tablist">
  	<?php $no=1; foreach($data as $row_one):  ?>

    <li role="presentation" class="<?php if($no == 1) echo 'active'; ?>"><a href="#exam<?php echo $no; ?>" aria-controls="exam<?php echo $no; ?>" role="tab" data-toggle="tab"><?php echo $no; ?></a></li> 
    <?php $no++; endforeach; ?>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
  	<?php $no2=1; foreach($data as $row):  ?>
    <div role="tabpanel" class="tab-pane <?php if($no2 == 1) echo 'active'; ?>" id="exam<?php echo $no2; ?>">
   		<table class="table table-striped table-bordered table-hover">              
					<thead>                  
					  <tr>  
						<th>Pertanyaan</th>
					  </tr>                 
					</thead> 
					<tbody>	 
						<tr>
							<td>
								<?php echo $row->quiz_question; ?> 
							</td>
						</tr>
						<tr>       
							<td> 
								<form method="post" action="#" id="button">

 								<ul>
									<div id="output_answer">

									<li>A <input type="radio" value="a" id="answer_exam" data="a" content="<?php echo $row->playquiz_quiz_id; ?>" key="<?php echo $key; ?>" name="answer" <?php if($row->answer == 'a' ) echo "checked"; else echo ""; ?> ><?php echo $row->quiz_answer_a; ?></li>
									<li>B <input type="radio" value="b" id="answer_exam" data="b" content="<?php echo $row->playquiz_quiz_id; ?>" key="<?php echo $key; ?>" name="answer" <?php if($row->answer == 'b' ) echo "checked"; else echo "";  ?> ><?php echo $row->quiz_answer_b; ?></li>
									<li>C <input type="radio" value="c" id="answer_exam" data="c" content="<?php echo $row->playquiz_quiz_id; ?>" key="<?php echo $key; ?>" name="answer" <?php if($row->answer == 'c' ) echo "checked"; else echo "";  ?> ><?php echo $row->quiz_answer_c; ?></li>
									<li>D <input type="radio" value="d" id="answer_exam" data="d" content="<?php echo $row->playquiz_quiz_id; ?>" key="<?php echo $key; ?>" name="answer" <?php if($row->answer == 'd' ) echo "checked"; else echo "";  ?> ><?php echo $row->quiz_answer_d; ?></li>
									<li>E <input type="radio" value="e" id="answer_exam" data="e" content="<?php echo $row->playquiz_quiz_id; ?>" key="<?php echo $key; ?>" name="answer" <?php if($row->answer == 'e' ) echo "checked"; else echo "";  ?> ><?php echo $row->quiz_answer_e; ?></li>
									

									</div>
								</ul>  
        		
								</form> 
							</td>	 
						</tr> 
					</tbody>	
		</table>  
     </div>
    <?php $no2++; $key = $row_one->playquiz_key; endforeach; ?> 
  </div>

</div>


			</div>
			<footer class="panel-footer">
				<div class="row">
					 
					<div class="col-sm-5 text-right text-center-xs pull-right">

						<?php   
						//if (empty($total_rows)) { ?>
						<a href="<?php echo site_url(); ?>/exam/raport/<?php echo $key; ?>" class="btn btn-default">SERAHKAN</a>
						<?php //} ?> 

					</div>
				</div>
			</footer>
		</div>
	</section>

	
