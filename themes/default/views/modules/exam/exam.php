
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header"><?php echo $total_rows.' Pertanyaan dari soal '.$data[0]->subject_name; ?></h2>
        </div>
    </div> 
		
	<section class="panel panel-default">
			<!-- TABLE HEADER -->
			<div class="panel-heading">

			Quiz &nbsp; <label> <div id="output_answer_end"></div></label>


			</div>
			<div class="panel-body"> 
			<div class="table-responsive">  
				
				<table class="table table-striped table-bordered table-hover">              
					<thead>                  
					  <tr>  
						<th>Pertanyaan</th>
					  </tr>                 
					</thead> 
					<tbody>	
					<?php
						 
					?>
					<?php foreach($data as $row):  ?>
						<tr>
							<td>
								<?php echo $row->quiz_question; ?> 
							</td>
						</tr>
						<tr>       
							<td> 
								<form method="post" action="#" id="button">

								<input type="hidden" value="<?php echo $row->playquiz_quiz_id; ?>" name="quiz_id" id="quiz_id">
								<ul>
									<div id="output_answer">

									<li>A <input type="radio" value="a" id="answer_exam" name="answer" <?php if($row->answer == 'a' ) echo "checked"; else echo ""; ?> ><?php echo $row->quiz_answer_a; ?></li>
									<li>B <input type="radio" value="b" id="answer_exam" name="answer" <?php if($row->answer == 'b' ) echo "checked"; else echo "";  ?> ><?php echo $row->quiz_answer_b; ?></li>
									<li>C <input type="radio" value="c" id="answer_exam" name="answer" <?php if($row->answer == 'c' ) echo "checked"; else echo "";  ?> ><?php echo $row->quiz_answer_c; ?></li>
									<li>D <input type="radio" value="d" id="answer_exam" name="answer" <?php if($row->answer == 'd' ) echo "checked"; else echo "";  ?> ><?php echo $row->quiz_answer_d; ?></li>
									<li>E <input type="radio" value="e" id="answer_exam" name="answer" <?php if($row->answer == 'e' ) echo "checked"; else echo "";  ?> ><?php echo $row->quiz_answer_e; ?></li>
									

									</div>
								</ul>  
        
								</form> 
							</td>	 
						</tr>
					<?php $key .= $row->playquiz_key;  endforeach; ?>
					</tbody>	
				</table>  
			</div>
			<footer class="panel-footer">
				<div class="row">
					 
					<div class="col-sm-5 text-right text-center-xs pull-right">

						<a href="<?php echo site_url(); ?>/exam/browse/<?php echo $key; ?>" id="">BACK</a>
						>
						<a href="<?php echo site_url(); ?>/exam/browse" id="quiz_next-">NEXT</a>
						>
						<?php   
						if (empty($total_rows)) { ?>
						<a href="<?php echo site_url(); ?>/exam/raport/<?php echo $key; ?>">FINISH</a>
						<?php } ?>

					</div>
				</div>
			</footer>
		</div>
	</section>

	
