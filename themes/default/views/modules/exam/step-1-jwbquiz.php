	
		 
		
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>

		<div class="table-responsive">  
				
				<table class="table table-striped table-bordered table-hover">              
					<thead>                  
					  <tr>
						<th width="30px">No</th>
						<th>Tingkatan Pendidikan</th>
						<th width="180px">Mata Pelajaran</th> 
						<th width="180px">Jenis Materi</th> 
						<th width="180px">Kode Materi</th>  
						<th width="80px" class="text-center">Action</th>
					  </tr>                  
					</thead> 
					<tbody>	
					<?php
						$total_rows=$config['total_rows'];
						if(empty($no)){ 
							$no=1; 
							$nostart=1;
							$noend=$config['per_page'];
							if($noend>$total_rows){ $noend=$total_rows; }
						}else{ 
							$no=$no+1;
							$nostart=$no;
							$noend=$nostart+$config['per_page']-1;
							if($noend>$total_rows){ $noend=$total_rows; }
						} 
					?>
					<?php 

						foreach($subject as $row):  
						//Get Specific Data
						$exam = $this->quiz_model->get_exam_by_subject($row->subject_id)->result();
						$exam = $exam[0];

					?>
						<tr>     
							<td align="center"><?php echo $no; ?></td>
							<td><?php echo $row->school_grade_name; ?></td>	
							<td>
							<?php echo $row->lesson_name; ?>
							<?php 
							echo ' ( '.$this->exam_model->count_quiz($exam->exam_id).' ) ';
							?>
							</td>	
							<td><?php echo $row->subject_category; ?></td>	
							<td><?php echo $row->subject_code; ?></td> 
							<td class="text-center">
								<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
									<input type="hidden" name="random" value="<?php echo $key; ?>" />
									<input type="hidden" name="quiz" value="<?php echo $exam->exam_id; ?>" />
									<input type="hidden" name="lesson_name" value="<?php echo $row->lesson_name; ?>" />
									<?php if($this->exam_model->count_quiz($exam->exam_id) > 0 ){ ?>
										<button type="submit" class="btn btn-success">Start</button>
									<?php } ?>
								</form>
							</td>
						</tr>
					<?php $no++; endforeach; ?>
					</tbody>	
				</table>  
			</div> 

		 
