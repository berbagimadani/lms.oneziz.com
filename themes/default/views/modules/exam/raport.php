
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Nilai <?php echo $total_rows.' Pertanyaan dari soal '.$data[0]->subject_name; ?></h2>
        </div>
    </div> 
		
	<section class="panel panel-default">
			<!-- TABLE HEADER -->
			<div class="panel-heading">

			Quiz &nbsp; <label> <div id="output_answer"></div></label>

			</div>
			<div class="panel-body"> 
			<div class="table-responsive">  

			<div role="tabpanel">

			  <!-- Nav tabs -->
			  <ul class="nav nav-tabs " role="tablist">
			  	<?php $no=1; foreach($result as $row_one):  ?>

			    <li role="presentation" class="<?php if($no == 1) echo 'active'; ?>"><a href="#exam<?php echo $no; ?>" aria-controls="exam<?php echo $no; ?>" role="tab" data-toggle="tab"><?php echo $no; ?></a></li> 
			    <?php $no++; endforeach; ?>
			    <li role="presentation" class=""><a href="#exam_score" aria-controls="exam<?php echo $no; ?>" role="tab" data-toggle="tab">Score</a></li> 
			  </ul>

		  <!-- Tab panes -->
		  <div class="tab-content">
		  	<?php $no2=1; foreach($result as $row):  ?>
		    <div role="tabpanel" class="tab-pane <?php if($no2 == 1) echo 'active'; ?>" id="exam<?php echo $no2; ?>">
		   		<table class="table table-striped table-bordered table-hover">              
							<thead>                  
							  <tr>  
								<th>Pertanyaan</th>
							  </tr>                 
							</thead> 
							<tbody>	 
								<tr>
									<td>
										<?php echo $row->quiz_question; ?> 
									</td>
								</tr>
								<tr>       
									<td> 
										<form method="post" action="#" id="button">

		 								<ul>
											<div id="output_answer">

											<li>A <input type="radio" value="a"  name="answer" <?php if($row->answer == 'a' ) echo "checked"; else echo ""; ?> disabled><?php echo $row->quiz_answer_a; ?></li>
											<li>B <input type="radio" value="b"  name="answer" <?php if($row->answer == 'b' ) echo "checked"; else echo "";  ?> disabled><?php echo $row->quiz_answer_b; ?></li>
											<li>C <input type="radio" value="c"  name="answer" <?php if($row->answer == 'c' ) echo "checked"; else echo "";  ?> disabled><?php echo $row->quiz_answer_c; ?></li>
											<li>D <input type="radio" value="d"  name="answer" <?php if($row->answer == 'd' ) echo "checked"; else echo "";  ?> disabled><?php echo $row->quiz_answer_d; ?></li>
											<li>E <input type="radio" value="e"  name="answer" <?php if($row->answer == 'e' ) echo "checked"; else echo "";  ?> disabled><?php echo $row->quiz_answer_e; ?></li>
											

											</div>
										</ul>  
		        		
										</form> 
									</td>	 
								</tr> 
								<tr>
									<td>
										 <b>Jawaban </b> : ( <?php echo $row->quiz_answer; ?> )
									</td>
								</tr>
								<tr>
									<td>
										<b>Pembahasan </b>
										<br>
										<?php echo $row->quiz_solution; ?>
									</td>
								</tr>
							</tbody>	
				</table>  
		     </div>
		    <?php $no2++; $key = $row_one->playquiz_key; endforeach; ?> 
		    <div role="tabpanel" class="tab-pane" id="exam_score">
		    	Jawaban Benar : <?php echo $true; ?>
		    	<br>
		    	Jawaban Salah : <?php echo $false; ?>
		    	<br>
		    	Nilai Penyelesain : <?php echo $percent.'%'; ?>
		    	<br>
		    	Nilai lulus :  
		    	<?php $pass = $this->exam_model->get_exam_pass( $exam_id ); echo $pass[0]->exam_pass; ?>
		    	<br>
		    	Kesimpulan : <?php //echo $summary; ?>
		    	<br>

		    	<?php if ( $percent < $pass[0]->exam_pass ) {?>
		    	<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
					<input type="hidden" name="random" value="<?php echo $keyopt; ?>">
					<input type="hidden" name="quiz" value="<?php echo $exam_id; ?>">
					<button type="submit" class="btn btn-success">Ulangi Latihan</button>
				</form>
				<?php } else { ?>

					<h3>Anda Lulus</h3>
					<a href="<?php echo site_url(); ?>/exam/start" class="btn btn-success">Lanjut Ke materi berikut</a>

				<?php } ?>


		    </div>
		  </div>

		</div>
				
				<!--<table class="table table-striped table-bordered table-hover">              
					<thead>                  
					  <tr>  
						<th>Result</th>
					  </tr>                 
					</thead> 
					<tbody>	  
						

						<tr>
							<td> 
								<label>Pertanyaan</label>
							</td>
						</tr>

						<?php $no=1; foreach ($result as $key => $value) { ?>
						
						<tr>
							<td>
								<?php  echo $value->quiz_question; ?>

								<br>

								<b>Jawaban </b> : ( <?php echo $value->quiz_answer; ?> )
								<br>
								<?php 

										$data = array(
											'a'	=> $value->quiz_answer_a,
											'b'	=> $value->quiz_answer_b,
											'c'	=> $value->quiz_answer_c,
											'd'	=> $value->quiz_answer_d,
											'e'	=> $value->quiz_answer_e,
										);

								?>

								<?php echo $data[$value->quiz_answer]; ?>
								<?php //if( $value->quiz_answer == $data[0]; ) echo $value->quiz_answer ?>

								<br>

								<b>Pembahasan </b>
								<br>
								<?php echo $value->quiz_solution; ?>

							</td>
						</tr>  	
						<?php $no++; } ?>

						<tr>
							<td>
								Jawaban Benar : <?php echo $true; ?>
							</td>
						</tr>  
						<tr>
							<td>
								Jawaban Salah : <?php echo $false; ?>
							</td>
						</tr>


					</tbody>	
				</table>  -->


			</div>
			<footer class="panel-footer">
				<div class="row">
					 
					<div class="col-sm-5 text-right text-center-xs pull-right">
						<a href="" id="quiz_next">NEXT</a>
					</div>
				</div>
			</footer>
		</div>
	</section>

	
