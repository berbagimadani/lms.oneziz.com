
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"><?php echo $menu_title; ?></h2>
			</div>
		</div>
	
		<form class="form-horizontal" enctype="multipart/form-data" id="" action="" method="post">
			<div class="panel panel-default">
			
				<!-- Panel Head -->
				<div class="panel-heading">
					<!-- Nav tabs -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#personalinfo" data-toggle="tab"><?php echo $menu_subtitle; ?></a></li>
					</ul>
				</div>
				
				<!-- Panel Body -->
				<div class="panel-body">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="personalinfo">
							<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
							
 
							<div class="form-group">
								<label for="subject_lesson" class="col-sm-3 control-label">Paket Bimbel *</label>
								<div class="col-sm-4">
									<select name="subject_course" class="form-control" id="subject_course" >
										<?php if(!$q_course_id){ ?><option value="">Pilih Paket Bimbel</option><?php } ?>
										<?php foreach($course as $row):  ?>
											<option value="<?php echo $row->course_id; ?>" <?php if($q_course_id == $row->course_id) { echo "selected";} ?> ><?php echo $row->course_name; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="subject_exam" class="col-sm-3 control-label">Mata Pelajaran *</label>
								<div class="col-sm-4">
									<select name="subject_lesson" class="form-control" id="subject_lesson" >
										<?php if(!$data->subject_lesson_id){ ?><option value="">Pilih Mata Pelajaran</option><?php } ?>
										<?php foreach($lesson as $row):  ?>
											<option class="<?php echo $row->lesson_course_id; ?>" value="<?php echo $row->lesson_id; ?>" <?php if($subject_lesson->subject_lesson_id == $row->lesson_id) { echo "selected";} ?> ><?php echo $row->lesson_name; ?></option>
										<?php endforeach; ?>										
									</select>
								</div>
							</div>
						
							<div class="form-group">
								<label for="subject_exam" class="col-sm-3 control-label">Nama Materi</label>
								<div class="col-sm-4">
									<select name="subject_exam" class="form-control" id="subject_exam" >
										<?php if(!$data->exam_subject_id){ ?><option value="">Pilih Exam Subject</option><?php } ?>
										<?php foreach($subject as $row):  ?>
											<option class="<?php echo $row->subject_lesson_id; ?>" value="<?php echo $row->subject_id; ?>" <?php if($data->exam_subject_id == $row->subject_lesson_id) { echo "selected";} ?> ><?php echo $row->subject_name; ?></option>
										<?php endforeach; ?>										
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="subject_name" class="col-sm-3 control-label">Exam Title *</label>
								<div class="col-sm-4">
									<input type="text" name="exam_title" class="form-control" id="" placeholder="" value="<?php echo set_value('exam_title', isset($data->exam_title) ? $data->exam_title : ''); ?>" />
								</div>
							</div>
							
						 
					</div>
					
				</div>
				
				<!-- Panel Footer -->
				<div class="panel-footer">
					<div class="form-group">
						<div class="col-sm-3 ">
							<input type="hidden" name="exam_id" class="form-control" id="" placeholder="" value="<?php echo set_value('exam_id', isset($data->exam_id) ? $data->exam_id : ''); ?>">
							<button type="submit" class="btn btn-success">Save Data</button>
						</div>
					</div>
				</div>
			</div>
		</form>
