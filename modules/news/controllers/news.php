<?php

class News extends Front_Controller{
	
	private $per_page 	= '30';
	private $title 		= 'News';
	private $module 	= 'news';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('news_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect('news/slta');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function slta($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->news_model->count_all($this->input->post('q'),1);
			
			//pagination
			$config['base_url']     = site_url('news/slta');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = 30; 
			$config['uri_segment']  = 4;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(4);
			
			$data = $this->news_model->get_all($config['per_page'], $no, $this->input->post('q'),1);		
			
			
			$this->template	->set('menu_title', 'Pengaturan Pengumuman')
							->set('menu_subtitle', 'Daftar Pengumuman SLTA')
							->set('menu_environment', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('news');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function sltp($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->news_model->count_all($this->input->post('q'),2);
			
			//pagination
			$config['base_url']     = site_url('news/sltp');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = 30; 
			$config['uri_segment']  = 4;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(4);
			
			$data = $this->news_model->get_all($config['per_page'], $no, $this->input->post('q'),2);		
			
			
			$this->template	->set('menu_title', 'Pengaturan Pengumuman')
							->set('menu_subtitle', 'Daftar Pengumuman SLTP')
							->set('menu_environment', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('news_sltp');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function create(){
		if($this->save_news()){
			$this->session->set_flashdata('message', 'success|Data berhasil ditambahkan');
			redirect('news/slta');
		}
					
			
		$this->template	->set('menu_title', 'Pengaturan Pengumuman')
						->set('menu_subtitle', 'Formulir Pengaturan Pengumuman')
						->build('news_edit');		
	}
	
	public function edit($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_news()){
			$this->session->set_flashdata('message', 'success|Data berhasil diubah');
			redirect('news/slta');
		}
		
		//Get Specific Data
		$data = $this->news_model->get_news($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'Pengaturan Pengumuman')
						->set('menu_subtitle', 'Formulir Pengaturan Pengumuman')
						->build('news_edit');		
	}
	
	public function create_sltp(){
		if($this->save_news()){
			$this->session->set_flashdata('message', 'success|Data berhasil ditambahkan');
			redirect('news/sltp');
		}
					
			
		$this->template	->set('menu_title', 'Pengaturan Pengumuman')
						->set('menu_subtitle', 'Formulir Pengaturan Pengumuman')
						->build('news_edit_sltp');		
	}
	
	public function edit_sltp($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_news()){
			$this->session->set_flashdata('message', 'success|Data berhasil diubah');
			redirect('news/sltp');
		}
		
		//Get Specific Data
		$data = $this->news_model->get_news($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'Pengaturan Pengumuman')
						->set('menu_subtitle', 'Formulir Pengaturan Pengumuman')
						->build('news_sltp_edit');		
	}
	
	public function view($id){
		$id =  $this->uri->segment(3);
		
		//Get Specific data
		$data = $this->news_model->get_news($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'View')
						->set('menu_subtitle', 'View')
						->build('news_edit');	
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(3);
		if($this->news_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Success delete row');
			redirect('news');
			exit;
		}
	}	
	
	private function save_news(){
		
		//set form validation
		$this->form_validation->set_rules('news_title', 'Judul News', 'required');
		$this->form_validation->set_rules('news_content', 'Konten News', 'required');
		$this->form_validation->set_rules('news_date', 'Date News', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('news_id');			
			
			//process the form
			$data = array(
				'news_title'       	=> $this->input->post('news_title'),
				'news_content'      => $this->input->post('news_content'),
				'news_date'       	=> $this->input->post('news_date'),
				'news_category'       	=> $this->input->post('news_category'),
				
			);
				
			if(!$id){
				return $this->news_model->insert($data);
			}else{
				return $this->news_model->update($id, $data);
			} 
		}
	}
}