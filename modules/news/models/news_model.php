<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * course Model
 * @module	course
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class news_model extends MY_Model {

    protected $table        = 'tbl_news';
    protected $key          = 'news_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='', $news_category)
	{
		if($search != '')
		{
			return $this->db->select('*')
							->from($this->table)
							->where('tbl_news.deleted','0')
							->like('news_title',$search)
							->where('news_category',$news_category)
							->limit($limit,$offset)
							->order_by('news_title','asc')
							->get()
							->result();
		}else
		{		
			return $this->db->select('*')
						->from($this->table)
						->where('tbl_news.deleted','0')
						->like('news_title',$search)
							->where('news_category',$news_category)
						->limit($limit,$offset)
						->order_by('news_title','asc')
						->get()
						->result();
		}
	}
	
	public function count_all($search, $news_category)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('deleted','0')
						->like('news_title',$search)
							->where('news_category',$news_category)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	
		
	public function get_news($id)
	{
		$this->db	->select('*')
					->where('tbl_news.deleted', 0)
					->where('news_id', $id);
        return $this->db->get($this->table);    
    }
	
		
	public function get_all_news()
	{
		$this->db	->select('*')
					->where('tbl_news.deleted', 0);
        return $this->db->get('tbl_news');    
    }
}