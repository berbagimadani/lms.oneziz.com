<?php

class Report extends Front_Controller{
	
	private $per_page 	= '15';
	private $title 		= 'Report';
	private $module 	= 'report';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect($this->module.'/student');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function student($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->user_model->count_report($this->input->post('province'), $this->input->post('city'), $this->input->post('school'), $this->input->post('date_start'), $this->input->post('date_end'));

			//pagination
			$config['base_url']     = site_url($this->module.'/student');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = 30;
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';

			$this->pagination->initialize($config);
			$no =  $this->uri->segment(3);

			$customer = $this->user_model->get_report($config['per_page'],$page,$this->input->post('province'), $this->input->post('city'), $this->input->post('school'), $this->input->post('date_start'), $this->input->post('date_end'));

			$province = $this->user_model->getProvince();
			$city     = $this->user_model->getCity();
			$tingkatan = $this->user_model->getSchoolGrade();
			$school = $this->user_model->getSchool();


			$this->template	->set('menu_title', 'Laporan Daftar Pelajar')
				->set('menu_database', 'active')
				->set('material_total',$config['total_rows'])
				->set('user', $customer)
				->set('tipe', 'all')
				->set('no', $no)
				->set('province', $province)
				->set('city', $city)
				->set('tingkatan', $tingkatan)
				->set('school', $school)
				->set('q_province',  $this->input->post('province'))
				->set('q_city',  $this->input->post('city'))
				->set('q_tingkatan',  $this->input->post('tingkat'))
				->set('q_school',  $this->input->post('school'))
				->set('q_dtstart',  $this->input->post('date_start'))
				->set('q_dtend',  $this->input->post('date_end'))
				->set('config', $config)
				->build('student');
		}
		else
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	

	public function download_excel(){ 
		
		$report = $this->user_model->get_report($config['per_page'],$page,$this->input->post('report_province'), $this->input->post('report_city'), $this->input->post('report_school'), $this->input->post('report_date_start'), $this->input->post('report_date_end'));
		$timestamp = date("Y-m-d");
		
					
		//load our new PHPExcel library
		$this->load->library('excel');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("ONEZIZ");
		$objPHPExcel->getProperties()->setLastModifiedBy("ONEZIZ");
		$objPHPExcel->getProperties()->setTitle("Laporan Daftar Pelajar");
		$objPHPExcel->getProperties()->setSubject("Laporan Daftar Pelajar");
		$objPHPExcel->getProperties()->setDescription("Laporan Daftar Pelajar");
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle('Laporan Daftar Pelajar');
		
		//TITLE
		$objPHPExcel->getActiveSheet()->setCellValue("A1", "LAPORAN DAFTAR PELAJAR");
		$objPHPExcel->getActiveSheet()->setCellValue("A2", "Bimbel Online ONEZIZ");
			$objPHPExcel->getActiveSheet()->mergeCells("A1:C1");
			$objPHPExcel->getActiveSheet()->mergeCells("A2:C2");
		$objPHPExcel->getActiveSheet()->getStyle("A1")->applyFromArray(array("font" => array( "bold" => true, 'size'  => 16)));
		$objPHPExcel->getActiveSheet()->getStyle("A2")->applyFromArray(array("font" => array( "bold" => true)));
			
		$num = 4;
		$objPHPExcel->getActiveSheet()->setCellValue("A$num", "NO");
		$objPHPExcel->getActiveSheet()->setCellValue("B$num", "NAMA LENGKAP");
		$objPHPExcel->getActiveSheet()->setCellValue("C$num", "EMAIL");
		$objPHPExcel->getActiveSheet()->setCellValue("D$num", "NAMA SEKOLAH");
		$objPHPExcel->getActiveSheet()->setCellValue("E$num", "KOTA SEKOLAH");
		$objPHPExcel->getActiveSheet()->setCellValue("F$num", "PROVINSI SEKOLAH");
		$objPHPExcel->getActiveSheet()->setCellValue("G$num", "PAKET BIMBEL");
		$objPHPExcel->getActiveSheet()->setCellValue("H$num", "TANGGAL PENDAFTARAN");
		$objPHPExcel->getActiveSheet()->getStyle("A4:H4")->applyFromArray(array("font" => array( "bold" => true)));
		$num++;
		
		
		$no=1;
		foreach($report as $row):
			$objPHPExcel->getActiveSheet()->setCellValue("A$num", $no);
			$objPHPExcel->getActiveSheet()->setCellValue("B$num", $row->fullname);
			$objPHPExcel->getActiveSheet()->setCellValue("C$num", $row->user_email);
			$objPHPExcel->getActiveSheet()->setCellValue("D$num", $row->school_name);
			$objPHPExcel->getActiveSheet()->setCellValue("E$num", $row->city_name);
			$objPHPExcel->getActiveSheet()->setCellValue("F$num", $row->province_name);
			$objPHPExcel->getActiveSheet()->setCellValue("G$num", $row->course_name);
			$objPHPExcel->getActiveSheet()->setCellValue("H$num", $row->order_date_start);
		$no++; $num++;
		endforeach;
		
		foreach(range('A','H') as $columnID) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
				->setAutoSize(true);
		}
		
		
		//EXPORT	
			$filename = "Laporan Daftar Siswa - " . $timestamp . '.xls'; //save our workbook as this file name
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			
	}
	
	public function download_pdf(){ 
		
		$report = $this->user_model->get_report($config['per_page'],$page,$this->input->post('report_province'), $this->input->post('report_city'), $this->input->post('report_school'), $this->input->post('report_date_start'), $this->input->post('report_date_end'));
		$timestamp = date("Y-m-d");
		
		$html = "<style> 
					body{margin:0;padding:0;font-family: 'Helvetica', 'Helvetica Neue', 'Calibri', 'Arial'; font-size: 11px;} 
					table tr td,table thead tr td, table tr th{ border-collapse: collapse; border-bottom: 1px solid #333;} 
					table thead tr td,table thead tr th,table tr th{ border-bottom: 2px solid #000; }
					h2{ margin-bottom:0;}
				</style>";
			
		$html .= "<h2>LAPORAN DAFTAR PELAJAR</h2>";
		$html .= "<b>Bimbel Online ONEZIZ</b><br/>";
		$html .= "<hr/><br/>";
		
		$html .= "<table width='100%' cellspacing='0' border='0'> ";            
		$html .= "<thead>";                  
		$html .= "<tr>";
		$html .= "<th width='30px' align='left'>NO</th>";
		$html .= "<th width='15%' align='left'>NAMA PELAJAR</th>";
		$html .= "<th width='15%' align='left'>EMAIL</th>";
		$html .= "<th align='left'>NAMA SEKOLAH</th>";
		$html .= "<th align='left'>KOTA SEKOLAH</th>";
		$html .= "<th align='left'>PROVINSI SEKOLAH</th>";
		$html .= "<th align='left'>PAKET BIMBEL</th>";
		$html .= "<th align='left'>TANGGAL PENDAFTARAN</th>";
		$html .= "</tr>";               
		$html .= "</thead>"; 
		$no = 1;		
		
		foreach($report as $row):
			$html .= "<tr>";
			$html .= "<td align='center'>$no</td>";
			$html .= "<td>$row->fullname</td>";
			$html .= "<td>$row->user_email</td>";
			$html .= "<td>$row->school_name</td>";
			$html .= "<td>$row->city_name</td>";
			$html .= "<td>$row->province_name</td>";
			$html .= "<td>$row->course_name</td>";
			$html .= "<td>$row->order_date_start</td>";
			$html .= "</tr>";
			$no++; 
		endforeach;
				
		$html .= "</table>";  
				
		
		$filename = "Laporan Daftar Siswa - " . $timestamp; //save our workbook as this file name
		$pdfFilePath = FCPATH."/downloads/report/$filename.pdf";
		
		$this->load->library('pdf');
		$pdf = $this->pdf->load();
		$pdf=new mPDF('utf-8', 'A4-L');
		$pdf->SetFooter("Bimbel Online ONEZIZ".'|{PAGENO}|'.$timestamp); 
		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($pdfFilePath, 'F'); // save to file 
		$pdffile = base_url()."downloads/report/$filename.pdf";
		redirect($pdffile, 'refresh');
	}
}