<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Customer Model
 * @module	Customer
 * @package	CMS
 * @author 	fikriwirawan
 * @since	10 Nov 2014
 */
 
class user_model extends MY_Model {

    protected $table        = 'tbl_user';
    protected $key          = 'user_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='')
	{
		if($search != '')
		{
			return $this->db->select('*')
							->from($this->table)
							->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
							->where('tbl_user.deleted','0')
							->where('tbl_user.user_level','2')
							->like('fullname',$search)
							->limit($limit,$offset)
							->order_by('fullname','asc')
							->get()
							->result();
		}else
		{		
			return $this->db->select('*')
						->from($this->table)
						->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
						->where('tbl_user.deleted','0')
						->where('tbl_user.user_level','2')
						->limit($limit,$offset)
						->order_by('fullname','asc')
						->get()
						->result();
		}
	}

	public function get_report($limit, $offset, $province='', $city='', $school='', $dtstart='', $dtend='')
	{
		if($province != '' && $city != '' && $school != '')
		{
			$this->db->select('*')
				->from($this->table)
				->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school')
				//->where('tbl_user.deleted','0')
				->where('tbl_user.user_level','2')
				->where('user_province',$province)
				->where('user_city',$city)
				->where('user_school',$school);

		}elseif($province != '' && $city != '')
		{
			$this->db->select('*')
				->from($this->table)
				->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school')
				//->where('tbl_user.deleted','0')
				->where('tbl_user.user_level','2')
				->where('user_province',$province)
				->where('user_city',$city);
		}elseif($province != '' && $school != '')
		{
			$this->db->select('*')
				->from($this->table)
				->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school')
				->where('tbl_user.deleted','0')
				->where('tbl_user.user_level','2')
				->where('user_province',$province)
				->where('user_school',$school);
		}elseif($province != '')
		{
			$this->db->select('*')
				->from($this->table)
				->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school')
				->where('tbl_user.deleted','0')
				->where('tbl_user.user_level','2')
				->where('user_province',$province);
		}elseif($city != '')
		{
			$this->db->select('*')
				->from($this->table)
				->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school')
				->where('tbl_user.deleted','0')
				->where('tbl_user.user_level','2')
				->where('user_city',$city);
		}else
		{
			$this->db->select('*')
				->from($this->table)
				->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school')
				//->where('tbl_user.deleted','0')
				->where('tbl_user.user_level','2');
		}

		//filter date
		if(trim($dtstart) != '' && trim($dtend) != ''){
			$this->db->where('order_date_start between "'.$dtstart.'" and "'.$dtend.'"');
		}

		/*if(trim($dtend) != ''){
			$this->db->where('order_date_end',$dtend);
		}*/

		return $this->db->limit($limit,$offset)
						->order_by('fullname','asc')
						->get()
						->result();
	}

	public function count_report($province,$city,$school='',$dtstart='', $dtend='')
	{
		if($province != '' && $city != '' && $school != '')
		{
			$this->db->select('count(*) as numrows')
				->from($this->table)
				->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->where('tbl_user.deleted','0')
				->where('tbl_user.user_level','2')
				->where('user_province',$province)
				->where('user_city',$city)
				->where('user_school',$school)
				->order_by('fullname','asc');
		}
		elseif($province != '' && $city != '')
		{
			$this->db->select('count(*) as numrows')
				->from($this->table)
				->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->where('tbl_user.deleted','0')
				->where('tbl_user.user_level','2')
				->where('user_province',$province)
				->where('user_city',$city)
				->order_by('fullname','asc');
		}elseif($province != '' && $school != '')
		{
			$this->db->select('count(*) as numrows')
				->from($this->table)
				->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->where('tbl_user.deleted','0')
				->where('tbl_user.user_level','2')
				->where('user_province',$province)
				->where('user_school',$school)
				->order_by('fullname','asc');
		}elseif($province != '')
		{
			$this->db->select('count(*) as numrows')
				->from($this->table)
				->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->where('tbl_user.deleted','0')
				->where('tbl_user.user_level','2')
				->where('user_province',$province)
				->order_by('fullname','asc');
		}elseif($city != '')
		{
			$this->db->select('count(*) as numrows')
				->from($this->table)
				->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->where('tbl_user.deleted','0')
				->where('tbl_user.user_level','2')
				->where('user_city',$city)
				->order_by('fullname','asc');
		}else
		{
			$this->db->select('count(*) as numrows')
				->from($this->table)
				->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->where('tbl_user.deleted','0')
				->where('tbl_user.user_level','2');
		}

		//filter date
		if(trim($dtstart) != '' && trim($dtend) != ''){
			$this->db->where('order_date_start between "'.$dtstart.'" and "'.$dtend.'"');
		}

		/*if(trim($dtend) != ''){
			$this->db->where('order_date_end',$dtend);
		}*/

		return $this->db->get()
						->row()
						->numrows;
	}
	
	public function count_all($search)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('deleted','0')
						->where('tbl_user.user_level','2')
						->like('fullname',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	
	public function get_all_user()
	{
		$this->db	->select('*')
					->where('tbl_user.deleted', 0)
					->where('tbl_user.user_level','2')
					->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level');
        return $this->db->get($this->table);    
    }
	
	public function get_user($id)
	{
		$this->db	->select('*')
					->where('tbl_user.deleted', 0)
					->where('user_id', $id);
        return $this->db->get($this->table);    
    }

	public function getProvince()
	{
		return $this->db->select("*")
			->from('tbl_province')
			->order_by('province_id','asc')
			->get()
			->result();
	}

	public function getCity()
	{
		return $this->db->select("*")
			->from('tbl_city')
			->order_by('city_id','asc')
			->get()
			->result();
	}

	public function getSchoolGrade()
	{
		return $this->db->select("*")
			->from('tbl_school_grade')
			->order_by('school_grade_id','asc')
			->get()
			->result();
	}

	public function getSchool()
	{
		return $this->db->select("*")
			->from('tbl_school')
			->order_by('school_id','asc')
			->get()
			->result();
	}
}