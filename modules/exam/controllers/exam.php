<?php

class Exam extends Front_Controller{
	
	private $per_page 	= '30';
	private $title 		= 'quiz';
	private $module 	= 'quiz';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('exam_model');
		$this->load->model('quiz_model');
		$this->load->model('subject_model');
		$this->load->model('score_model');
		$this->load->model('lesson_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect('exam/start');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}


	public function answer(){
		//if($this->session->userdata('logged_in'))
		//{
		$quiz = $this->exam_model->cek_answer($_POST['quiz_id']);
		//echo $_POST['answer'].$quiz->quiz_answer;

		if($quiz->quiz_answer == $_POST['answer'])  {

			$data = array(
	               'status' => 2, 
	               'answer' => $_POST['answer'], 
	        );
	        $this->db->where('playquiz_quiz_id', $quiz->quiz_id);
	        $this->db->where('playquiz_key', $_POST['key']);
			$this->db->update('tbl_playquiz', $data);
 	

			$options = array(
				'a'	=> $quiz->quiz_answer_a,
				'b'	=> $quiz->quiz_answer_b,
				'c'	=> $quiz->quiz_answer_c,
				'd'	=> $quiz->quiz_answer_d,
				'e'	=> $quiz->quiz_answer_e,
			);

			foreach ($options as $key => $value) {

				$check = null;
				if($_POST['answer'] == $key){
					$check = 'checked';
				}
				echo '<li>'.$key. '<input type="radio" value="'.$key.'" id="answer_exam" name="answer" '.$check.' readonly>'.$value.'</li>'; 
			}

			 

		} else {

			$data = array(
	               'status' => 1, 
	               'answer' => $_POST['answer'], 
	        );
	        $this->db->where('playquiz_quiz_id', $quiz->quiz_id);
	        $this->db->where('playquiz_key', $_POST['key']);
			$this->db->update('tbl_playquiz', $data);
 			
			$options = array(
				'a'	=> $quiz->quiz_answer_a,
				'b'	=> $quiz->quiz_answer_b,
				'c'	=> $quiz->quiz_answer_c,
				'd'	=> $quiz->quiz_answer_d,
				'e'	=> $quiz->quiz_answer_e,
			);

			foreach ($options as $key => $value) {
				
				$check = null;
				if($_POST['answer'] == $key){
					$check = 'checked';
				}
				echo '<li>'.ucfirst($key). '<input type="radio" value="'.$key.'" id="answer_exam" name="answer" '.$check.' readonly>'.$value.'</li>';
			}
			 
		}

		//} 
	}


	public function start(){

		$quiz = $this->exam_model->cek_quiz($this->session->userdata('user_id'));

		//if($quiz)	{
		//	redirect('exam/browse');

		//} else{
			if($this->save_quiz()){
				$this->session->set_flashdata('message', 'success|Success add new record');
				redirect('exam/browse');
			}

			$key = $this->generateRandomString();

			$subject = $this->subject_model->get_all_subject( $this->session->userdata('user_id') )->result();	

			$this->template	->set('menu_title', 'Pertanyaan')
						->set('menu_environment', 'active') 
						->set('subject', $subject) 
						->set('key', $key)
						->build('step-1-jwbquiz');
		//}
		 
	}

	public function generateRandomString($length = 10) {

	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	
	public function browse($page='0'){

		$quiz = $this->exam_model->cek_quiz($this->session->userdata('user_id'));
		$key = $this->exam_model->get_key();


		//if(!$quiz) {
		//	redirect('exam/raport/'.$key[0]->playquiz_key.'');
		//}

		if($this->session->userdata('logged_in'))
		{ 
			

			$data = $this->exam_model->get_all($key[0]->playquiz_key);
			$total_rows = $this->exam_model->count_all( $key[0]->playquiz_key );

			$this->template	->set('menu_title', 'Pertanyaan')
							->set('menu_environment', 'active')
							->set('total_rows',$total_rows)
							->set('data', $data)
							->set('no', $no) 
							->set('config', $config)
							->set('key', $key[0]->playquiz_key)
							->set('exam_id', $key[0]->playquiz_exam_id)
							->build('ujian');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}

	public function raport($key){ 

		if($this->session->userdata('logged_in'))
		{ 

			if($this->save_quiz()){ 
				redirect('exam/browse');
			}

			$total_rows = $this->exam_model->count_all();

			if( $total_rows != 0 ) {
				//redirect('exam/browse');
			} 
			$data = array(
	            'playquiz_end_time' => date('Y-m-d H:i:s'), 
	            'deleted' => 1, 
	        );
	        $this->db->where('playquiz_key', $key);
	        $this->db->where('playquiz_user_id', $this->session->userdata('user_id'));
			$this->db->update('tbl_playquiz', $data);


			$false = $this->exam_model->get_raport(1,$key);
			$true = $this->exam_model->get_raport(2, $key);	

			$total_rows = $this->exam_model->count_one($key);
			
			$data = $this->exam_model->get_subject($key);

			$result = $this->exam_model->get_all_result($key);


			$result_bagi = $true / $total_rows;
			$percent = round($result_bagi * 100 );
			$summary = $this->exam_model->get_raport(2, $key);
			$keyopt = $this->generateRandomString();

			/* Insert to Scroe Table */
			$cek_score = $this->score_model->cek_score($this->session->userdata('user_id'), $key);

			$total_soal = $true + $false;

			if(empty($cek_score)) {
				$score = array(
					'score_quiz_id'  	=> $data[0]->subject_id,
					'score_quiz_key'  	=> $key,
					'score_user'      	=> $this->session->userdata('user_id'), 
					'score_value'		=> $true / $total_soal  * 100,
					'score_correct'		=> $true,
					'score_wrong'		=> $false,
					'score_date'		=> $result[0]->created_on,
					'score_start_time'	=> $result[0]->playquiz_start_time,
					'score_end_time'	=> $result[0]->playquiz_end_time,
				);
				$this->score_model->insert($score);
			}
 

			$this->template	->set('menu_title', 'Pertanyaan')
							->set('menu_environment', 'active')
							->set('total_rows',$total_rows)
							->set('false', $false)
							->set('true', $true)
							->set('percent', $percent) 
							->set('summary', $summary) 
							->set('exam_id', $data[0]->playquiz_exam_id)
							->set('subject_id', $data[0]->subject_id)
							->set('keyopt', $keyopt)
							->set('data', $data)
							->set('result', $result)
							->build('raport');
		} 
	}


	private function save_quiz(){
		
		//set form validation
		$this->form_validation->set_rules('quiz', 'Pilih Quiz', 'required'); 
		$quiz = $this->quiz_model->get_quiz_subject($this->input->post('quiz'));
	
		//print_r($quiz);
		
		if($this->form_validation->run() === TRUE){ 

			foreach ($quiz->result() as $row) {
 				//process the form
				
				$data = array(
					'playquiz_quiz_id'  	=> $row->quiz_id,
					'playquiz_user_id'  	=> $this->session->userdata('user_id'),
					'playquiz_key'      	=> $this->input->post('random'), 
					'playquiz_exam_id'      => $this->input->post('quiz'), 
					//'playquiz_start_time'   => $date, 
				);
				 $this->exam_model->insert($data);
			}

			return true;
			
		}

	}
	 
	 
	 public function ujian(){

		$this->template	->set('menu_title', 'Ujian')
						->set('menu_environment', 'active') 
						->set('subject', $subject) 
						->set('key', $key)
						->build('ujian');
		 
	}
}