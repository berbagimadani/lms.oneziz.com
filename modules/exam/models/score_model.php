<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * subject Model
 * @module	subject
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class Score_model extends MY_Model {

    protected $table        = 'tbl_score';
    protected $key          = 'score_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }   

    public function cek_score($user_id, $key)
	{
		return $this->db->select('*')
				->from($this->table) 
				->where('score_user', $user_id) 
				->where('score_quiz_key', $key) 
				->get() 
				->result();
    }
	 
}