<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * subject Model
 * @module	subject
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class subject_model extends MY_Model {

    protected $table        = 'tbl_subject';
    protected $key          = 'subject_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='')
	{
		
		if($this->ion_auth->is_siswa()){
			return $this->db->select('*')
						->from($this->table)
						->join('tbl_lesson','tbl_lesson.lesson_id = tbl_subject.subject_lesson_id')
						->where('subject_lesson_id', $this->session->userdata('user_id'))
						->where('tbl_subject.deleted','0')
						->limit($limit,$offset)
						->order_by('subject_name','asc')
						->get()
						->result();
		}
		else{

			if($search != '')
			{
				return $this->db->select('*')
								->from($this->table)
								->join('tbl_lesson','tbl_lesson.lesson_id = tbl_subject.subject_lesson_id')
								//->where('tbl_customer.deleted','0')
								->where('tbl_subject.deleted','0')
								->like('subject_name',$search)
								->limit($limit,$offset)
								->order_by('subject_name','asc')
								->get()
								->result();
			}else
			{		
				return $this->db->select('*')
							->from($this->table)
							->join('tbl_lesson','tbl_lesson.lesson_id = tbl_subject.subject_lesson_id')
							->where('tbl_subject.deleted','0')
							->limit($limit,$offset)
							->order_by('subject_name','asc')
							->get()
							->result();
			}
		}
	}
	
	public function count_all($search)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('deleted','0')
						->like('subject_name',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	
		
	public function get_subject($id)
	{
		$this->db	->select('*')
					->where('tbl_subject.deleted', 0)
					->where('subject_id', $id);
        return $this->db->get($this->table);    
    }
	
		
	public function get_all_subject( $user_id )
	{
		if($this->ion_auth->is_siswa()){

			/*
			$this->db	->select('*')
						->join('tbl_lesson','tbl_lesson.lesson_id = tbl_subject.subject_lesson_id', 'left')
						->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id', 'left')
						->join('tbl_order','tbl_order.order_course = tbl_course.course_id', 'left') 
						->where('order_student', $user_id)
						->where('tbl_subject.deleted', 0);
	        return $this->db->get('tbl_subject');    */

	        return $this->db->select('*')
							->from('tbl_exam')
							->join('tbl_subject','tbl_subject.subject_id = tbl_exam.exam_subject_id')
							->join('tbl_lesson','tbl_lesson.lesson_id = tbl_subject.subject_lesson_id')
							->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
							->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_course.course_school_grade')
							->join('tbl_order','tbl_order.order_course = tbl_course.course_id')
							->where('tbl_order.order_student', $this->session->userdata('user_id'))
							//->where('subject_lesson_id', $this->session->userdata('user_id'))
							//->like('tbl_school_grade.school_grade_name', 'slta-ipa')
							->where('tbl_subject.deleted','0')
							//->limit($limit,$offset)
							->order_by('school_grade_name','asc')
							->order_by('lesson_name','asc')
							->order_by('subject_name','asc')
							->get(); 


    	} else{
 
	        return $this->db->select('*')
							->from('tbl_exam')
							->join('tbl_subject','tbl_subject.subject_id = tbl_exam.exam_subject_id')
							->join('tbl_lesson','tbl_lesson.lesson_id = tbl_subject.subject_lesson_id')
							->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
							->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_course.course_school_grade')
							->join('tbl_order','tbl_order.order_course = tbl_course.course_id')
							//->where('tbl_order.order_student', $this->session->userdata('user_id'))
							//->where('subject_lesson_id', $this->session->userdata('user_id'))
							//->like('tbl_school_grade.school_grade_name', 'slta-ipa')
							->where('tbl_subject.deleted','0')
							//->limit($limit,$offset)
							->order_by('school_grade_name','asc')
							->order_by('lesson_name','asc')
							->order_by('subject_name','asc')
							->get();

    	}
    }
	
	public function get_all_lesson()
	{
		$this->db	->select('*')
					->where('tbl_lesson.deleted', 0)
					->order_by('lesson_name','asc');
        return $this->db->get('tbl_lesson');    
    }
}