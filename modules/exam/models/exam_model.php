<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * subject Model
 * @module	subject
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class Exam_model extends MY_Model {

    protected $table        = 'tbl_playquiz';
    protected $key          = 'playquiz_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }  

    public function get_all($page=null)
	{
 		
 		if(empty($page)) {
 			
 			return $this->db->select('subject_id,subject_name,playquiz_id,playquiz_quiz_id, playquiz_key, answer, quiz_question, quiz_answer_a,quiz_answer_b,quiz_answer_c,quiz_answer_d,quiz_answer_e,quiz_answer')
				->from($this->table)
				->join('tbl_quiz tq','tq.quiz_id = tbl_playquiz.playquiz_quiz_id')
				->join('tbl_subject ts','ts.subject_id = tq.quiz_subject_id') 
				->where('playquiz_user_id', $this->session->userdata('user_id')) 
				->where('tbl_playquiz.deleted',0)
				->where('tbl_playquiz.playquiz_key', $page)
				->order_by("rand()")
				->order_by('playquiz_id','desc')
				->get()
				->result(); 

 		} else {
 			
 			return $this->db->select('subject_id,subject_name,playquiz_id,playquiz_quiz_id, playquiz_key, answer, quiz_question, quiz_answer_a,quiz_answer_b,quiz_answer_c,quiz_answer_d,quiz_answer_e,quiz_answer')
				->from($this->table)
				->join('tbl_quiz tq','tq.quiz_id = tbl_playquiz.playquiz_quiz_id')
				->join('tbl_subject ts','ts.subject_id = tq.quiz_subject_id') 
				->where('playquiz_user_id', $this->session->userdata('user_id')) 
				->where('tbl_playquiz.deleted', 0)
				->where('tbl_playquiz.playquiz_key', $page)
				->order_by("rand()")
				->order_by('playquiz_id','desc')
				->get()
				->result(); 

 		}
		
	}

	 public function get_all_result($key=null)
	{
 		 
 			return $this->db->select('subject_name,playquiz_id,playquiz_quiz_id, playquiz_key, answer, quiz_question, quiz_answer_a,quiz_answer_b,quiz_answer_c,quiz_answer_d,quiz_answer_e,quiz_answer,quiz_solution,tbl_playquiz.created_on,playquiz_start_time,playquiz_end_time')
				->from($this->table)
				->join('tbl_quiz tq','tq.quiz_id = tbl_playquiz.playquiz_quiz_id')
				->join('tbl_subject ts','ts.subject_id = tq.quiz_subject_id') 
				->where('playquiz_user_id', $this->session->userdata('user_id')) 
				->where('tbl_playquiz.deleted',1)
				->where('playquiz_key', $key)
				->get()
				->result();  
		
	}

	public function get_subject($key)
	{
 
		return $this->db->select('subject_id,subject_name,playquiz_exam_id,playquiz_id, playquiz_quiz_id, quiz_question, quiz_answer_a,quiz_answer_b,quiz_answer_c,quiz_answer_d,quiz_answer_e,quiz_answer')
				->from($this->table)
				->join('tbl_quiz tq','tq.quiz_id = tbl_playquiz.playquiz_quiz_id')
				->join('tbl_subject ts','ts.subject_id = tq.quiz_subject_id') 
				->where('playquiz_user_id', $this->session->userdata('user_id')) 
				->where('tbl_playquiz.deleted','1') 
				->where('playquiz_key', $key) 
				->limit(1,0)
				//->order_by("rand()")
				->order_by('playquiz_id','desc')
				->get()
				->result(); 
	}

	public function get_key()
	{
 
		return $this->db->select('playquiz_key,playquiz_exam_id')
				->from($this->table) 
				->where('playquiz_user_id', $this->session->userdata('user_id')) 
				->where('tbl_playquiz.deleted','0') 
				->limit(1,0) 
				->order_by('playquiz_id','desc')
				->get()
				->result(); 
	}

	public function get_raport($id, $key)
	{
 
		return $this->db->select('count(*) as numrows')
				->from($this->table) 
				->where('playquiz_user_id', $this->session->userdata('user_id')) 
				->where('tbl_playquiz.deleted','1') 
				->where('playquiz_key', $key) 
				->where('status', $id) 
				->get()
				->row()
				->numrows;
	}

	public function count_one($key)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table) 
						->where('tbl_playquiz.deleted','1') 
						->where('playquiz_key', $key) 
						->get()
						->row()
						->numrows;
	}
	
	public function count_all( $key=null )
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('deleted','0') 
						->where('playquiz_key', $key)
						->where('status', null)
						->get()
						->row()
						->numrows;
	}

	public function count_quiz($id)
	{
 		
 		/*
		return $this->db->select('count(*) as numrows')
								->from('tbl_quiz') 
								->where('quiz_subject_id', $id) 
								->where('tbl_quiz.deleted','0')   
								->get()
								->row()
								->numrows;*/

		return $this->db->select('count(*) as numrows')
					->from('tbl_quiz')
					->where('deleted', 0)
					->where('quiz_exam_id', $id)
					->get()
					->row()
					->numrows; 
	}

	public function cek_quiz($id)
	{

		return $this->db->select('*')
				->from($this->table) 
				->where('playquiz_user_id', $this->session->userdata('user_id')) 
				->where('tbl_playquiz.deleted','0')
				//->where('status', null) 
				->get() 
				->result();
		 
    }

    public function cek_answer($id)
	{
		return $this->db->select('*')
				->from('tbl_quiz') 
				->where('quiz_id', $id) 
				->where('tbl_quiz.deleted','0')
				->get() 
				->row();
		 
    }

    public function get_exam_pass($id){

    	return $this->db->select('*')
				->from('tbl_exam') 
				->where('tbl_exam.exam_id', $id)
				->where('tbl_exam.deleted', 0)
    			->get() 
				->result();
    }
	 
}