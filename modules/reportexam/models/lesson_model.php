<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * lesson Model
 * @module	lesson
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class lesson_model extends MY_Model {

    protected $table        = 'tbl_lesson';
    protected $key          = 'lesson_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='')
	{
		if($this->ion_auth->is_siswa()){
			return $this->db->select('*')
							->from($this->table)
							->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
							->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_course.course_school_grade')
							->join('tbl_order','tbl_order.order_course = tbl_course.course_id')
							->where('tbl_order.order_student', $this->session->userdata('user_id'))
							->where('tbl_lesson.deleted','0')
							->where('tbl_course.deleted','0')
							->where('tbl_order.deleted','0')
							->where('tbl_school_grade.deleted','0')
							->limit($limit,$offset)
							->order_by('school_grade_name','asc')
							->order_by('course_name','asc')
							->order_by('lesson_name','asc')
							->get()
							->result();
		}else{
			if($search != '')
			{ 
				return $this->db->select('*')
								->from($this->table)
								->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
								->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_course.course_school_grade')
								->where('tbl_customer.deleted','0')
								->where('tbl_course.deleted','0')
								->where('tbl_school_grade.deleted','0')
								->like('lesson_name',$search)
								->limit($limit,$offset)
								->order_by('school_grade_name','asc')
								->order_by('course_name','asc')
								->order_by('lesson_name','asc')
								->get()
								->result();
			}else
			{		
				return $this->db->select('*')
							->from($this->table)
							->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
							->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_course.course_school_grade')
							->where('tbl_lesson.deleted','0')
							->where('tbl_course.deleted','0')
							->where('tbl_school_grade.deleted','0')
							->limit($limit,$offset)								
							->order_by('school_grade_name','asc')
							->order_by('course_name','asc')
							->order_by('lesson_name','asc')
							->get()
							->result();
			}
		}
	}
	
	public function count_all($search)
	{
		return $this->db->select("count(*) as numrows")
						->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
						->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_course.course_school_grade')
						->from($this->table)
						->where('tbl_lesson.deleted','0')
						->where('tbl_course.deleted','0')
						->where('tbl_school_grade.deleted','0')
						->like('lesson_name',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	
		
	public function get_lesson($id)
	{
		$this->db	->select('*')
					->where('tbl_lesson.deleted', 0)
					->where('lesson_id', $id);
        return $this->db->get($this->table);    
    }
	
		
	public function get_all_lesson()
	{
		$this->db	->select('*')
					->where('tbl_lesson.deleted', 0);
        return $this->db->get('tbl_lesson');    
    }
	
	public function get_all_course()
	{
		$this->db	->select('*')
					->where('tbl_course.deleted', 0)
					->order_by('course_name','asc');
        return $this->db->get('tbl_course');    
    }
}