<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * subject Model
 * @module	subject
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class Score_model extends MY_Model {

    protected $table        = 'tbl_score';
    protected $key          = 'score_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }   

    public function cek_score($user_id, $key)
	{
		return $this->db->select('*')
				->from($this->table) 
				->where('score_user', $user_id) 
				->where('score_quiz_key', $key) 
				->get() 
				->result();
    }
    public function get_report_exam($limit, $offset, $province='', $city='', $school='', $score_start='', $score_end='')
	{
		 	 
		if($province != '' && $city != '' && $school != '')
		{  	
			$this->db->select('*')
				->from($this->table)
				->join('tbl_user','tbl_user.user_id = tbl_score.score_user') 
				->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_user.user_school_grade') 
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_subject','tbl_subject.subject_id = tbl_score.score_quiz_id')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city', 'left')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school', 'left')  
				->where('user_province',$province)
				->where('user_city',$city)
				->where('user_school',$school);

		}
		elseif($province != '' && $city != '')
		{ 	
			
			$this->db->select('*')
				->from($this->table)
				->join('tbl_user','tbl_user.user_id = tbl_score.score_user') 
				->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_user.user_school_grade') 
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_subject','tbl_subject.subject_id = tbl_score.score_quiz_id')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city', 'left')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school', 'left')  
				->where('user_province',$province)
				->where('user_city',$city);

		}elseif($province != '' && $school != '')
		{
			
			$this->db->select('*')
				->from($this->table)
				->join('tbl_user','tbl_user.user_id = tbl_score.score_user') 
				->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_user.user_school_grade') 
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_subject','tbl_subject.subject_id = tbl_score.score_quiz_id')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city', 'left')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school', 'left')  
				->where('user_province',$province)
				->where('user_school',$school);

		}elseif($province != '')
		{	

			$this->db->select('*')
				->from($this->table)
				->join('tbl_user','tbl_user.user_id = tbl_score.score_user')
				->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_user.user_school_grade')  
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_subject','tbl_subject.subject_id = tbl_score.score_quiz_id')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city', 'left')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school', 'left')  
				->where('user_province',$province);

		}elseif($city != '')
		{
			$this->db->select('*')
				->from($this->table)
				->join('tbl_user','tbl_user.user_id = tbl_score.score_user') 
				->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_user.user_school_grade') 
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_subject','tbl_subject.subject_id = tbl_score.score_quiz_id')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city', 'left')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school', 'left')  
				->where('user_city',$city);
		}
		else
		{

			$this->db->select('*')
				->from($this->table)
				->join('tbl_user','tbl_user.user_id = tbl_score.score_user') 
				->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_user.user_school_grade') 
				->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_subject','tbl_subject.subject_id = tbl_score.score_quiz_id')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city', 'left')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school', 'left');
		}

		//filter date
		if(trim($score_start) != ''){
			$this->db->where('score_value between "'.$score_start.'" and "'.$score_end.'"');
		}

		return $this->db->limit($limit,$offset)
						->order_by('fullname','asc')
						->get()
						->result();

	}

	public function count_report($province='', $city='', $school='', $score_start='', $score_end='')
	{


		 if($province != '' && $city != '' && $school != '')
		{  	
			$this->db->select('*')
				->from($this->table)
				->join('tbl_user','tbl_user.user_id = tbl_score.score_user') 
				//->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				//->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_subject','tbl_subject.subject_id = tbl_score.score_quiz_id')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city', 'left')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school', 'left')  
				->where('user_province',$province)
				->where('user_city',$city)
				->where('user_school',$school);

		}
		elseif($province != '' && $city != '')
		{ 	
			
			$this->db->select('*')
				->from($this->table)
				->join('tbl_user','tbl_user.user_id = tbl_score.score_user') 
				//->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				//->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_subject','tbl_subject.subject_id = tbl_score.score_quiz_id')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city', 'left')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school', 'left')  
				->where('user_province',$province)
				->where('user_city',$city);

		}elseif($province != '' && $school != '')
		{
			
			$this->db->select('*')
				->from($this->table)
				->join('tbl_user','tbl_user.user_id = tbl_score.score_user') 
				//->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				//->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_subject','tbl_subject.subject_id = tbl_score.score_quiz_id')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city', 'left')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school', 'left')  
				->where('user_province',$province)
				->where('user_school',$school);

		}elseif($province != '')
		{	

			$this->db->select('*')
				->from($this->table)
				->join('tbl_user','tbl_user.user_id = tbl_score.score_user') 
				//->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				//->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_subject','tbl_subject.subject_id = tbl_score.score_quiz_id')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city', 'left')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school', 'left')  
				->where('user_province',$province);

		}elseif($city != '')
		{
			$this->db->select('*')
				->from($this->table)
				->join('tbl_user','tbl_user.user_id = tbl_score.score_user') 
				//->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				//->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_subject','tbl_subject.subject_id = tbl_score.score_quiz_id')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city', 'left')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school', 'left')  
				->where('user_city',$city);
		}
		else
		{

			$this->db->select('*')
				->from($this->table)
				->join('tbl_user','tbl_user.user_id = tbl_score.score_user') 
				//->join('tbl_order','tbl_order.order_student = tbl_user.user_id')
				//->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
				->join('tbl_subject','tbl_subject.subject_id = tbl_score.score_quiz_id')
				->join('tbl_city','tbl_city.city_id = tbl_user.user_city', 'left')
				->join('tbl_province','tbl_province.province_id = tbl_user.user_province')
				->join('tbl_school','tbl_school.school_id = tbl_user.user_school', 'left');
		}

		//filter date
		if(trim($score_start) != ''){
			$this->db->where('score_value between "'.$score_start.'" and "'.$score_end.'"');
		}

		return $this->db->get()
						->row()
						->numrows;

	}

}