<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * subject Model
 * @module	subject
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class formexam_model extends MY_Model {

    protected $table        = 'tbl_exam';
    protected $key          = 'exam_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='')
	{
		
		if($search != '') {
			return $this->db->select('*')
						->from($this->table)
						->join('tbl_subject','tbl_subject.subject_id = tbl_exam.exam_subject_id') 
						->where('tbl_exam.deleted','0')
						->limit($limit,$offset)
						->order_by('exam_title','asc')
						->get()
						->result();
		}
		else{

			return $this->db->select('*')
								->from($this->table)
								->join('tbl_subject','tbl_subject.subject_id = tbl_exam.exam_subject_id')  
								->where('tbl_exam.deleted','0')
								->like('exam_title',$search)
								->limit($limit,$offset)
								->order_by('exam_title','asc')
								->get()
								->result();
		}
	}
	
	public function count_all($search)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('deleted','0')
						->like('exam_title',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	
		
	public function get_subject($id)
	{
		$this->db	->select('*')
					->where('tbl_exam.deleted', 0)
					->where('exam_id', $id);
        return $this->db->get($this->table);    
    }

    public function get_subject_lesson($id)
	{
		$this->db	->select('*')
					->where('tbl_subject.deleted', 0)
					->where('subject_id', $id);
        return $this->db->get('tbl_subject');    
    }
		
	public function get_all_subject()
	{
		$this->db	->select('*')
					->where('tbl_subject.deleted', 0);
        return $this->db->get('tbl_subject');    
    }
	
	 
}