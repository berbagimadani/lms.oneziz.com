<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * subject Model
 * @module	subject
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class subject_model extends MY_Model {

    protected $table        = 'tbl_subject';
    protected $key          = 'subject_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='')
	{
		
		if($this->ion_auth->is_siswa()){
			return $this->db->select('*')
						->from($this->table)
						->join('tbl_lesson','tbl_lesson.lesson_id = tbl_subject.subject_lesson_id')
						->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
						->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_course.course_school_grade')
						->join('tbl_order','tbl_order.order_course = tbl_course.course_id')
						->where('tbl_order.order_student', $this->session->userdata('user_id'))
						//->where('subject_lesson_id', $this->session->userdata('user_id'))
						->where('tbl_subject.deleted','0')
						->limit($limit,$offset)
						->order_by('school_grade_name','asc')
						->order_by('lesson_name','asc')
						->order_by('subject_name','asc')
						->get()
						->result();
		}
		else{

			if($search != '')
			{
				return $this->db->select('*')
								->from($this->table)
								->join('tbl_lesson','tbl_lesson.lesson_id = tbl_subject.subject_lesson_id')
								//->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_lesson.lesson_course_id')
								->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
								->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_course.course_school_grade')
								//->where('tbl_customer.deleted','0')
								->where('tbl_subject.deleted','0')
								->like('subject_name',$search)
								->limit($limit,$offset)
								->order_by('school_grade_name','asc')
								->order_by('lesson_name','asc')
								->order_by('subject_name','asc')
								->get()
								->result();
			}else
			{		
				return $this->db->select('*')
							->from('tbl_subject')
							->join('tbl_lesson','tbl_lesson.lesson_id = tbl_subject.subject_lesson_id')
							->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
							->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_course.course_school_grade')
							->where('tbl_subject.deleted','0')
							->limit($limit,$offset)
							->order_by('school_grade_name','asc')
							->order_by('lesson_name','asc')
							->order_by('subject_name','asc')
							->get()
							->result();
			}
		}
	}
	
	public function count_all($search)
	{
		if($this->ion_auth->is_siswa()){
			return $this->db->select("count(*) as numrows")	
							->from($this->table)
							->join('tbl_lesson','tbl_lesson.lesson_id = tbl_subject.subject_lesson_id')
							->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
							->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_course.course_school_grade')
							->join('tbl_order','tbl_order.order_course = tbl_course.course_id')
							->where('tbl_order.order_student', $this->session->userdata('user_id'))
							->get()
							->row()
							->numrows;
		}else{
			return $this->db->select("count(*) as numrows")
							->from($this->table)
								->join('tbl_lesson','tbl_lesson.lesson_id = tbl_subject.subject_lesson_id')
								->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
							->where('tbl_subject.deleted','0')
							->like('subject_name',$search)
							//->or_like('content',$search)
							->get()
							->row()
							->numrows;
		}
	}
	
		
	public function get_subject($id)
	{
		$this->db	->select('*')
					->where('tbl_subject.deleted', 0)
					->where('subject_id', $id);
        return $this->db->get($this->table);    
    }
	
		
	public function get_all_subject()
	{
		$this->db	->select('*')
					->where('tbl_subject.deleted', 0);
        return $this->db->get('tbl_subject');    
    }
	
	public function get_all_lesson()
	{
		$this->db	->select('*')
					->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
					->where('tbl_lesson.deleted', 0)
					->order_by('course_name','asc')
					->order_by('lesson_name','asc');
        return $this->db->get('tbl_lesson');    
    }

	public function getCourse()
	{
		return $this->db->select("*")
			->from('tbl_course')
			->order_by('course_id','asc')
			->get()
			->result();
	}
}