<?php

class Formexam extends Front_Controller{
	
	private $per_page 	= '30';
	private $title 		= 'Form Exam';
	private $module 	= 'formexam';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('formexam_model');
		$this->load->model('subject_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){

		if($this->session->userdata('logged_in'))
		{
			redirect('formexam/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->formexam_model->count_all($this->input->post('q'));
			
			//pagination
			$config['base_url']     = site_url('subject/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = $this->per_page; 
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(3);
			
			$data = $this->formexam_model->get_all($config['per_page'], $no, $this->input->post('q'));		
			
			
			$this->template	->set('menu_title', 'Setting Exam')
							->set('menu_environment', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('formexam');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function create(){
		if($this->save_exam()){
			$this->session->set_flashdata('message', 'success|Success add new record');
			redirect('formexam');
		}
					
		$lesson = $this->subject_model->get_all_lesson()->result();
		$course = $this->subject_model->getCourse();

		$subject = $this->formexam_model->get_all_subject()->result();
		
		$this->template	->set('menu_title', 'Setting Exam')
						->set('lesson', $lesson)
						->set('course', $course)
						->set('subject', $subject)
						->set('menu_subtitle', 'Form Penambahan Exam')
						->build('formexam_edit');		
	}
	
	public function edit($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_exam()){
			$this->session->set_flashdata('message', 'success|Success edit record');
			redirect('formexam');
		}
		
		//Get Specific Data
		$data = $this->formexam_model->get_subject($id)->result();
		$data = $data[0];
		
		$lesson = $this->subject_model->get_all_lesson()->result();

		$subject_lesson = $this->formexam_model->get_subject_lesson($data->exam_subject_id)->result();
		$subject_lesson = $subject_lesson[0];

		//print_r($subject_lesson->subject_lesson_id);

		foreach($lesson as $row){
			if($row->lesson_id == $subject_lesson->subject_lesson_id){
				$q_course_id = $row->lesson_course_id;
			}
		}
		$course = $this->subject_model->getCourse();


		$subject = $this->formexam_model->get_all_subject()->result();
		
		$this->template	->set('data', $data)
						->set('lesson', $lesson)
						->set('course', $course)
						->set('q_course_id', $q_course_id)
						->set('subject_lesson', $subject_lesson)
						->set('subject', $subject)
						->set('menu_title', 'Setting Subjek')
						->set('menu_subtitle', 'Form Edit Subjek')
						->build('formexam_edit');		
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(3);
		if($this->formexam_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Success delete row');
			redirect('formexam');
			exit;
		}
	}	
	
	private function save_exam(){
		
		//set form validation
		$this->form_validation->set_rules('subject_exam', 'Nama Subjek', 'required');
		$this->form_validation->set_rules('exam_title', 'Exam Title', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('exam_id');			
			
			//process the form
			$data = array(
					'exam_subject_id'      	=> $this->input->post('subject_exam'), 
					'exam_title'       		=> $this->input->post('exam_title'),
			);
				
			if(!$id){
				return $this->formexam_model->insert($data);
			}else{
				return $this->formexam_model->update($id, $data);
			} 
		}
	}
}