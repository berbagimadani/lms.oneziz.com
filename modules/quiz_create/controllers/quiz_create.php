<?php

class Quiz_create extends Front_Controller{
	
	private $per_page 	= '30';
	private $title 		= 'Create Quiz';
	private $module 	= 'quiz_create';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('quiz_model');
		$this->load->model('subject_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){

		if($this->session->userdata('logged_in'))
		{
			redirect('quiz_create/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0'){
		if($this->session->userdata('logged_in'))
		{
			if ( $this->uri->segment(3) == 'slta-ipa'){
				$grade = "SLTA IPA";
				$grade_title = "SLTA IPA/MIA";
			}elseif ( $this->uri->segment(3) == 'slta-ips'){
				$grade = "SLTA IPS";
				$grade_title = "SLTA IPS/IIS";
			}elseif ( $this->uri->segment(3) == 'sltp'){
				$grade = "SLTP";
				$grade_title = "SLTP";
			}
			
			$total_rows = $this->quiz_model->count_all($grade);
			
			//pagination
			$config['base_url']     = site_url('quiz_create/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = $this->per_page; 
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(4);
			
			
			
			$data = $this->quiz_model->get_all($config['per_page'], $no, $this->input->post('q'),$grade);		
			
			
			$this->template	->set('menu_title', 'Pengaturan Daftar Soal Latihan '.$grade_title)
							->set('submenu_title', 'Daftar Soal Latihan')
							->set('menu_quiz_create', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('quiz_list');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function create(){
		if($this->save_exam()){
			$this->session->set_flashdata('message', 'success|Data berhasil ditambahkan');
			redirect('quiz_create');
		}
		
		
		$course = $this->subject_model->getCourse();
		$lesson = $this->subject_model->get_all_lesson()->result();
		$subject = $this->quiz_model->get_all_subject()->result();
		
		$this->template	->set('menu_title', 'Penambahan Daftar Spesifikasi Soal Latihan')
						->set('lesson', $lesson)
						->set('course', $course)
						->set('subject', $subject)
						->set('menu_subtitle', 'Formulir Penambahan Daftar Spesifikasi Soal Latihan')
						->build('quiz_create');		
	}
	
	public function edit($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_exam()){
			$this->session->set_flashdata('message', 'success|Data berhasil diubah');
			redirect('quiz_create');
		}
		
		//Get Specific Data
		$data = $this->quiz_model->get_subject($id)->result();
		$data = $data[0];
		
		$lesson = $this->subject_model->get_all_lesson()->result();

		$subject_lesson = $this->quiz_model->get_subject_lesson($data->exam_subject_id)->result();
		$subject_lesson = $subject_lesson[0];

		//print_r($subject_lesson->subject_lesson_id);

		foreach($lesson as $row){
			if($row->lesson_id == $subject_lesson->subject_lesson_id){
				$q_course_id = $row->lesson_course_id;
			}
		}
		$course = $this->subject_model->getCourse();


		$subject = $this->quiz_model->get_all_subject()->result();
		
		$this->template	->set('data', $data)
						->set('lesson', $lesson)
						->set('course', $course)
						->set('q_course_id', $q_course_id)
						->set('subject_lesson', $subject_lesson)
						->set('subject', $subject)
						->set('menu_title', 'Pengubahan Daftar Spesifikasi Soal Latihan')
						->set('menu_subtitle', 'Formulir Penambahan Daftar Spesifikasi Soal Latihan')
						->build('quiz_create');		
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(3);
		if($this->quiz_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Data berhasil dihapus');
			redirect('quiz_create');
			exit;
		}
	}	
	
	private function save_exam(){
		
		//set form validation
		$this->form_validation->set_rules('subject_exam', 'Nama Subjek', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('exam_id');			
			
			//process the form
			$data = array(
					'exam_subject_id'      	=> $this->input->post('subject_exam'), 
					'exam_pass'       		=> $this->input->post('exam_pass'),
					'exam_frequency'       		=> $this->input->post('exam_frequency'),
					'exam_time'       		=> $this->input->post('exam_time'),
					'exam_time_notification'       		=> $this->input->post('exam_time_notification'),
			);
				
			if(!$id){
				return $this->quiz_model->insert($data);
			}else{
				return $this->quiz_model->update($id, $data);
			} 
		}
	}
}