<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * subject Model
 * @module	Quiz
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class quiz_model extends MY_Model {

    protected $table        = 'tbl_exam';
    protected $key          = 'exam_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
	/*
	public function get_all($limit, $offset, $search='')
	{
		
		if($search != '') {
			return $this->db->select('*')
						->from($this->table)
						->join('tbl_subject','tbl_subject.subject_id = tbl_exam.exam_subject_id') 
						->where('tbl_exam.deleted','0')
						->limit($limit,$offset)
						->order_by('exam_title','asc')
						->get()
						->result();
		}
		else{

			return $this->db->select('*')
								->from($this->table)
								->join('tbl_subject','tbl_subject.subject_id = tbl_exam.exam_subject_id')  
								->where('tbl_exam.deleted','0')
								->like('exam_title',$search)
								->limit($limit,$offset)
								->order_by('exam_title','asc')
								->get()
								->result();
		}
	}
	
	public function count_all($search)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('deleted','0')
						->like('exam_title',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	*/
	
	public function get_all($limit, $offset, $search='', $grade, $lesson=null, $opt=null)
	{
		
		

			if($this->ion_auth->is_siswa()){
				return $this->db->select('*')
							->from($this->table)
									->join('tbl_subject','tbl_subject.subject_id = tbl_exam.exam_subject_id')
							->join('tbl_lesson','tbl_lesson.lesson_id = tbl_subject.subject_lesson_id')
							->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
							->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_course.course_school_grade')
							->join('tbl_order','tbl_order.order_course = tbl_course.course_id')
							->where('tbl_order.order_student', $this->session->userdata('user_id'))
							//->where('subject_lesson_id', $this->session->userdata('user_id'))
							->like('tbl_school_grade.school_grade_name', $grade)
							->where('tbl_subject.deleted','0')
							->limit($limit,$offset)
							->order_by('school_grade_name','asc')
							->order_by('lesson_name','asc')
							->order_by('subject_name','asc')
							->get()
							->result();
			}
			else{
				return $this->db->select('*')
									->from($this->table)
									->join('tbl_subject','tbl_subject.subject_id = tbl_exam.exam_subject_id')
									->join('tbl_lesson','tbl_lesson.lesson_id = tbl_subject.subject_lesson_id')
									//->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_lesson.lesson_course_id')
									->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
									->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_course.course_school_grade')
									//->where('tbl_customer.deleted','0')
									->where('tbl_subject.deleted','0') 
									->like('tbl_school_grade.school_grade_name', $grade)
									//->where("(tbl_course.course_id ='$grade' and tbl_lesson.lesson_course_id='$lesson')", NULL, FALSE) 
									->limit($limit,$offset)
									->order_by('school_grade_name','asc')
									->order_by('lesson_name','asc')
									->order_by('subject_name','asc')
									->get()
									->result();
			}
	}
	
	public function count_all($grade)
	{
		if($this->ion_auth->is_siswa()){
			return $this->db->select("count(*) as numrows")	
							->from($this->table)
							->join('tbl_subject','tbl_subject.subject_id = tbl_exam.exam_subject_id')
							->join('tbl_lesson','tbl_lesson.lesson_id = tbl_subject.subject_lesson_id')
							->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
							->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_course.course_school_grade')
							->join('tbl_order','tbl_order.order_course = tbl_course.course_id')
							->where('tbl_order.order_student', $this->session->userdata('user_id'))
							->get()
							->row()
							->numrows;
		}else{
			return $this->db->select("count(*) as numrows")
							->from($this->table)
							->join('tbl_subject','tbl_subject.subject_id = tbl_exam.exam_subject_id')
							->join('tbl_lesson','tbl_lesson.lesson_id = tbl_subject.subject_lesson_id')
							->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
							->join('tbl_school_grade','tbl_school_grade.school_grade_id = tbl_course.course_school_grade')
							->where('tbl_subject.deleted','0') 
							->like('tbl_school_grade.school_grade_name', $grade)
							->get()
							->row()
							->numrows;
		}
	}
		
	public function get_subject($id)
	{
		$this->db	->select('*')
					->where('tbl_exam.deleted', 0)
					->where('exam_id', $id);
        return $this->db->get($this->table);    
    }

    public function get_subject_lesson($id)
	{
		$this->db	->select('*')
					->where('tbl_subject.deleted', 0)
					->where('subject_id', $id);
        return $this->db->get('tbl_subject');    
    }
		
	public function get_all_subject()
	{
		$this->db	->select('*')
					->where('tbl_subject.deleted', 0);
        return $this->db->get('tbl_subject');    
    }
	
	public function get_exam_by_subject($id)
	{
		$this->db	->select('*')
					->where('tbl_exam.deleted', 0)
					->where('exam_subject_id', $id);
        return $this->db->get('tbl_exam');    
    }
	
	public function get_question_list($id)
	{
		$this->db	->select('*')
					->where('deleted', 0)
					->where('quiz_exam_id', $id);
        return $this->db->get('tbl_quiz');    
    }
}