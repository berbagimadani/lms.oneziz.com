<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * subject Model
 * @module	subject
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class quizcustom_model extends MY_Model {

    protected $table        = 'tbl_quiz_custom';
    protected $key          = 'quiz_custom_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='')
	{
		if($search != '')
		{
			return $this->db->select('*')
							->from($this->table)
							//->where('tbl_customer.deleted','0')
							->where('tbl_quiz_custom.deleted','0')
							->like('quiz_custom_correct',$search)
							->limit($limit,$offset)
							->order_by('quiz_custom_correct','asc')
							->get()
							->result();
		}else
		{		
			return $this->db->select('*')
						->from($this->table)
						->where('tbl_quiz_custom.deleted','0')
						->limit($limit,$offset)
						->order_by('quiz_custom_correct','asc')
						->get()
						->result();
		}
	}
	
	public function count_all($search)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('deleted','0')
						->like('quiz_custom_correct',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	
		
	public function get_quiz($id)
	{
		$this->db	->select('*')
					->where('tbl_quiz_custom.deleted', 0)
					->where('quiz_custom_id', $id);
        return $this->db->get($this->table);    
    }
	
		
	public function get_all_quiz()
	{
		$this->db	->select('*')
					->where('tbl_quiz_custom.deleted', 0);
        return $this->db->get('tbl_quiz_custom');
    }
	
}