<?php

class Quizcustom extends Front_Controller{
	
	private $per_page 	= '30';
	private $title 		= 'quiz custom';
	private $module 	= 'quizcustom';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('quizcustom_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect('quizcustom/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->quizcustom_model->count_all($this->input->post('q'));
			
			//pagination
			$config['base_url']     = site_url('quizcustom/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = $this->per_page; 
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(3);
			
			$data = $this->quizcustom_model->get_all($config['per_page'], $no, $this->input->post('q'));
			
			
			$this->template	->set('menu_title', 'Custom Quiz')
							->set('menu_environment', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('quiz');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function create(){
		if($this->save_quiz()){
			$this->session->set_flashdata('message', 'success|Data berhasil ditambahkan');
			redirect('quizcustom');
		}

		$this->template	->set('menu_title', 'Pengaturan Custom Quiz')
						->set('menu_subtitle', 'Form Penambahan Custom Quiz')
						->build('quiz_edit');		
	}
	
	public function edit($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_quiz()){
			$this->session->set_flashdata('message', 'success|Data berhasil diubah');
			redirect('quizcustom');
		}
		
		//Get Specific Data
		$data = $this->quizcustom_model->get_quiz($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'Pengaturan Custom Quiz')
						->set('menu_subtitle', 'Form Pengaturan Custom Quiz')
						->build('quiz_edit');
	}
	
	public function view($id){
		$id =  $this->uri->segment(3);
		
		//Get Specific data
		$data = $this->quizcustom_model->get_quiz($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'Lihat Custom Quiz')
						->set('menu_subtitle', 'Lihat Custom Quiz')
						->build('quiz_edit');	
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(3);
		if($this->quizcustom_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Data berhasil dihapus');
			redirect('quizcustom');
			exit;
		}
	}	
	
	private function save_quiz(){
		
		//set form validation
		$this->form_validation->set_rules('quiz_custom_correct', 'Correct', 'required');
		$this->form_validation->set_rules('quiz_custom_wrong', 'Wrong ', 'required');
		$this->form_validation->set_rules('quiz_custom_status', 'Status', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('quiz_custom_id');
			
			//process the form
			$data = array(
				'quiz_custom_correct'     => $this->input->post('quiz_custom_correct'),
				'quiz_custom_wrong'   => $this->input->post('quiz_custom_wrong'),
				'quiz_custom_status'		=> $this->input->post('quiz_custom_status'),
			);
				
			if(!$id){
				return $this->quizcustom_model->insert($data);
			}else{
				return $this->quizcustom_model->update($id, $data);
			} 
		}
	}
}