<?php

class Finance extends Front_Controller{
	
	private $per_page 	= '15';
	private $title 		= 'Finance';
	private $module 	= 'finance';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('finance_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect($this->module.'/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->finance_model->count_all($this->input->post('q'));
			
			//pagination
			$config['base_url']     = site_url($this->module.'/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = 30; 
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(3);
			
			$cash = $this->finance_model->get_all($this->input->post('q'));		
			
			
			$this->template	->set('menu_title', 'Transaksi Keuangan')
							->set('menu_finance', 'active')
							->set('material_total',$config['total_rows'])
							->set('cash', $cash)
							->set('no', $no)
							->set('config', $config)
							->build('finance');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function create(){
		if($this->save_finance()){
			$this->session->set_flashdata('message', 'success|Data berhasil ditambahkan');
			redirect($this->module.'/');
		}
					
			
		$this->template	->set('menu_title', 'Tambah Data Transaksi')
						->set('menu_subtitle', 'Formulir Pencatatan Transaksi')
						->set('menu_database', 'active')
						->build('finance_edit');		
	}
	
	public function edit($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_finance()){
			$this->session->set_flashdata('message', 'success|Data berhasil diubah');
			redirect($this->module.'/');
		}
		
		//Get Specific finance
		$data = $this->finance_model->get_cash($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'Ubah Data Transaksi')
						->set('menu_subtitle', 'Formulir Pencatatan Transaksi')
						->set('menu_database', 'active')
						->build('finance_edit');	
	}
	
	public function view($id){
		$id =  $this->uri->segment(3);
		
		//Get Specific finance
		$data = $this->finance_model->get_finance($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'View finance')
						->set('menu_subtitle', 'View finance')
						->set('menu_database', 'active')
						->build('finance_view');	
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(3);
		if($this->finance_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Success delete row');
			redirect($this->module.'/browse');
			exit;
		}
	}	
	
	private function save_finance(){
		
		//set form validation
		$this->form_validation->set_rules('cash_remark', 'Keterangan', 'required');
		$this->form_validation->set_rules('cash_date', 'Tanggal Transaksi', 'required');
		$this->form_validation->set_rules('cash_total', 'Nominal', 'required');
		$this->form_validation->set_rules('cash_flow', 'Arus Transaksi', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('cash_id');			
			
			//process the form
			$data = array(
					'cash_date'       	=> $this->input->post('cash_date'),
					'cash_remark'      => $this->input->post('cash_remark'),
					'cash_total'       	=> $this->input->post('cash_total'),
					'cash_flow'       	=> $this->input->post('cash_flow'),
					'cash_reference'       => $this->input->post('cash_reference'),
			);
				
			if(!$id){
				return $this->finance_model->insert($data);
			}else{
				return $this->finance_model->update($id, $data);
			} 
		}
	}
}