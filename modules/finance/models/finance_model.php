<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Customer Model
 * @module	Customer
 * @package	CMS
 * @author 	fikriwirawan
 * @since	10 Nov 2014
 */
 
class finance_model extends MY_Model {

    protected $table        = 'tbl_cash';
    protected $key          = 'cash_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='')
	{
		if($search != '')
		{
			return $this->db->select('*')
							->from($this->table)
							->where('tbl_cash.deleted','0')
							->like('cash_remark',$search)
							->limit($limit,$offset)
							->order_by('cash_date','desc')
							->get()
							->result();
		}else
		{		
			return $this->db->select('*')
						->from($this->table)
						->where('tbl_cash.deleted','0')
						->limit($limit,$offset)
						->order_by('cash_date','desc')
						->get()
						->result();
		}
	}
	
	public function count_all($search)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('deleted','0')
						->like('cash_remark',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	
	public function get_all_cash()
	{
		$this->db	->select('*')
					->where('tbl_cash.deleted', 0);
        return $this->db->get($this->table);    
    }
	
	public function get_cash($id)
	{
		$this->db	->select('*')
					->where('tbl_cash.deleted', 0)
					->where('cash_id', $id);
        return $this->db->get($this->table);    
    }
}