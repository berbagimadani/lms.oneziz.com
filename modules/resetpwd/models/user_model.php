<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * User Model
 * @module	User
 * @package	ONEZIZ
 * @author 	fikriwirawan
 * @since	10 Nov 2014
 */
 
class user_model extends MY_Model {

    protected $table        = 'tbl_user';
    protected $key          = 'user_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	
	
	public function get_user($email)
	{
		$this->db	->select('user_id, fullname, user_uniquecode')
					->where('tbl_user.deleted', 0)
					->where('user_email', $email);
        return $this->db->get($this->table);    
    }
	
	
	public function get_user_by_code($code)
	{
		$this->db	->select('user_id, username, user_email, fullname, user_uniquecode')
					->where('tbl_user.deleted', 0)
					->where('user_uniquecode', $code)
					->where('user_resetpwd', '1');
        return $this->db->get($this->table);    
    }
}