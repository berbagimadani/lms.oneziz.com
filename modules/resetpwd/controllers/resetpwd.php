<?php

class Resetpwd extends Front_Controller{
	
	private $per_page 	= '15';
	private $title 		= 'Resetpwd';
	private $module 	= 'resetpwd';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		$this->load->library('pagination');	
	}
	
	public function index(){
		if($this->reset_pwd()){
			$this->session->set_flashdata('message', 'Silakan cek email anda untuk melakukan reset password.');
			redirect($this->module.'/');
		}
		
				
		$this->template	->set('site_title', 'Lupa Password')
						->set_layout('signin')
						->build('resetpwd');
	}
	
	public function reset(){
		$user_code = $this->uri->segment(3);		
		$user_detail = $this->user_model->get_user_by_code($user_code)->result();
		$user_detail = $user_detail[0];
		$uid = $user_detail->user_id;
		
		
		if($uid){
			//Generate New Pasword
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < 8; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			$pwd = $randomString;
			
			//Send Email
				$this->load->library('email');
				$config = Array(
					'protocol' => 'smtp',
					'smtp_host' => 'mail.oneziz.com',
					'smtp_port' => '25',
					'smtp_user' => 'admin@oneziz.com', // change it to yours
					'smtp_pass' => 'Kotak123', // change it to yours
					'mailtype' => 'html',
					'charset' => 'utf-8',
					'wordwrap' => FALSE,
					'newline' => "\r\n"
				);

				$this->email->initialize($config);

				$this->email->from('info@oneziz.com','Oneziz Bimbel Online'); // change it to yours
				$this->email->to("$user_detail->user_email","$user_detail->fullname");// change it to yours
				$this->email->subject('Reset Password LMS Oneziz Berhasil');

				$messagebody = '';
				$messagebody .= 'Halo '.$user_detail->fullname.', <br/><br/>';
				$messagebody .= 'Password LMS Oneziz Anda berhasil direset. Berikut detail info untuk mengakses LMS Oneziz Anda:<br/><br/>';
				$messagebody .= 'Username: '.$user_detail->username.'<br/>';
				$messagebody .= 'Password: '.$pwd.'<br/><br/>';
				$messagebody .= '<a href="http://lms.oneziz.com/">Login LMS Oneziz</a><br/><br/>';
				$messagebody .= 'Terima Kasih<br/><br/>';
				$messagebody .= 'Salam,<br/>';
				$messagebody .= 'Oneziz Bimbel Online';

				$this->email->message($messagebody);
				if($this->email->send())
				{
					//update tbl_user
					$data = array( 
						'password'       => md5($pwd),
						'user_resetpwd'  => '0'
					);
					$this->user_model->update($uid, $data);
					$msg = "Password anda telah kami reset. Silakan cek email anda untuk melihat detail cara akses ke LMS Oneiz. Terima Kasih";
				}
				else
				{
					$msg = "Maaf password anda gagal direset. Silakan lakukan reset password kembali atau kontak kami (info@oneziz.com) untuk reset password. Terima Kasih";
				}
	
					
		}
		
		$this->template	->set('site_title', 'Lupa Password')
						->set_layout('signin')
						->set('msg', $msg)
						->build('resetpwd_success');
	}
	
	private function reset_pwd(){
		
		//set form validation
		$this->form_validation->set_rules('user_email', 'Email', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$user_email = $this->input->post('user_email'); 
			$user_detail = $this->user_model->get_user($user_email)->result();
			$user_detail = $user_detail[0];
			$uid = $user_detail->user_id;
			
			//Send Email
			if($uid){
				$this->load->library('email');
				$config = Array(
					'protocol' => 'smtp',
					'smtp_host' => 'mail.oneziz.com',
					'smtp_port' => '25',
					'smtp_user' => 'admin@oneziz.com', // change it to yours
					'smtp_pass' => 'Kotak123', // change it to yours
					'mailtype' => 'html',
					'charset' => 'utf-8',
					'wordwrap' => FALSE,
					'newline' => "\r\n"
				);

				$this->email->initialize($config);

				$this->email->from('info@oneziz.com','Oneziz Bimbel Online'); // change it to yours
				$this->email->to("$user_email","$user_detail->fullname");// change it to yours
				$this->email->subject('Reset Password LMS Oneziz');

				$messagebody = '';
				$messagebody .= 'Halo '.$user_detail->fullname.', <br/><br/>';
				$messagebody .= 'Anda melakukan permintaan reset password LMS Oneziz. Silakan klik link di bawah ini untuk melakukan reset password. <br/><br/>';
				$messagebody .= '<a href="http://lms.oneziz.com/index.php/resetpwd/reset/'.$user_detail->user_uniquecode.'">http://lms.oneziz.com/index.php/resetpwd/reset/'.$user_detail->user_uniquecode.'</a><br/><br/>';
				$messagebody .= 'Terima Kasih<br/><br/>';
				$messagebody .= 'Salam,<br/>';
				$messagebody .= 'Oneziz Bimbel Online';

				$this->email->message($messagebody);
				if($this->email->send())
				{
					//update tbl_user
					$data = array( 
						'user_resetpwd'       => '1'
					);
					
					return $this->user_model->update($uid, $data);
				}
				else
				{
					return false;
				}
			}
			
			
		}
	}



}