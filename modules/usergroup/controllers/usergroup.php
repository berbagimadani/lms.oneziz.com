<?php

class Usergroup extends Front_Controller{
	
	private $per_page 	= '30';
	private $title 		= 'Usergroup';
	private $module 	= 'usergroup';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('usergroup_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect('usergroup/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->usergroup_model->count_all($this->input->post('q'));
			
			//pagination
			$config['base_url']     = site_url('browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = 40; 
			$config['uri_segment']  = 4;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(4);
			
			$data = $this->usergroup_model->get_all($config['per_page'], $no, $this->input->post('q'));		
			
			
			$this->template	->set('menu_title', 'Pengaturan Kelompok Pemakai')
							->set('menu_environment', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('usergroup');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function create(){
		if($this->save_usergroup()){
			$this->session->set_flashdata('message', 'success|Data berhasil ditambahkan');
			redirect('usergroup/browse');
		}
					
			
		$this->template	->set('menu_title', 'Pengaturan Kelompok Pemakai')
						->set('menu_subtitle', 'Formulir Penambahan Kelompok Pemakai')
						->build('usergroup_edit');		
	}
	
	public function edit($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_usergroup()){
			$this->session->set_flashdata('message', 'success|Data berhasil diubah');
			redirect('usergroup/browse');
		}
		
		//Get Specific Data
		$data = $this->usergroup_model->get_usergroup($id)->result();
		$data = $data[0];
		
		
		$this->template	->set('data', $data)
						->set('usergroup', $usergroup)
						->set('menu_title', 'Pengaturan Kelompok Pemakai')
						->set('menu_subtitle', 'Formulir Pengaturan Kelompok Pemakai')
						->build('usergroup_edit');		
	}
	
	public function view($id){
		$id =  $this->uri->segment(3);
		
		//Get Specific data
		$data = $this->usergroup_model->get_usergroup($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'View')
						->set('menu_subtitle', 'View')
						->build('usergroup_edit');	
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(3);
		if($this->usergroup_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Data berhasil dihapus');
			redirect('usergroup/browse');
			exit;
		}
	}	
	
	private function save_usergroup(){
		
		//set form validation
		$this->form_validation->set_rules('usergroup_name', 'Nama Kelompok Pemakai', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('usergroup_id');			
			
			//process the form
			$data = array(
					'usergroup_name'       	=> $this->input->post('usergroup_name')
			);
				
			if(!$id){
				return $this->usergroup_model->insert($data);
			}else{
				return $this->usergroup_model->update($id, $data);
			} 
		}
	}
}