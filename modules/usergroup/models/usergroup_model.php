<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * usergroup Model
 * @module	usergroup
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class usergroup_model extends MY_Model {

    protected $table        = 'tbl_usergroup';
    protected $key          = 'usergroup_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='')
	{
		if($search != '')
		{
			return $this->db->select('*')
							->from($this->table)
							->where('tbl_usergroup.deleted','0')
							->like('usergroup_name',$search)
							->limit($limit,$offset)
							->order_by('usergroup_name','asc')
							->get()
							->result();
		}else
		{		
			return $this->db->select('*')
						->from($this->table)
						->where('tbl_usergroup.deleted','0')
						->limit($limit,$offset)
						->order_by('usergroup_name','asc')
						->get()
						->result();
		}
	}
	
	public function count_all($search)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('deleted','0')
						->like('usergroup_name',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	
		
	public function get_usergroup($id)
	{
		$this->db	->select('*')
					->where('tbl_usergroup.deleted', 0)
					->where('usergroup_id', $id);
        return $this->db->get($this->table);    
    }
	
		
	public function get_all_usergroup()
	{
		$this->db	->select('*')
					->where('tbl_usergroup.deleted', 0);
        return $this->db->get('tbl_usergroup');    
    }
}