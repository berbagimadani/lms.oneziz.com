<?php

class Quiz extends Front_Controller{
	
	private $per_page 	= '30';
	private $title 		= 'quiz';
	private $module 	= 'quiz';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('quiz_model');
		$this->load->model('subject_model');
		$this->load->model('formexam_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect('quiz/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->quiz_model->count_all($this->input->post('q'));
			
			//pagination
			$config['base_url']     = site_url('quiz/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = $this->per_page; 
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(3);
			
			$data = $this->quiz_model->get_all($config['per_page'], $no, $this->input->post('q'));		
			
			
			$this->template	->set('menu_title', 'Pengaturan Latihan Soal')
							->set('menu_environment', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('quiz');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function create(){
		if($this->save_quiz()){
			$this->session->set_flashdata('message', 'success|Data berhasil ditambahkan');
			redirect('quiz');
		}
					
		$lesson = $this->subject_model->get_all_lesson()->result();
		$course = $this->subject_model->getCourse();
 
		$subject = $this->quiz_model->get_all_subject()->result();

		$exam = $this->quiz_model->get_all_exam()->result();
		
		$this->template	->set('menu_title', 'Pengaturan Latihan Soal')
						->set('lesson', $lesson)
						->set('course', $course)
						->set('subject', $subject)
						->set('exam', $exam)
						->set('menu_subtitle', 'Formulir Penambahan Latihan Soal')
						->build('quiz_edit');		
	}
	
	public function edit($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_quiz()){
			$this->session->set_flashdata('message', 'success|Data berhasil diubah');
			redirect('quiz');
		}
		
		//Get Specific Data
		$data = $this->quiz_model->get_quiz($id)->result();
		$data = $data[0];

		$lesson = $this->subject_model->get_all_lesson()->result();

		echo $data->quiz_subject_id;
		
		$subject_lesson = $this->formexam_model->get_subject_lesson($data->quiz_subject_id)->result();
		$subject_lesson = $subject_lesson[0];
 		
 		foreach($lesson as $row){
			if($row->lesson_id == $subject_lesson->subject_lesson_id){
				$q_course_id = $row->lesson_course_id;
			}
		}

		$course = $this->subject_model->getCourse();
		$subject  = $this->quiz_model->get_all_subject()->result();
		
		$exam = $this->quiz_model->get_all_exam()->result();

		$this->template	->set('data', $data)
						->set('lesson', $lesson)
						->set('course', $course)
						->set('q_course_id', $q_course_id)
						->set('subject_lesson', $subject_lesson)
						->set('subject', $subject )
						->set('exam', $exam)
						->set('menu_title', 'Pengaturan Latihan Soal')
						->set('menu_subtitle', 'Form Pengaturan Latihan Soal')
						->build('quiz_edit');		
	}
	
	public function view($id){
		$id =  $this->uri->segment(3);
		
		//Get Specific data
		$data = $this->quiz_model->get_quiz($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'Lihat Latihan Soal')
						->set('menu_subtitle', 'Lihat Latihan Soal')
						->build('quiz_edit');	
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(3);
		if($this->quiz_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Data berhasil dihapus');
			redirect('quiz');
			exit;
		}
	}	
	
	private function save_quiz(){
		
		//set form validation
		$this->form_validation->set_rules('quiz_question', 'pertanyaan', 'required');
		$this->form_validation->set_rules('quiz_subject_id', 'Mata Pelajaran', 'required');
		$this->form_validation->set_rules('quiz_exam_id', 'Exam Title', 'required');
		$this->form_validation->set_rules('quiz_answer_a', 'Jawaban Pertanyaan A', 'required');
		$this->form_validation->set_rules('quiz_answer_b', 'Jawaban Pertanyaan B', 'required');
		$this->form_validation->set_rules('quiz_answer_c', 'Jawaban Pertanyaan C', 'required');
		$this->form_validation->set_rules('quiz_answer_d', 'Jawaban Pertanyaan D', 'required');
		$this->form_validation->set_rules('quiz_answer_e', 'Jawaban Pertanyaan E', 'required');
		$this->form_validation->set_rules('quiz_answer', 'Jawaban ', 'required');
		$this->form_validation->set_rules('quiz_solution', 'Quiz Pembahasan', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('quiz_id');			
			
			//process the form
			$data = array(
				'quiz_question'     => $this->input->post('quiz_question'),
				'quiz_subject_id'   => $this->input->post('quiz_subject_id'),
				'quiz_exam_id'   	=> $this->input->post('quiz_exam_id'),
				'quiz_answer_a'		=> $this->input->post('quiz_answer_a'),
				'quiz_answer_b'		=> $this->input->post('quiz_answer_b'),
				'quiz_answer_c'		=> $this->input->post('quiz_answer_c'),
				'quiz_answer_d'		=> $this->input->post('quiz_answer_d'),
				'quiz_answer_e'		=> $this->input->post('quiz_answer_e'),
				'quiz_answer'		=> $this->input->post('quiz_answer'),
				'quiz_solution'		=> $this->input->post('quiz_solution'),
			);
				
			if(!$id){
				return $this->quiz_model->insert($data);
			}else{
				return $this->quiz_model->update($id, $data);
			} 
		}
	}
}