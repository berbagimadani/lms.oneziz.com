<?php

class User extends Front_Controller{
	
	private $per_page 	= '15';
	private $title 		= 'User';
	private $module 	= 'user';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect($this->module.'/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->user_model->count_all($this->input->post('q'));
			
			//pagination
			$config['base_url']     = site_url($this->module.'/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = 30; 
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(3);
			
			$customer = $this->user_model->get_all($this->input->post('q'));
			
			
			$this->template	->set('menu_title', 'Pengaturan Pemakai')
							->set('menu_database', 'active')
							->set('material_total',$config['total_rows'])
							->set('user', $customer)
							->set('no', $no)
							->set('config', $config)
							->build('user');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function create(){
		if($this->save_user()){
			$this->session->set_flashdata('message', 'success|Data berhasil ditambahkan');
			redirect($this->module.'/');
		}
/*
		$userlevel = array(
			array("id"=>"1","name"=>"Administrator"),
			array("id"=>"2","name"=>"Siswa"),
			array("id"=>"3","name"=>"Teacher"),
			array("id"=>"4","name"=>"Finance"),
			array("id"=>"5","name"=>"Guest"),
		);*/
		
		$group = $this->user_model->get_all_usergroup()->result();
		$province = $this->user_model->getProvince();
		$city     = $this->user_model->getCity();
			
		$this->template	->set('menu_title', 'Pengaturan Pemakai')
						->set('menu_subtitle', 'Formulir Penambahan Pemakai')
						->set('menu_database', 'active')
						->set('group', $group)
						->set('province', $province)
						->set('city', $city)
						->build('user_create');
	}
	
	public function edit($id){
		$id =  $this->uri->segment(3);
		
		if($this->edit_user()){
			$this->session->set_flashdata('message', 'success|Data berhasil diubah');
			redirect('user/');
		}



		$province = $this->user_model->getProvince();
		$city     = $this->user_model->getCity();
		$group = $this->user_model->get_all_usergroup()->result();
		 
		//Get Specific customer
		$data = $this->user_model->get_user($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('province', $province)
						->set('city', $city)
						->set('group', $group)
						->set('action', 'edit')
						->set('menu_title', 'Pengaturan Pemakai')
						->set('menu_subtitle', 'Formulir Pengaturan Pemakai')
						->set('menu_database', 'active')
						->set('user_level', $userlevel)
						->build('user_edit');
	}

    public function chpass($id){
        $id = $this->session->userdata('user_id');

        if($this->do_chpass()){
            $this->session->set_flashdata('message', 'Password Berhasil Diubah');
            redirect($this->module.'/chpass');
        }

        //Get Specific customer
        $data = $this->user_model->get_user($id)->result();
        $data = $data[0];

        $this->template	->set('data', $data)
            ->set('menu_title', 'Ubah Password')
            ->set('menu_subtitle', 'Formulir Ubah Password')
            ->set('menu_database', 'active')
            ->set('user_level', $userlevel)
            ->build('user_chpass');
    }
	
	public function view($id){
		$id =  $this->uri->segment(3);
		
		//Get Specific customer
		$data = $this->user_model->get_user($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'Lihat Data Pemakai')
						->set('menu_subtitle', 'Formulir Data Pemakai')
						->set('menu_database', 'active')
						->build('user_view');
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(3);
		if($this->user_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Data berhasil dihapus');
			redirect($this->module.'/browse');
			exit;
		}
	}	
	
	private function save_user(){
		
		//set form validation
		$this->form_validation->set_rules('fullname', 'Nama Pemakai', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('user_email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('repassword', 'Confirm Password', 'required');
		$this->form_validation->set_rules('user_level', 'Kelompok Pemakai', 'required');

	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('user_id');
			
			//process the form
			$data = array(
					'username'       	=> $this->input->post('username'),
					'fullname'       	=> $this->input->post('fullname'),
					'password'       	=> md5($this->input->post('password')),
					'user_email'       	=> $this->input->post('user_email'),
					'user_level'       	=> $this->input->post('user_level'),
					'user_phone'       	=> $this->input->post('user_phone'),
					'user_mobile'       => $this->input->post('user_mobile'),
					'user_address'      => $this->input->post('user_address'),
					'user_city'       	=> $this->input->post('user_city'),
					'user_province'     => $this->input->post('user_province'),
					'user_zipcode'      => $this->input->post('user_zipcode'),
			);
				
			if(!$id){
				return $this->user_model->insert($data);
			}else{
				return $this->user_model->update($id, $data);
			} 
		}
	}
	
	
	private function edit_user(){
		
		//set form validation
		$this->form_validation->set_rules('fullname', 'Nama Pemakai', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('user_email', 'Email', 'required');
		$this->form_validation->set_rules('user_level', 'Kelompok Pemakai', 'required');

	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('user_id');
			
			//process the form
			$data = array(
					'username'       	=> $this->input->post('username'),
					'fullname'       	=> $this->input->post('fullname'),
					'user_email'       	=> $this->input->post('user_email'),
					'user_level'       	=> $this->input->post('user_level'),
					'user_phone'       	=> $this->input->post('user_phone'),
					'user_mobile'       => $this->input->post('user_mobile'),
					'user_address'      => $this->input->post('user_address'),
					'user_city'       	=> $this->input->post('user_city'),
					'user_province'     => $this->input->post('user_province'),
					'user_zipcode'      => $this->input->post('user_zipcode'),
			);
				
			if(!$id){
				return $this->user_model->insert($data);
			}else{
				return $this->user_model->update($id, $data);
			} 
		}
	}
	
    private function do_chpass(){

        //set form validation
        $this->form_validation->set_rules('password', 'Password', 'required');

        if($this->form_validation->run() === TRUE){
            $id = $this->session->userdata('user_id');

            //process the form
            $data = array(
                'password'       	=> md5($this->input->post('password'))
            );

            if(!$id){
                //todo: return error
            }else{
                return $this->user_model->update($id, $data);
            }
        }
    }
}