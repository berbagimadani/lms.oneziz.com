<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Customer Model
 * @module	Customer
 * @package	CMS
 * @author 	fikriwirawan
 * @since	10 Nov 2014
 */
 
class user_model extends MY_Model {

    protected $table        = 'tbl_user';
    protected $key          = 'user_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='')
	{
		if($search != '')
		{
			return $this->db->select('*')
							->from($this->table)
							->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
							->join('tbl_city','tbl_city.city_id = tbl_user.user_city')
							->where('tbl_user.deleted','0')
							->where('tbl_user.user_level != 2')
							->like('fullname',$search)
							->limit($limit,$offset)
							->order_by('fullname','asc')
							->get()
							->result();
		}else
		{		
			return $this->db->select('*')
						->from($this->table)
						->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
						->join('tbl_city','tbl_city.city_id = tbl_user.user_city')
						->where('tbl_user.deleted','0')
						->where('tbl_user.user_level != 2')
						->limit($limit,$offset)
						->order_by('fullname','asc')
						->get()
						->result();
		}
	}
	
	public function count_all($search)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level')
						->join('tbl_city','tbl_city.city_id = tbl_user.user_city')
						->where('tbl_user.deleted','0')
						->where('tbl_user.user_level != 2')
						->like('fullname',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	
	public function get_all_user()
	{
		$this->db	->select('*')
					->where('tbl_user.deleted', 0)
					->join('tbl_usergroup','tbl_usergroup.usergroup_id = tbl_user.user_level');
        return $this->db->get($this->table);    
    }
	
	public function get_user($id)
	{
		$this->db	->select('*')
					->where('tbl_user.deleted', 0)
					->where('user_id', $id);
        return $this->db->get($this->table);    
    }

	public function getProvince()
	{
		return $this->db->select("*")
			->from('tbl_province')
			->order_by('province_id','asc')
			->get()
			->result();
	}

	public function getCity()
	{
		return $this->db->select("*")
			->from('tbl_city')
			->order_by('city_id','asc')
			->get()
			->result();
	}
	
	public function get_all_usergroup()
	{ 
		$this->db	->select('*')
					->where('tbl_usergroup.deleted', 0);
        return $this->db->get('tbl_usergroup');    
    }
	
}