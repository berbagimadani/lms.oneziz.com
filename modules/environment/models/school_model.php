<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * School Model
 * @module	school
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class school_model extends MY_Model {

    protected $table        = 'tbl_school_grade';
    protected $key          = 'school_grade_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='')
	{
		if($search != '')
		{
			return $this->db->select('*')
							->from($this->table)
							->where('deleted','0')
							->like('school_grade_name',$search)
							->limit($limit,$offset)
							->order_by('school_grade_name','asc')
							->get()
							->result();
		}else
		{		
			return $this->db->select('*')
						->from($this->table)
						->where('tbl_school_grade.deleted','0')
						->limit($limit,$offset)
						->order_by('school_grade_name','asc')
						->get()
						->result();
		}
	}
	
	public function count_all($search)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('deleted','0')
						->like('school_grade_name',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	
		
	public function get_school($id)
	{
		$this->db	->select('*')
					->where('tbl_school_grade.deleted', 0)
					->where('school_grade_id', $id);
        return $this->db->get($this->table);    
    }
	
		
	public function get_all_school()
	{
		$this->db	->select('*')
					->where('tbl_school_grade.deleted', 0);
        return $this->db->get('tbl_school_grade');    
    }
}