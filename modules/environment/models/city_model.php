<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * City Model
 * @module	City
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class city_model extends MY_Model {

    protected $table        = 'tbl_city';
    protected $key          = 'city_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='')
	{
		if($search != '')
		{
			return $this->db->select('*')
							->from($this->table)
							->join('tbl_province','tbl_province.province_id = tbl_city.city_province_id')
							->where('tbl_customer.deleted','0')
							->like('city_name',$search)
							->limit($limit,$offset)
							->order_by('city_name','asc')
							->get()
							->result();
		}else
		{		
			return $this->db->select('*')
						->from($this->table)
						->join('tbl_province','tbl_province.province_id = tbl_city.city_province_id')
						->where('tbl_city.deleted','0')
						->limit($limit,$offset)
						->order_by('city_name','asc')
						->get()
						->result();
		}
	}
	
	public function count_all($search)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('deleted','0')
						->like('city_name',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	
		
	public function get_city($id)
	{
		$this->db	->select('*')
					->where('tbl_city.deleted', 0)
					->where('city_id', $id);
        return $this->db->get($this->table);    
    }
	
		
	public function get_all_province()
	{
		$this->db	->select('*')
					->where('tbl_province.deleted', 0);
        return $this->db->get('tbl_province');    
    }
}