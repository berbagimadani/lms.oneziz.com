<?php

class Province extends Front_Controller{
	
	private $per_page 	= '30';
	private $title 		= 'Province';
	private $module 	= 'province';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('province_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect('environment/province/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->province_model->count_all($this->input->post('q'));
			
			//pagination
			$config['base_url']     = site_url('environment/province/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = 40; 
			$config['uri_segment']  = 4;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(4);
			
			$data = $this->province_model->get_all($config['per_page'], $no, $this->input->post('q'));		
			
			
			$this->template	->set('menu_title', 'Pengaturan Provinsi')
							->set('menu_environment', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('province/province');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function create(){
		if($this->save_province()){
			$this->session->set_flashdata('message', 'success|Data berhasil ditambahkan');
			redirect('environment/province');
		}
					
			
		$this->template	->set('menu_title', 'Pengaturan Provinsi')
						->set('menu_subtitle', 'Form Penambahan Provinsi')
						->build('province/province_edit');		
	}
	
	public function edit($id){
		$id =  $this->uri->segment(4);
		
		if($this->save_province()){
			$this->session->set_flashdata('message', 'success|Data berhasil diubah');
			redirect('environment/province');
		}
		
		//Get Specific Data
		$data = $this->province_model->get_province($id)->result();
		$data = $data[0];
		
		
		$this->template	->set('data', $data)
						->set('province', $province)
						->set('menu_title', 'Pengaturan Provinsi')
						->set('menu_subtitle', 'Form Pengaturan Provinsi')
						->build('province/province_edit');		
	}
	
	public function view($id){
		$id =  $this->uri->segment(4);
		
		//Get Specific data
		$data = $this->province_model->get_province($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'View')
						->set('menu_subtitle', 'View')
						->build('province_edit');	
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(4);
		if($this->province_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Data berhasil dihapus');
			redirect('environment/province');
			exit;
		}
	}	
	
	private function save_province(){
		
		//set form validation
		$this->form_validation->set_rules('province_name', 'Nama Provinsi', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('province_id');			
			
			//process the form
			$data = array(
					'province_name'       	=> $this->input->post('province_name')
			);
				
			if(!$id){
				return $this->province_model->insert($data);
			}else{
				return $this->province_model->update($id, $data);
			} 
		}
	}
}