<?php

class City extends Front_Controller{
	
	private $per_page 	= '30';
	private $title 		= 'City';
	private $module 	= 'city';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('city_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect('environment/city/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->city_model->count_all($this->input->post('q'));
			
			//pagination
			$config['base_url']     = site_url('environment/city/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = 30; 
			$config['uri_segment']  = 4;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(4);
			
			$data = $this->city_model->get_all($config['per_page'], $no, $this->input->post('q'));		
			
			
			$this->template	->set('menu_title', 'Pengaturan Kota')
							->set('menu_environment', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('city/city');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function create(){
		if($this->save_city()){
			$this->session->set_flashdata('message', 'success|Data berhasil ditambahkan');
			redirect('environment/city');
		}
					
		$province = $this->city_model->get_all_province()->result();
		$this->template	->set('menu_title', 'Pengaturan Kota')
						->set('province', $province)
						->set('menu_subtitle', 'Form Penambahan Kota')
						->build('city/city_edit');		
	}
	
	public function edit($id){
		$id =  $this->uri->segment(4);
		
		if($this->save_city()){
			$this->session->set_flashdata('message', 'success|Data berhasil diubah');
			redirect('environment/city');
		}
		
		//Get Specific Data
		$data = $this->city_model->get_city($id)->result();
		$data = $data[0];
		
		$province = $this->city_model->get_all_province()->result();
		
		$this->template	->set('data', $data)
						->set('province', $province)
						->set('menu_title', 'Edit Kota')
						->set('menu_subtitle', 'Form Edit Kota')
						->build('city/city_edit');		
	}
	
	public function view($id){
		$id =  $this->uri->segment(4);
		
		//Get Specific data
		$data = $this->city_model->get_customer($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'View')
						->set('menu_subtitle', 'View')
						->build('city_edit');	
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(4);
		if($this->city_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Data berhasil dihapus');
			redirect('environment/city');
			exit;
		}
	}	
	
	private function save_city(){
		
		//set form validation
		$this->form_validation->set_rules('city_name', 'Nama Kota', 'required');
		$this->form_validation->set_rules('city_province', 'Nama Provinsi', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('city_id');			
			
			//process the form
			$data = array(
					'city_name'       	=> $this->input->post('city_name'),
					'city_province_id'  => $this->input->post('city_province'),
					'city_zipcode'      => $this->input->post('city_zipcode'),
					'city_type'       	=> $this->input->post('city_type'),
			);
				
			if(!$id){
				return $this->city_model->insert($data);
			}else{
				return $this->city_model->update($id, $data);
			} 
		}
	}
}