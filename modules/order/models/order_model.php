<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * lesson Model
 * @module	lesson
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class order_model extends MY_Model {

    protected $table        = 'tbl_order';
    protected $key          = 'order_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='', $school_grade)
	{

		if($this->ion_auth->is_siswa()){
			return $this->db->select('course_name, order_id, a.user_id, a.fullname, b.fullname as teller, order_date_start, order_date_end, order_price, order_pay_date, order_status')
							->from($this->table)
							->join('tbl_user a','a.user_id = tbl_order.order_student')
							->join('tbl_user b','b.user_id = tbl_order.order_teller')
							->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
							->where('order_student', $this->session->userdata('user_id'))
							->where('order_status', '1')
							->where('tbl_order.deleted','0')
							->limit($limit,$offset)
							->order_by('order_student','asc')
							->get()
							->result();
		}
		else{

			if($search != '')
			{
				return $this->db->select('*')
								->from($this->table)
								->join('tbl_user','tbl_user.user_id = tbl_order.order_student')
								->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
								->where('tbl_order.deleted','0')
								//->like('lesson_name',$search)
								->limit($limit,$offset)
								->order_by('order_student','asc')
								->get()
								->result();
			}else
			{		
				return $this->db->select('course_name, order_id, a.user_id, a.fullname, a.user_email, a.user_mobile, 
							school_grade_name, school_name, city_name, 
							b.fullname as teller, order_date_start, order_date_end, order_price, order_pay_date, order_status')
							->from($this->table)
							->join('tbl_user a','a.user_id = tbl_order.order_student')
							->join('tbl_user b','b.user_id = tbl_order.order_teller')
							->join('tbl_school_grade','tbl_school_grade.school_grade_id = a.user_school_grade')
							->join('tbl_school','tbl_school.school_id = a.user_school')
							->join('tbl_city','tbl_city.city_id = tbl_school.school_city')
							->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
							->where('tbl_order.deleted','0')
							->like('school_grade_name', $school_grade)
							->limit($limit,$offset)
							->order_by('order_student','asc')
							->get()
							->result();
			}

		}
	}
	
	public function count_all($search,$school_grade)
	{
		return $this->db->select("count(*) as numrows")
						->join('tbl_user a','a.user_id = tbl_order.order_student')
						->join('tbl_user b','b.user_id = tbl_order.order_teller')
						->join('tbl_school_grade','tbl_school_grade.school_grade_id = a.user_school_grade')
						->join('tbl_school','tbl_school.school_id = a.user_school')
						->join('tbl_city','tbl_city.city_id = tbl_school.school_city')
						->join('tbl_course','tbl_course.course_id = tbl_order.order_course')
						->from($this->table)
						->where('tbl_order.deleted','0')
						->like('order_student',$search)
							->like('school_grade_name', $school_grade)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	
		
	public function get_order($id)
	{
		$this->db	->select('*')
					->where('tbl_order.deleted', 0)
					->where('order_id', $id);
        return $this->db->get($this->table);    
    }
	
		
	public function get_all_order()
	{
		$this->db	->select('*')
					->where('tbl_order.deleted', 0);
        return $this->db->get('tbl_order');    
    }
	
	public function get_all_user()
	{
		$this->db	->select('*')
					->where('tbl_user.deleted', 0)
					->where('user_level', 2)
					->order_by('fullname','asc');
        return $this->db->get('tbl_user');    
    }

    public function get_all_course()
	{
		$this->db	->select('*')
					->where('tbl_course.deleted', 0) 
					->order_by('course_name','asc');
        return $this->db->get('tbl_course');    
    }

    public function get_all_teller()
	{
		$this->db	->select('*')
					->where('tbl_user.deleted', 0)
					->where('user_level', 3)
					->order_by('fullname','asc');
        return $this->db->get('tbl_user');    
    }
}