<?php

class order extends Front_Controller{
	
	private $per_page 	= '30';
	private $title 		= 'Order';
	private $module 	= 'order';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('order_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect('order/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->order_model->count_all($this->input->post('q'));
			
			//pagination
			$config['base_url']     = site_url('order/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = $this->per_page; 
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(3);
			
			$data = $this->order_model->get_all($config['per_page'], $no, $this->input->post('q'));		
			
			
			$this->template	->set('menu_title', 'Setting Order')
							->set('menu_environment', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('order');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function sltp($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->order_model->count_all($this->input->post('q'),"SLTP");
			
			//pagination
			$config['base_url']     = site_url('order/slta');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = $this->per_page; 
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(3);
			
			$data = $this->order_model->get_all($config['per_page'], $no, $this->input->post('q'),"SLTP");		
			
			
			$this->template	->set('menu_title', 'Pengaturan Pendaftaran')
							->set('menu_subtitle', 'Daftar Pendaftar Paket Tingkatan SLTP')
							->set('menu_order', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('order_sltp');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function slta($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->order_model->count_all($this->input->post('q'),"SLTA");
			
			//pagination
			$config['base_url']     = site_url('order/slta');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = $this->per_page; 
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(3);
			
			$data = $this->order_model->get_all($config['per_page'], $no, $this->input->post('q'),"SLTA");		
			
			
			$this->template	->set('menu_title', 'Pengaturan Pendaftaran')
							->set('menu_subtitle', 'Daftar Pendaftar Paket Tingkatan SLTA')
							->set('menu_order', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('order_slta');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}

	public function create(){
		if($this->save_order()){
			$this->session->set_flashdata('message', 'success|Success add new record');
			redirect('order/slta');
		}
					
			
		$user = $this->order_model->get_all_user()->result();
		$course = $this->order_model->get_all_course()->result();
		$teller = $this->order_model->get_all_teller()->result();
		
		$this->template	->set('menu_title', 'Pengaturan Pendaftaran')
						->set('user', $user)
						->set('course', $course)
						->set('teller', $teller)
						->set('menu_subtitle', 'Formulir Penambahan Pendaftaran')
						->build('order_edit');		
	}
	
	public function create_sltp(){
		if($this->save_order()){
			$this->session->set_flashdata('message', 'success|Success add new record');
			redirect('order/sltp');
		}
					
			
		$user = $this->order_model->get_all_user()->result();
		$course = $this->order_model->get_all_course()->result();
		$teller = $this->order_model->get_all_teller()->result();
		
		$this->template	->set('menu_title', 'Pengaturan Pendaftaran')
						->set('user', $user)
						->set('course', $course)
						->set('teller', $teller)
						->set('menu_subtitle', 'Formulir Penambahan Pendaftaran')
						->build('order_edit');		
	}
	
	public function edit($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_order()){
			$this->session->set_flashdata('message', 'success|Data Berhasil Diubah');
			redirect('order/slta');
		}
		
		//Get Specific Data
		$data = $this->order_model->get_order($id)->result();
		$data = $data[0];
		
		$user = $this->order_model->get_all_user()->result();
		$course = $this->order_model->get_all_course()->result();
		$teller = $this->order_model->get_all_teller()->result();
		
		$this->template	->set('data', $data)
						->set('user', $user)
						->set('course', $course)
						->set('teller', $teller)
						->set('menu_title', 'Pengaturan Pendaftaran')
						->set('menu_subtitle', 'Formulir Pengubahan Pendaftaran')
						->build('order_edit');		
	}
	
	public function edit_sltp($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_order()){
			$this->session->set_flashdata('message', 'success|Success edit record');
			redirect('order/sltp');
		}
		
		//Get Specific Data
		$data = $this->order_model->get_order($id)->result();
		$data = $data[0];
		
		$user = $this->order_model->get_all_user()->result();
		$course = $this->order_model->get_all_course()->result();
		$teller = $this->order_model->get_all_teller()->result();
		
		$this->template	->set('data', $data)
						->set('user', $user)
						->set('course', $course)
						->set('teller', $teller)
						->set('menu_title', 'Pengaturan Pendaftaran')
						->set('menu_subtitle', 'Formulir Pengubahan Pendaftaran')
						->build('order_edit');		
	}
	

	public function view($id){
		$id =  $this->uri->segment(3);
		
		//Get Specific data
		$data = $this->order_model->get_order($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'View')
						->set('menu_subtitle', 'View')
						->build('order_edit');	
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(3);
		if($this->order_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Success delete row');
			redirect('order');
			exit;
		}
	}	
	
	private function save_order(){
		
		//set form validation
		$this->form_validation->set_rules('order_student', 'Order Student', 'required');
		$this->form_validation->set_rules('order_course', 'Order Course', 'required');
		$this->form_validation->set_rules('order_date_start', 'Order Date Start', 'required');
		$this->form_validation->set_rules('order_date_end', 'Order Date End', 'required');
		$this->form_validation->set_rules('order_price', 'Order Price', 'required');
		$this->form_validation->set_rules('order_pay_date', 'Order Pay Date', 'required');
		$this->form_validation->set_rules('order_teller', 'Order Teller', 'required');
		$this->form_validation->set_rules('order_status', 'Order Status', 'required');
		
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('order_id');			
			
			//process the form
			$data = array(
					'order_student'     => $this->input->post('order_student'),
					'order_course'      => $this->input->post('order_course'),
					'order_date_start'  => nl2br($this->input->post('order_date_start')),
					'order_date_end'    => $this->input->post('order_date_end'),
					'order_price'    => $this->input->post('order_price'),
					'order_pay_date'    => $this->input->post('order_pay_date'),
					'order_teller'    => $this->input->post('order_teller'),
					'order_status'    => $this->input->post('order_status'),
			);
				
			if(!$id){
				return $this->order_model->insert($data);
			}else{
				return $this->order_model->update($id, $data);
			} 
		}
	}
}