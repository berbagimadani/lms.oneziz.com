<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Customer Model
 * @module	Customer
 * @package	CMS
 * @author 	fikriwirawan
 * @since	10 Nov 2014
 */
 
class school_model extends MY_Model {

    protected $table        = 'tbl_school';
    protected $key          = 'school_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='')
	{
		if($search != '')
		{
			return $this->db->select('*')
							->from($this->table)
							->join('tbl_city','tbl_city.city_id = tbl_school.school_city')
							->join('tbl_province','tbl_province.province_id = tbl_school.school_province')
							->where('tbl_school.deleted','0')
							->like('school_name',$search)
							->limit($limit,$offset)
							->order_by('school_name','asc')
							->order_by('city_name','asc')
							->get()
							->result();
		}else
		{		
			return $this->db->select('*')
						->from($this->table)
						->join('tbl_city','tbl_city.city_id = tbl_school.school_city')
						->join('tbl_province','tbl_province.province_id = tbl_school.school_province')
						->where('tbl_school.deleted','0')
				        ->limit($limit,$offset)
						->order_by('school_name','asc')
							->order_by('city_name','asc')
						->get()
						->result();
		}
	}
	
	public function count_all($search)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('deleted','0')
			->like('school_name',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}

	public function get_byid($id)
	{
		$this->db	->select('*')
					->where('deleted', 0)
					->where('school_id', $id);
        return $this->db->get($this->table);    
    }

	public function getProvince()
	{
		return $this->db->select("*")
			->from('tbl_province')
			->order_by('province_id','asc')
			->get()
			->result();
	}

	public function getCity()
	{
		return $this->db->select("*")
			->from('tbl_city')
			->order_by('city_id','asc')
			->get()
			->result();
	}
}