<?php

class School extends Front_Controller{
	
	private $per_page 	= '15';
	private $title 		= 'School';
	private $module 	= 'school';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('school_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect($this->module.'/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->school_model->count_all($this->input->post('q'));
			
			//pagination
			$config['base_url']     = site_url($this->module.'/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = 30; 
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(3);
			
			$dataresult = $this->school_model->get_all($this->input->post('q'));
			
			
			$this->template	->set('menu_title', 'Pengaturan Sekolah')
							->set('menu_database', 'active')
							->set('material_total',$config['total_rows'])
							->set('dataresult', $dataresult)
							->set('no', $no)
							->set('config', $config)
							->build('school');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function create(){
		if($this->save_data()){
			$this->session->set_flashdata('message', 'success|Data berhasil ditambahkan');
			redirect($this->module.'/');
		}

		$province = $this->school_model->getProvince();
		$city     = $this->school_model->getCity();

		//ug teacher
		$userlevel = 3;
			
		$this->template	->set('menu_title', 'Pengaturan Sekolah')
						->set('menu_subtitle', 'Formulir Penambahan Data Sekolah')
						->set('menu_database', 'active')
						->set('province', $province)
						->set('city', $city)
						->build('school_edit');
	}
	
	public function edit($id){
		$id =  $this->uri->segment(3);

		if($this->save_data()){
			$this->session->set_flashdata('message', 'success|Data berhasil diubah');
			redirect($this->module.'/');
		}

		$province = $this->school_model->getProvince();
		$city     = $this->school_model->getCity();
		
		//Get Specific customer
		$data = $this->school_model->get_byid($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('province', $province)
						->set('city', $city)
						->set('action', 'edit')
						->set('menu_title', 'Pengaturan Data Sekolah')
						->set('menu_subtitle', 'Formulir Pengaturan Data Sekolah')
						->set('menu_database', 'active')
						->build('school_edit');
	}

	public function view($id){
		$id =  $this->uri->segment(3);

		$province = $this->school_model->getProvince();
		$city     = $this->school_model->getCity();

		//Get Specific customer
		$data = $this->school_model->get_byid($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'Data Sekolah')
						->set('menu_subtitle', 'Data Sekolah')
						->set('menu_database', 'active')
			->set('city', $city)
			->set('province', $province)
			->build('school_view');
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(3);
		if($this->school_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Data Berhasil Dihapus');
			redirect($this->module.'/browse');
			exit;
		}
	}	
	
	private function save_data(){
		
		//set form validation
		$this->form_validation->set_rules('school_name', 'Nama Sekolah', 'required');
//		$this->form_validation->set_rules('school_head_name', 'Nama Kepala Sekolah', 'required');
//		$this->form_validation->set_rules('school_head_email', 'Email', 'required');
//		$this->form_validation->set_rules('school_head_phone', 'Telp', 'required');
		$this->form_validation->set_rules('school_city', 'School City', 'required');
		$this->form_validation->set_rules('school_province', 'School Province', 'required');

	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('school_id');
			
			//process the form
			$data = array(
					'school_name'       => $this->input->post('school_name'),
					'school_province'   => $this->input->post('school_province'),
					'school_city'       => $this->input->post('school_city'),
					'school_address'       	=> $this->input->post('school_address'),
					'school_head_name'      => $this->input->post('school_head_name'),
					'school_head_email'     => $this->input->post('school_head_email'),
					'school_head_phone'     => $this->input->post('school_head_phone')
			);
				
			if(!$id){
				return $this->school_model->insert($data);
			}else{
				return $this->school_model->update($id, $data);
			} 
		}
	}
}