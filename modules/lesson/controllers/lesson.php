<?php

class Lesson extends Front_Controller{
	
	private $per_page 	= '30';
	private $title 		= 'Lesson';
	private $module 	= 'lesson';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('lesson_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect('lesson/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->lesson_model->count_all($this->input->post('q'));
			
			//pagination
			$config['base_url']     = site_url('lesson/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = 30; 
			$config['uri_segment']  = 4;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(4);
			
			$data = $this->lesson_model->get_all($config['per_page'], $no, $this->input->post('q'));		
			
			if($this->ion_auth->is_siswa()){$menu_title='Daftar Mata Pelajaran';}
			else{$menu_title='Pengaturan Mata Pelajaran';}
			$this->template	->set('menu_title', $menu_title)
							->set('menu_environment', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('lesson');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function create(){
		if($this->save_lesson()){
			$this->session->set_flashdata('message', 'success|Success add new record');
			redirect('lesson');
		}
					
			
		$course = $this->lesson_model->get_all_course()->result();
		
		$this->template	->set('menu_title', 'Pengaturan Mata Pelajaran')
						->set('course', $course)
						->set('menu_subtitle', 'Formulir Penambahan Mata Pelajaran')
						->build('lesson_edit');		
	}
	
	public function edit($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_lesson()){
			$this->session->set_flashdata('message', 'success|Success edit record');
			redirect('lesson');
		}
		
		//Get Specific Data
		$data = $this->lesson_model->get_lesson($id)->result();
		$data = $data[0];
		
		$course = $this->lesson_model->get_all_course()->result();
		
		$this->template	->set('data', $data)
						->set('course', $course)
						->set('menu_title', 'Pengaturan Mata Pelajaran')
						->set('menu_subtitle', 'Formulir Pengaturan Mata Pelajaran')
						->build('lesson_edit');		
	}
	
	public function view($id){
		$id =  $this->uri->segment(3);
		
		//Get Specific data
		$data = $this->lesson_model->get_lesson($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'View')
						->set('menu_subtitle', 'View')
						->build('lesson_edit');	
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(3);
		if($this->lesson_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Data berhasil dihapus');
			redirect('lesson');
			exit;
		}
	}	
	
	private function save_lesson(){
		
		//set form validation
		$this->form_validation->set_rules('lesson_name', 'Nama Mata Pelajaran', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('lesson_id');			
			
			//process the form
			$data = array(
					'lesson_name'       	=> $this->input->post('lesson_name'),
					'lesson_course_id'      => $this->input->post('lesson_course'),
					'lesson_description'    => nl2br($this->input->post('lesson_description'))
			);
				
			if(!$id){
				return $this->lesson_model->insert($data);
			}else{
				return $this->lesson_model->update($id, $data);
			} 
		}
	}
}