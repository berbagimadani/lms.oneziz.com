<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * course Model
 * @module	course
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class course_model extends MY_Model {

    protected $table        = 'tbl_course';
    protected $key          = 'course_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='')
	{
		if($this->ion_auth->is_siswa()){
			return $this->db->select('*')
								->from($this->table)
								->join('tbl_order','tbl_order.order_course = tbl_course.course_id')
								->where('tbl_order.order_student', $this->session->userdata('user_id'))
								->where('tbl_course.deleted','0')
								->where('tbl_order.deleted','0')
								->order_by('course_name','asc')
								->get()
								->result();
		}else{
			if($search != '')
			{
				return $this->db->select('*')
								->from($this->table)
								->where('tbl_course.deleted','0')
								->like('course_name',$search)
								->limit($limit,$offset)
								->order_by('course_name','asc')
								->get()
								->result();
			}else
			{		
				return $this->db->select('*')
							->from($this->table)
							->where('tbl_course.deleted','0')
							->limit($limit,$offset)
							->order_by('course_name','asc')
							->get()
							->result();
			}
		}
	}
	
	public function count_all($search)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('deleted','0')
						->like('course_name',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	
		
	public function get_course($id)
	{
		$this->db	->select('*')
					->where('tbl_course.deleted', 0)
					->where('course_id', $id);
        return $this->db->get($this->table);    
    }
	
		
	public function get_all_course()
	{
		$this->db	->select('*')
					->where('tbl_course.deleted', 0);
        return $this->db->get('tbl_course');    
    }
	
		
	public function get_all_school_grade()
	{
		$this->db	->select('*')
					->where('tbl_school_grade.deleted', 0);
        return $this->db->get('tbl_school_grade');    
    }
}