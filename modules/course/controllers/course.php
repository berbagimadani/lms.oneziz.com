<?php

class Course extends Front_Controller{
	
	private $per_page 	= '30';
	private $title 		= 'Course';
	private $module 	= 'course';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('course_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect('course/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->course_model->count_all($this->input->post('q'));
			
			//pagination
			$config['base_url']     = site_url('course/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = 30; 
			$config['uri_segment']  = 4;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(4);
			
			$data = $this->course_model->get_all($config['per_page'], $no, $this->input->post('q'));		
			
			if($this->ion_auth->is_siswa()){$menu_title='Daftar Paket Bimbel';}
			else{$menu_title='Pengaturan Daftar Paket Bimbel';}
			$this->template	->set('menu_title', $menu_title)
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('course');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function create(){
		if($this->save_course()){
			$this->session->set_flashdata('message', 'success|Success add new record');
			redirect('course');
		}
					
		$course_school_grade = $this->course_model->get_all_school_grade()->result();
		
		$this->template	->set('menu_title', 'Pengaturan Daftar Paket Bimbel')
						->set('menu_subtitle', 'Formulir Penambahan Daftar Paket Bimbe')
						->set('course_school_grade', $course_school_grade)
						->build('course_edit');		
	}
	
	public function edit($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_course()){
			$this->session->set_flashdata('message', 'success|Success edit record');
			redirect('course');
		}
		
		//Get Specific Data
		$data = $this->course_model->get_course($id)->result();
		$data = $data[0];
		
		$course_school_grade = $this->course_model->get_all_school_grade()->result();
		
		$this->template	->set('data', $data)
						->set('menu_title', 'Pengaturan Daftar Paket Bimbel')
						->set('menu_subtitle', 'Formulir Pengaturan Daftar Paket Bimbel')
						->set('course_school_grade', $course_school_grade)
						->build('course_edit');		
	}
	
	public function view($id){
		$id =  $this->uri->segment(3);
		
		//Get Specific data
		$data = $this->course_model->get_course($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'View')
						->set('menu_subtitle', 'View')
						->build('course_edit');	
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(3);
		if($this->course_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Data berhasil dihapus');
			redirect('course');
			exit;
		}
	}	
	
	private function save_course(){
		
		//set form validation
		$this->form_validation->set_rules('course_name', 'Nama Paket Bimbel', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('course_id');			
			
			//process the form
			$data = array(
					'course_name'       	=> $this->input->post('course_name'),
					'course_price'       	=> $this->input->post('course_price'),
					'course_school_grade'       	=> $this->input->post('course_school_grade')
			);
				
			if(!$id){
				return $this->course_model->insert($data);
			}else{
				return $this->course_model->update($id, $data);
			} 
		}
	}
}