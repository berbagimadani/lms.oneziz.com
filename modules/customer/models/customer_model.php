<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Customer Model
 * @module	Customer
 * @package	CMS
 * @author 	fikriwirawan
 * @since	10 Nov 2014
 */
 
class customer_model extends MY_Model {

    protected $table        = 'tbl_customer';
    protected $key          = 'customer_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='')
	{
		if($search != '')
		{
			return $this->db->select('*')
							->from($this->table)
							->where('tbl_customer.deleted','0')
							->like('customer_company',$search)
							->limit($limit,$offset)
							->order_by('customer_company','asc')
							->get()
							->result();
		}else
		{		
			return $this->db->select('*')
						->from($this->table)
							->where('tbl_customer.deleted','0')
						->limit($limit,$offset)
						->order_by('customer_company','asc')
						->get()
						->result();
		}
	}
	
	public function count_all($search)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('deleted','0')
						->like('customer_company',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	
	public function get_all_customer()
	{
		$this->db	->select('*')
					->where('tbl_customer.deleted', 0);
        return $this->db->get($this->table);    
    }
	
	public function get_customer($id)
	{
		$this->db	->select('*')
					->where('tbl_customer.deleted', 0)
					->where('customer_id', $id);
        return $this->db->get($this->table);    
    }
}