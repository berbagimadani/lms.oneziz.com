<?php

class Customer extends Front_Controller{
	
	private $per_page 	= '15';
	private $title 		= 'Customer';
	private $module 	= 'customer';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('customer_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect($this->module.'/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->customer_model->count_all($this->input->post('q'));
			
			//pagination
			$config['base_url']     = site_url($this->module.'/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = 30; 
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(3);
			
			$customer = $this->customer_model->get_all($this->input->post('q'));		
			
			
			$this->template	->set('menu_title', 'Browse Customer')
							->set('menu_database', 'active')
							->set('material_total',$config['total_rows'])
							->set('customer', $customer)
							->set('no', $no)
							->set('config', $config)
							->build('customer');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function create(){
		if($this->save_customer()){
			$this->session->set_flashdata('message', 'success|Success add new record');
			redirect($this->module.'/');
		}
					
			
		$this->template	->set('menu_title', 'Add New customer')
						->set('menu_subtitle', 'New customer Form')
						->set('menu_database', 'active')
						->build('customer_edit');		
	}
	
	public function edit($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_customer()){
			$this->session->set_flashdata('message', 'success|Success edit record');
			redirect($this->module.'/');
		}
		
		//Get Specific customer
		$data = $this->customer_model->get_customer($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'Edit customer')
						->set('menu_subtitle', 'Edit customer Form')
						->set('menu_database', 'active')
						->build('customer_edit');	
	}
	
	public function view($id){
		$id =  $this->uri->segment(3);
		
		//Get Specific customer
		$data = $this->customer_model->get_customer($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'View customer')
						->set('menu_subtitle', 'View customer')
						->set('menu_database', 'active')
						->build('customer_view');	
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(3);
		if($this->customer_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Success delete row');
			redirect($this->module.'/browse');
			exit;
		}
	}	
	
	private function save_customer(){
		
		//set form validation
		$this->form_validation->set_rules('customer_name', 'customer Name', 'required');
		$this->form_validation->set_rules('customer_company', 'Company Name', 'required');
		$this->form_validation->set_rules('customer_email', 'Email', 'required');
		$this->form_validation->set_rules('customer_phone', 'Phone', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('customer_id');			
			
			//process the form
			$data = array(
					'customer_name'       	=> $this->input->post('customer_name'),
					'customer_company'      => $this->input->post('customer_company'),
					'customer_email'       	=> $this->input->post('customer_email'),
					'customer_phone'       	=> $this->input->post('customer_phone'),
					'customer_mobile'       => $this->input->post('customer_mobile'),
					'customer_address'      => $this->input->post('customer_address'),
					'customer_city'       	=> $this->input->post('customer_city'),
					'customer_state'       	=> $this->input->post('customer_state'),
					'customer_country'      => $this->input->post('customer_country'),
					'customer_fax'       	=> $this->input->post('customer_fax'),
					'customer_zipcode'      => $this->input->post('customer_zipcode'),
					'customer_remark' 		=> $this->input->post('customer_remark'),
			);
				
			if(!$id){
				return $this->customer_model->insert($data);
			}else{
				return $this->customer_model->update($id, $data);
			} 
		}
	}
}