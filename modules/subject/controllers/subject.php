<?php

class Subject extends Front_Controller{
	
	private $per_page 	= '30';
	private $title 		= 'subject';
	private $module 	= 'subject';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('subject_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect('subject/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0', $opt=null){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->subject_model->count_all($this->input->post('q'), $this->input->post('grade'), $this->input->post('lesson'), $opt );
			
			//pagination
			$config['base_url']     = site_url('subject/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = $this->per_page; 
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(3);
			
			$data = $this->subject_model->get_all($config['per_page'], $no, $this->input->post('q'), $this->input->post('grade'), $this->input->post('lesson'), $opt );		
			
			$grade = $this->subject_model->get_grade( $opt );
			$lesson = $this->subject_model->get_lesson( $opt );

			if($this->ion_auth->is_siswa()){
				$menu_title="Daftar Materi Bimbel";
			}else{
				$menu_title="Pengaturan Materi Bimbel";
			}
			$this->template	->set('menu_title', $menu_title)
				 			->set('menu_environment', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->set('grade', $grade)
							->set('lesson', $lesson)
							->set('q_grade', $this->input->post('grade') )
							->set('q_lesson', $this->input->post('lesson') )
							->build('subject');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}

	public function create(){
		if($this->save_subject()){
			$this->session->set_flashdata('message', 'success|Data berhasil ditambahkan');
			redirect('subject');
		}
					
			
		$lesson = $this->subject_model->get_all_lesson()->result();
		$course = $this->subject_model->getCourse();
		
		$this->template	->set('menu_title', 'Pengaturan Materi Bimbel')
						->set('lesson', $lesson)
						->set('course', $course)
						->set('menu_subtitle', 'Formulir Penambahan Materi Bimbel')
						->build('subject_edit');		
	}

	public function viewer($id){
		$data = $this->subject_model->get_subject($id)->result();
		$data = $data[0];
		$this->template	->set('filename', $data->subject_material)
			    		->build('subject_viewer_pdfjs_master');
	}

	public function viewer_al($id){
		$data = $this->subject_model->get_subject($id)->result();
		$data = $data[0];
		$this->template	->set('filename', $data->subject_material)
			    		->build('subject_viewer_pdfjs');
	}
	
	public function edit($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_subject()){
			$this->session->set_flashdata('message', 'success|Data berhasil diubah');
			redirect('subject');
		}
		
		//Get Specific Data
		$data = $this->subject_model->get_subject($id)->result();
		$data = $data[0];
		
		$lesson = $this->subject_model->get_all_lesson()->result();
		foreach($lesson as $row){
			if($row->lesson_id == $data->subject_lesson_id){
				$q_course_id = $row->lesson_course_id;
			}
		}
		$course = $this->subject_model->getCourse();

		$this->template	->set('data', $data)
						->set('lesson', $lesson)
						->set('course', $course)
						->set('q_course_id', $q_course_id)
						->set('menu_title', 'Pengaturan Materi Bimbel')
						->set('menu_subtitle', 'Formulir Pengaturan Materi Bimbel')
						->build('subject_edit');		
	}
	
	public function view($id){
		$id =  $this->uri->segment(3);
		
		//Get Specific data
		$data = $this->subject_model->get_subject($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'View')
						->set('menu_subtitle', 'View')
						->build('subject_edit');	
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(3);
		if($this->subject_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Success delete row');
			redirect('subject');
			exit;
		}
	}	
	
	private function save_subject(){
		
		//set form validation
		//$this->form_validation->set_rules('subject_name', 'Nama Materi', 'required');
		$this->form_validation->set_rules('subject_lesson', 'Nama Mata Pelajaran', 'required');
		$this->form_validation->set_rules('subject_section_no', 'Bab', 'required');
		$this->form_validation->set_rules('subject_section_name', 'Judul Bab', 'required');
		$this->form_validation->set_rules('subject_subsection_no', 'Sub Bab', 'required');
		$this->form_validation->set_rules('subject_subsection_name', 'Judul Sub Bab', 'required');
		$this->form_validation->set_rules('subject_code', 'Subject Code', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('subject_id');			
			
			
			
			//process the form
			$data = array(
					/*'subject_name'       	=> $this->input->post('subject_name'),*/
					'subject_name'       	=> $this->input->post('subject_name'),
					'subject_section_no'    => $this->input->post('subject_section_no'),
					'subject_section_name'       	=> $this->input->post('subject_section_name'),
					'subject_subsection_no'    => $this->input->post('subject_subsection_no'),
					'subject_subsection_name'       	=> $this->input->post('subject_subsection_name'),
					'subject_code'       	=> $this->input->post('subject_code'),
					'subject_lesson_id'      => $this->input->post('subject_lesson'),
					'subject_category'      => $this->input->post('subject_category'),
					'subject_description'    => nl2br($this->input->post('subject_description'))
			);
			//try to upload image first
			//if($this->input->post('subject_material')){
				try{
				 
					$timestamp = date("Ymdhis");
					$filename = "$timestamp";
					$config['upload_path'] 		= 'media/material';
					$config['allowed_types'] 	= 'pdf|PDF';
					$config['encrypt_name']	 	= FALSE;
					$config['file_name']	 	= $filename;
					$this->load->library('upload', $config);
					$this->upload->overwrite = true;
					if($this->upload->do_upload('subject_material')){
						$upload 			= $this->upload->data(); 
						$data['subject_material']  = $filename.$upload['file_ext'];
					}
					
				}catch(Exception $e){
					//echo $e;
					//exit;
				
				}
			//}
			
			
			
			if(!$id){
				return $this->subject_model->insert($data);
			}else{
				return $this->subject_model->update($id, $data);
			} 
		}
	}
}