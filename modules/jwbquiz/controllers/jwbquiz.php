<?php

class Jwbquiz extends Front_Controller{
	
	private $per_page 	= '30';
	private $title 		= 'quiz';
	private $module 	= 'quiz';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('jwbquiz_model');
		$this->load->model('quiz_model');
		$this->load->model('subject_model');
		$this->load->model('lesson_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect('jwbquiz/start');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}


	public function answer(){
		//if($this->session->userdata('logged_in'))
		//{
		$quiz = $this->jwbquiz_model->cek_answer($_POST['quiz_id']);
		//echo $_POST['answer'].$quiz->quiz_answer;

		if($quiz->quiz_answer == $_POST['answer'])  {

			$data = array(
	               'status' => 2, 
	               'answer' => $_POST['answer'], 
	        );
	        $this->db->where('playquiz_quiz_id', $quiz->quiz_id);
			$this->db->update('tbl_playquiz', $data);

			echo "Jawaban Anda Benar <br>";
			echo "Pembahasan <br> ";
			echo $quiz->quiz_solution;
		} else {

			$data = array(
	               'status' => 1, 
	               'answer' => $_POST['answer'], 
	        );
	        $this->db->where('playquiz_quiz_id', $quiz->quiz_id);
			$this->db->update('tbl_playquiz', $data);

			echo "Jawaban Anda Salah<br>";
			echo "Pembahasan <br> ";
			echo $quiz->quiz_solution;
		}

		//} 
	}


	public function start(){

		$quiz = $this->jwbquiz_model->cek_quiz($this->session->userdata('user_id'));

		//if($quiz)	{
			//redirect('jwbquiz/browse');

		//} else{
			if($this->save_quiz()){
				$this->session->set_flashdata('message', 'success|Success add new record');
				redirect('jwbquiz/browse');
			}

			$key = $this->generateRandomString();

			$subject = $this->subject_model->get_all_subject()->result();		
			$this->template	->set('menu_title', 'Pertanyaan')
						->set('menu_environment', 'active') 
						->set('subject', $subject) 
						->set('key', $key)
						->build('step-1-jwbquiz');
		//}
		 
	}

	public function generateRandomString($length = 10) {

	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	
	public function browse($page='0'){

		$quiz = $this->jwbquiz_model->cek_quiz($this->session->userdata('user_id'));
		$key = $this->jwbquiz_model->get_key();

		if(!$quiz) {
			redirect('jwbquiz/raport/'.$key[0]->playquiz_key.'');
		}

		if($this->session->userdata('logged_in'))
		{ 
			
			$data = $this->jwbquiz_model->get_all($key[0]->playquiz_key);
			$total_rows = $this->jwbquiz_model->count_all($key[0]->playquiz_key);

			$this->template	->set('menu_title', 'Pertanyaan')
							->set('menu_environment', 'active')
							->set('total_rows',$total_rows)
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('jwbquiz');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}

	public function raport($key){ 

		if($this->session->userdata('logged_in'))
		{ 

			$data = array(
	               'deleted' => 1, 
	        );
	        $this->db->where('playquiz_key', $key);
	        $this->db->where('playquiz_user_id', $this->session->userdata('user_id'));
			$this->db->update('tbl_playquiz', $data);
			
			$false = $this->jwbquiz_model->get_raport(1,$key);
			$true = $this->jwbquiz_model->get_raport(2, $key);	

			$total_rows = $this->jwbquiz_model->count_one($key);
			
			$data = $this->jwbquiz_model->get_subject($key);

			$this->template	->set('menu_title', 'Pertanyaan')
							->set('menu_environment', 'active')
							->set('total_rows',$total_rows)
							->set('false', $false)
							->set('true', $true)  
							->set('data', $data)
							->build('raport');
		} 
	}


	private function save_quiz(){
		
		//set form validation
		$this->form_validation->set_rules('quiz', 'Pilih Quiz', 'required'); 
		$quiz = $this->quiz_model->get_quiz_subject($this->input->post('quiz'));
	
		//print_r($quiz);
		
		if($this->form_validation->run() === TRUE){ 

			foreach ($quiz as $row) {
 				//process the form
				
				$data = array(
					'playquiz_quiz_id'  => $row->quiz_id,
					'playquiz_user_id'  => $this->session->userdata('user_id'),
					'playquiz_key'      => $this->input->post('random'), 
				);
				 $this->jwbquiz_model->insert($data);
			}

			return true;
			
		}

	}
	 
}