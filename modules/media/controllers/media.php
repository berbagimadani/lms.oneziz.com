<?php

class Media extends Front_Controller{
	
	private $per_page 	= '30';
	private $title 		= 'media';
	private $module 	= 'media';
	private $path 		= 'media'; 
	
	public function __construct(){
		parent::__construct();
		$this->load->model('media_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect('media/materi');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function materi($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->media_model->count_all_materi($this->input->post('q'),'materi');
			
			//pagination
			$config['base_url']     = site_url('media/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = $this->per_page; 
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(3);
			
			$data = $this->media_model->get_all_materi($config['per_page'], $no, $this->input->post('q'),'materi');		
			
			
			$this->template	->set('menu_title', 'Pengaturan Media')
							->set('menu_subtitle', 'Daftar Media Materi')
							->set('menu_media', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('media_materi');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	
	public function sumberdaya($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->media_model->count_all_sumberdaya($this->input->post('q'), 'sumberdaya');
			
			//pagination
			$config['base_url']     = site_url('media/sumberdaya');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = $this->per_page; 
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(3);
			
			$data = $this->media_model->get_all($config['per_page'], $no, $this->input->post('q'), 'sumberdaya');		
			
			
			$this->template	->set('menu_title', 'Pengaturan Media')
							->set('menu_subtitle', 'Daftar Media Sumber Daya')
							->set('menu_media', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('media_sumberdaya');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	
	public function create_sumberdaya(){
		if($this->save_media_sumberdaya()){
			$this->session->set_flashdata('message', 'success|Success add new record');
			redirect('media/sumberdaya');
		}
					
		
		$this->template	->set('menu_title', 'Pengaturan Media')
						->set('subject', $subject)
						->set('menu_subtitle', 'Formulir Penambahan Media')
						->build('media_sumberdaya_edit');		
	}
	
	public function create_materi(){
		if($this->save_media_materi()){
			$this->session->set_flashdata('message', 'success|Data berhasil ditambahkan');
			redirect('media/materi');
		}
					
			
		
		$lesson = $this->media_model->get_all_lesson()->result();
		$course = $this->media_model->getCourse();
		
		$this->template	->set('menu_title', 'Pengaturan Media')
						->set('lesson', $lesson)
						->set('course', $course)
						->set('menu_subtitle', 'Formulir Penambahan Media')
						->build('media_materi_edit');		
	}
	
	
	public function edit_sumberdaya($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_media_sumberdaya()){
			$this->session->set_flashdata('message', 'success|Success edit record');
			redirect('media/sumberdaya');
		}
		
		//Get Specific Data
		$data = $this->media_model->get_media($id)->result();
		$data = $data[0];
		
		$subject = $this->media_model->get_all_subject()->result();
		
		$this->template	->set('data', $data)
						->set('subject', $subject)
						->set('menu_title', 'Pengaturan Media')
						->set('menu_subtitle', 'Formulir Pengubahan Media')
						->build('media_sumberdaya_edit');		
	}
	
	
	public function edit_materi($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_media_materi()){
			$this->session->set_flashdata('message', 'success|Success edit record');
			redirect('media');
		}
		
		//Get Specific Data
		$data = $this->media_model->get_media($id)->result();
		$data = $data[0];
		
		$lesson = $this->media_model->get_all_lesson()->result();
		$course = $this->media_model->getCourse();
		
		$this->template	->set('data', $data)
						->set('lesson', $lesson)
						->set('course', $course)
						->set('menu_title', 'Pengaturan Media')
						->set('menu_subtitle', 'Formulir Pengubahan Media')
						->build('media_materi_edit');		
	}
	
	public function view($id){
		$id =  $this->uri->segment(3);
		
		//Get Specific data
		$data = $this->media_model->get_media($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'View')
						->set('menu_subtitle', 'View')
						->build('media_edit');	
	}
	
	public function delete($id = '0'){

		$id =  $this->uri->segment(3);

		$this->load->helper("file"); 
		$row = $this->media_model->get_media_file($id); 
		$original = read_file($this->path."/".$row->filename); 
		$thumb = read_file($this->path."/thumb/".$row->filename); 
		$this->media_model->delete($id);
		if($original){
			unlink($this->path."/".$row->filename);    
			unlink($this->path."/thumb/".$row->filename);  
			redirect('media');
		}
		else{ 
		}

	}	
	
	private function save_media_sumberdaya(){
		
		//set form validation
		$this->form_validation->set_rules('media_name', 'Nama Media', 'required');
		$this->form_validation->set_rules('media_type', 'Media Type', 'required');

		$id = $this->input->post('media_id');
		
		if(!$id){
		// <---- images -->
			$d["file_name"] = "";
			$file = $_FILES["images"]["name"];
			if(!empty($file)){ 
				$config2["upload_path"] = $this->path;
				$config2["allowed_types"] = "iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|wmv|avi|mpg|mpeg|mp3|mp4|3gp|gif|jpg|jpeg|png";
				$config2["max_size"] = "900000";
				$config2["file_ext"] = ""; 
				$file = $_FILES["images"]["name"]; 
				$filename = explode(".",$file);
				$config2["overwrite"] = TRUE;
				$config2["file_name"] = date('Ymd').'_'.$this->input->post("media_subject").'_'.$filename[0];
				
				$this->load->library("upload", $config2);
				$this->load->library("image_lib");
				if (!$this->upload->do_upload("images")){
					 $error = array("error" => $this->upload->display_errors());
					 //$this->load->view("delete", $error);  
				}else{
				$res = $this->upload->data();
				$file_ext     = $res["file_ext"];
				//$final_file_name = time().$file_ext;
				$img = $config2["file_name"].$file_ext;
				$d["file_name"] = $img;
				
				$config["image_library"] = "gd2";
				$config["source_image"] = $this->path."/".$d["file_name"];
				$config["create_thumb"] = TRUE;
				$config["maintain_ratio"] = true;
				$config["thumb_marker"] = FALSE;
				$config["width"] = 550;
				$config["height"] = 340;
				$this->image_lib->initialize($config); 
				$this->load->library("image_lib", $config);
				$this->image_lib->resize();
				
				$config3["image_library"] = "gd2";
				$config3["source_image"] = $this->path."/".$d["file_name"];
				$config3["new_image"] = $this->path."/thumb/".$d["file_name"];
				$config3["create_thumb"] = TRUE;
				$config3["maintain_ratio"] = true;
				$config3["thumb_marker"] = false;
				$config3["width"] = 100;
				$config3["height"] = 100;
				$this->image_lib->initialize($config3); 
				$this->load->library("image_lib", $config3);
				$this->image_lib->resize();
			}
		}
		// <----/images-->
		}
		else{


			// <---- images --> 
				$path = "media";
				$config2["upload_path"] = $this->path;
				$config2["allowed_types"] = "iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|wmv|avi|mpg|mpeg|mp3|mp4|3gp|gif|jpg|jpeg|png";
				$config2["max_size"] = "900000";
				$config2["file_ext"] = ""; 
				$file = $_FILES["images"]["name"]; 
				$filename = explode(".",$file);
				$config2["overwrite"] = TRUE;
				$config2["file_name"] = date('Ymd').'_'.$this->input->post("media_subject").'_'.$filename[0];
				
				$this->load->library("upload", $config2);
				$this->load->library("image_lib");
				
				$this->upload->do_upload("images");
					$res = $this->upload->data();
					$file_ext     = $res["file_ext"];
					//$final_file_name = time().$file_ext;
					$img = $config2["file_name"].$file_ext;
					$d["file_name"] = $img;
				
					$config["image_library"] = "gd2";
					$config["source_image"] = $this->path."/".$d["file_name"];
					$config["create_thumb"] = TRUE;
					$config["maintain_ratio"] = true;
					$config["thumb_marker"] = FALSE;
					$config["width"] = 550;
					$config["height"] = 340;
					$this->image_lib->initialize($config); 
					$this->load->library("image_lib", $config);
					$this->image_lib->resize();
					
					$config3["image_library"] = "gd2";
					$config3["source_image"] = $this->path."/".$d["file_name"];
					$config3["new_image"] = $this->path."/thumb/".$d["file_name"];
					$config3["create_thumb"] = TRUE;
					$config3["maintain_ratio"] = true;
					$config3["thumb_marker"] = false;
					$config3["width"] = 100;
					$config3["height"] = 100;
					$this->image_lib->initialize($config3); 
					$this->load->library("image_lib", $config3);
					$this->image_lib->resize();
					
					$file_images = $this->input->post("media_url");
					if (!$_FILES["images"]["name"])
					{
						$img = $file_images;
					}
					else
					{
						$this->load->helper("file");  
						$img = $d["file_name"];
						$im = $file_images;
						if(!empty($file_images)){
							$string = read_file($this->path."/".$im);
							$string2 = read_file($this->path."/thumb/".$im);
							if($file_images){
								unlink($this->path."/".$im);  
								unlink($this->path."/thumb/".$im);        
							}
							else
							{
							}
						}  
			}
			
			// <----/images-->

		}

                
		if($this->form_validation->run() === TRUE){

			
			$id = $this->input->post('media_id');			
			//process the form 
			if(!$id){
				$data = array(
					'media_name'       	=> $this->input->post('media_name'),
					'media_type'       	=> $this->input->post('media_type'),
					'media_url'       	=> base_url().$this->path.'/'.$this->clear_space($d["file_name"]),
					'filename'       	=> $this->clear_space($d["file_name"]),
					'media_category'     => 'sumberdaya', 
				);
				return $this->media_model->insert($data);
			}else{
				$data = array(
					'media_name'       	=> $this->input->post('media_name'),
					'media_type'       	=> $this->input->post('media_type'),
					'media_url'       	=> base_url().$this->path.'/'.$this->clear_space($img),
					'filename'       	=> $this->clear_space($img),
					'media_category'     => 'sumberdaya', 
				);
				return $this->media_model->update($id, $data);
			} 
		}
	}
	
	private function save_media_materi(){
		
		//set form validation
		$this->form_validation->set_rules('media_name', 'Nama Media', 'required');
		$this->form_validation->set_rules('media_type', 'Media Type', 'required');

		$id = $this->input->post('media_id');
		
		if(!$id){
		// <---- images -->
			$d["file_name"] = "";
			$file = $_FILES["images"]["name"];
			if(!empty($file)){ 
				$config2["upload_path"] = $this->path;
				$config2["allowed_types"] = "iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|wmv|avi|mpg|mpeg|mp3|mp4|3gp|gif|jpg|jpeg|png";
				$config2["max_size"] = "900000";
				$config2["file_ext"] = ""; 
				$file = $_FILES["images"]["name"]; 
				$filename = explode(".",$file);
				$config2["overwrite"] = TRUE;
				$config2["file_name"] = date('Ymd').'_'.$this->input->post("media_subject").'_'.$filename[0];
				
				$this->load->library("upload", $config2);
				$this->load->library("image_lib");
				if (!$this->upload->do_upload("images")){
					 $error = array("error" => $this->upload->display_errors());
					 //$this->load->view("delete", $error);  
				}else{
				$res = $this->upload->data();
				$file_ext     = $res["file_ext"];
				//$final_file_name = time().$file_ext;
				$img = $config2["file_name"].$file_ext;
				$d["file_name"] = $img;
				
				$config["image_library"] = "gd2";
				$config["source_image"] = $this->path."/".$d["file_name"];
				$config["create_thumb"] = TRUE;
				$config["maintain_ratio"] = true;
				$config["thumb_marker"] = FALSE;
				$config["width"] = 550;
				$config["height"] = 340;
				$this->image_lib->initialize($config); 
				$this->load->library("image_lib", $config);
				$this->image_lib->resize();
				
				$config3["image_library"] = "gd2";
				$config3["source_image"] = $this->path."/".$d["file_name"];
				$config3["new_image"] = $this->path."/thumb/".$d["file_name"];
				$config3["create_thumb"] = TRUE;
				$config3["maintain_ratio"] = true;
				$config3["thumb_marker"] = false;
				$config3["width"] = 100;
				$config3["height"] = 100;
				$this->image_lib->initialize($config3); 
				$this->load->library("image_lib", $config3);
				$this->image_lib->resize();
			}
		}
		// <----/images-->
		}
		else{


			// <---- images --> 
				$path = "media";
				$config2["upload_path"] = $this->path;
				$config2["allowed_types"] = "iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|wmv|avi|mpg|mpeg|mp3|mp4|3gp|gif|jpg|jpeg|png";
				$config2["max_size"] = "900000";
				$config2["file_ext"] = ""; 
				$file = $_FILES["images"]["name"]; 
				$filename = explode(".",$file);
				$config2["overwrite"] = TRUE;
				$config2["file_name"] = date('Ymd').'_'.$this->input->post("media_subject").'_'.$filename[0];
				
				$this->load->library("upload", $config2);
				$this->load->library("image_lib");
				
				$this->upload->do_upload("images");
					$res = $this->upload->data();
					$file_ext     = $res["file_ext"];
					//$final_file_name = time().$file_ext;
					$img = $config2["file_name"].$file_ext;
					$d["file_name"] = $img;
				
					$config["image_library"] = "gd2";
					$config["source_image"] = $this->path."/".$d["file_name"];
					$config["create_thumb"] = TRUE;
					$config["maintain_ratio"] = true;
					$config["thumb_marker"] = FALSE;
					$config["width"] = 550;
					$config["height"] = 340;
					$this->image_lib->initialize($config); 
					$this->load->library("image_lib", $config);
					$this->image_lib->resize();
					
					$config3["image_library"] = "gd2";
					$config3["source_image"] = $this->path."/".$d["file_name"];
					$config3["new_image"] = $this->path."/thumb/".$d["file_name"];
					$config3["create_thumb"] = TRUE;
					$config3["maintain_ratio"] = true;
					$config3["thumb_marker"] = false;
					$config3["width"] = 100;
					$config3["height"] = 100;
					$this->image_lib->initialize($config3); 
					$this->load->library("image_lib", $config3);
					$this->image_lib->resize();
					
					$file_images = $this->input->post("media_url");
					if (!$_FILES["images"]["name"])
					{
						$img = $file_images;
					}
					else
					{
						$this->load->helper("file");  
						$img = $d["file_name"];
						$im = $file_images;
						if(!empty($file_images)){
							$string = read_file($this->path."/".$im);
							$string2 = read_file($this->path."/thumb/".$im);
							if($file_images){
								unlink($this->path."/".$im);  
								unlink($this->path."/thumb/".$im);        
							}
							else
							{
							}
						}  
			}
			
			// <----/images-->

		}

                
		if($this->form_validation->run() === TRUE){

			
			$id = $this->input->post('media_id');			
			//process the form 
			if(!$id){
				$data = array(
					'media_name'       	=> $this->input->post('media_name'),
					'media_type'       	=> $this->input->post('media_type'),
					'media_url'       	=> base_url().$this->path.'/'.$this->clear_space($d["file_name"]),
					'filename'       	=> $this->clear_space($d["file_name"]),
					'media_lesson_id'     => $this->input->post('media_lesson_id'), 
					'media_course_id'     => $this->input->post('media_course_id'), 
				);
				return $this->media_model->insert($data);
			}else{
				$data = array(
					'media_name'       	=> $this->input->post('media_name'),
					'media_type'       	=> $this->input->post('media_type'),
					'media_url'       	=> base_url().$this->path.'/'.$this->clear_space($img),
					'filename'       	=> $this->clear_space($img),
					'media_lesson_id'     => $this->input->post('media_lesson_id'), 
					'media_course_id'     => $this->input->post('media_course_id'),
				);
				return $this->media_model->update($id, $data);
			} 
		}
	}

	public function clear_space($string){
		return str_replace(' ', '_', $string);
	}
}