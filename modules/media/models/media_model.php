<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * media Model
 * @module	media
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class media_model extends MY_Model {

    protected $table        = 'tbl_media';
    protected $key          = 'media_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='', $category)
	{
		if($search != '')
		{
			return $this->db->select('*')
							->from($this->table)
							->join('tbl_subject','tbl_subject.subject_id = tbl_media.media_subject', 'left')
							//->where('tbl_customer.deleted','0')
							->where('tbl_media.deleted','0')
							->where('tbl_media.media_category',$category)
							->like('media_name',$search)
							->limit($limit,$offset)
							->order_by('media_name','asc')
							->get()
							->result();
		}else
		{		
			return $this->db->select('*')
						->from($this->table)
						->join('tbl_subject','tbl_subject.subject_id = tbl_media.media_subject', 'left')
						->where('tbl_media.deleted','0')
						->where('tbl_media.media_category',$category)
						->limit($limit,$offset)
						->order_by('media_name','asc')
						->get()
						->result();
		}
	}
	public function get_all_materi($limit, $offset, $search='', $category)
	{
		if($search != '')
		{
			return $this->db->select('*')
							->from($this->table)
							->join('tbl_course','tbl_course.course_id = tbl_media.media_course_id', 'left')
							->join('tbl_lesson','tbl_lesson.lesson_id = tbl_media.media_lesson_id', 'left')
							//->where('tbl_customer.deleted','0')
							->where('tbl_media.deleted','0')
							->where('tbl_media.media_category',$category)
							->like('media_name',$search)
							->limit($limit,$offset)
							->order_by('media_name','asc')
							->get()
							->result();
		}else
		{		
			return $this->db->select('*')
						->from($this->table)
							->join('tbl_course','tbl_course.course_id = tbl_media.media_course_id', 'left')
							->join('tbl_lesson','tbl_lesson.lesson_id = tbl_media.media_lesson_id', 'left')
						->where('tbl_media.deleted','0')
						->where('tbl_media.media_category',$category)
						->limit($limit,$offset)
						->order_by('media_name','asc')
						->get()
						->result();
		}
	}
	
	public function count_all_sumberdaya($search,$category)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('deleted','0')
						->where('tbl_media.media_category',$category)
						->like('media_name',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	
	public function count_all_materi($search,$category)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->join('tbl_course','tbl_course.course_id = tbl_media.media_course_id', 'left')
						->join('tbl_lesson','tbl_lesson.lesson_id = tbl_media.media_lesson_id', 'left')
						->where('tbl_media.deleted','0')
						->where('tbl_media.media_category',$category)
						->like('media_name',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
		
	public function get_media($id)
	{
		$this->db	->select('*')
					->where('tbl_media.deleted', 0)
					->where('media_id', $id);
        return $this->db->get($this->table);    
    }

    public function get_media_file($id)
	{
		return $this->db->select('*')
					->from($this->table)
					->where('tbl_media.deleted', 0)
					->where('media_id', $id)
					->get()
					->row();
    }
	
		
	public function get_all_media()
	{
		$this->db	->select('*')
					->where('tbl_media.deleted', 0);
        return $this->db->get('tbl_media');    
    }
	
	public function get_all_subject()
	{
		$this->db	->select('*')
					->where('tbl_subject.deleted', 0)
					->order_by('subject_name','asc');
        return $this->db->get('tbl_subject');    
    }
	
	public function get_all_lesson()
	{
		$this->db	->select('*')
					->join('tbl_course','tbl_course.course_id = tbl_lesson.lesson_course_id')
					->where('tbl_lesson.deleted', 0)
					->order_by('course_name','asc')
					->order_by('lesson_name','asc');
        return $this->db->get('tbl_lesson');    
    }

	public function getCourse()
	{
		return $this->db->select("*")
			->from('tbl_course')
			->order_by('course_id','asc')
			->get()
			->result();
	}
}