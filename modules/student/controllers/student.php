<?php

class Student extends Front_Controller{
	
	private $per_page 	= '15';
	private $title 		= 'Student';
	private $module 	= 'student';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect($this->module.'/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->user_model->count_all($this->input->post('q'));
			
			//pagination
			$config['base_url']     = site_url($this->module.'/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = 30; 
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(3);
			
			$customer = $this->user_model->get_all($this->input->post('q'));
			
			
			$this->template	->set('menu_title', 'Pengaturan  Pelajar')
							->set('menu_database', 'active')
							->set('material_total',$config['total_rows'])
							->set('user', $customer)
							->set('no', $no)
							->set('config', $config)
							->build('student');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function create(){
		if($this->save_user()){
			$this->session->set_flashdata('message', 'success|Data berhasil ditambahkan');
			redirect($this->module.'/');
		}

		$province = $this->user_model->getProvince();
		$city     = $this->user_model->getCity();
		$school_grade = $this->user_model->getGrade();
		$school = $this->user_model->getSchool();
		//ug student
		$userlevel = 2;

		$this->template	->set('menu_title', 'Pengaturan Pelajar')
						->set('menu_subtitle', 'Formulir Penambahan Pelajar')
						->set('menu_database', 'active')
						->set('user_level', $userlevel)
						->set('province', $province)
						->set('city', $city)
						->set('school_grade', $school_grade)
						->set('school', $school)
						->build('student_create');
	}
	
	public function edit($id){
		$id =  $this->uri->segment(3);

		if($this->save_user()){
			$this->session->set_flashdata('message', 'success|Data berhasil diubah');
			redirect('student/');
		}

		$province = $this->user_model->getProvince();
		$city     = $this->user_model->getCity();
		$school_grade = $this->user_model->getGrade();
		$school = $this->user_model->getSchool();

		//Get Specific customer
		$data = $this->user_model->get_user($id)->result();
		$data = $data[0];

		foreach($school as $row){
			if($row->school_id == $data->user_school){
				$user_school_city = $row->school_city;
				$user_school_province = $row->school_province;
			}
		}
		
		$this->template	->set('data', $data)
						->set('province', $province)
						->set('city', $city)
						->set('school_grade', $school_grade)
						->set('school', $school)
						->set('user_school_city', $user_school_city)
						->set('user_school_province', $user_school_province)
						->set('action', 'edit')
						->set('menu_title', 'Pengaturan Pelajar')
						->set('menu_subtitle', 'Formulir Pengaturan Data Pelajar')
						->set('menu_database', 'active')
						->build('student_edit');
	}
	public function view($id){
		$id =  $this->uri->segment(3);
		
		//Get Specific customer
		$data = $this->user_model->get_user($id)->result();
		$data = $data[0];

		$province = $this->user_model->getProvince();
		$city     = $this->user_model->getCity();
		$school_grade = $this->user_model->getGrade();
		$school = $this->user_model->getSchool();

		foreach($school as $row){
			if($row->school_id == $data->user_school){
				$user_school_city = $row->school_city;
				$user_school_province = $row->school_province;
			}
		}
		
		$this->template	->set('data', $data)
						->set('province', $province)
						->set('city', $city)
						->set('school_grade', $school_grade)
						->set('school', $school)
						->set('user_school_city', $user_school_city)
						->set('user_school_province', $user_school_province)
						->set('menu_title', 'Lihat Pelajar')
						->set('menu_subtitle', 'Lihat Pelajar')
						->set('menu_database', 'active')
						->build('student_view');
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(3);
		if($this->user_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Data berhasil dihapus');
			redirect($this->module.'/browse');
			exit;
		}
	}	
	
	private function save_user(){
		
		//set form validation
		$this->form_validation->set_rules('fullname', 'Nama Lengkap', 'required');
		$this->form_validation->set_rules('user_email', 'Email', 'required');

	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('user_id');
			
			//process the form
			$data = array(
					'username'       	=> $this->input->post('user_email'),
					'fullname'       	=> $this->input->post('fullname'),
					'user_email'       	=> $this->input->post('user_email'),
					'user_level'       	=> '2',
					'user_mobile'       => $this->input->post('user_mobile'),
					'user_phone'       => $this->input->post('user_phone'),
					'user_address'      => $this->input->post('user_address'),
					'user_city'       	=> $this->input->post('user_city'),
					'user_province'     => $this->input->post('user_province'),
					'user_zipcode'      => $this->input->post('user_zipcode'),
					'user_parent_name'      => $this->input->post('user_parent_name'),
					'user_parent_mobile'    => $this->input->post('user_parent_mobile'),
					'user_parent_email'     => $this->input->post('user_parent_email'),
					'user_school'      		=> $this->input->post('user_school'),
					'user_school_province'  => $this->input->post('user_school_province'),
					'user_school_city'      => $this->input->post('user_school_city'),
					'user_school_grade'      => $this->input->post('user_school_grade'),
			);
				
			if(!$id){
				$data['password'] = md5($this->input->post('password'));
				return $this->user_model->insert($data);
			}else{
				return $this->user_model->update($id, $data);
			} 
		}
	}

}