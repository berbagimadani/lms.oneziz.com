<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * subject Model
 * @module	subject
 * @package	Oneziz
 * @author 	fikriwirawan
 * @since	07 Feb 2015
 */
 
class test_model extends MY_Model {

    protected $table        = 'tbl_quiz';
    protected $key          = 'quiz_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    
    public function __construct()
	{
        parent::__construct();
    }    
	
		
	public function get_all($limit, $offset, $search='')
	{
		if($search != '')
		{
			return $this->db->select('*')
							->from($this->table)
							->join('tbl_subject','tbl_subject.subject_id = tbl_quiz.quiz_subject_id')
							//->where('tbl_customer.deleted','0')
							->where('tbl_quiz.deleted','0')
							->like('quiz_question',$search)
							->limit($limit,$offset)
							->order_by('quiz_question','asc')
							->get()
							->result();
		}else
		{		
			return $this->db->select('*')
						->from($this->table)
						->join('tbl_subject','tbl_subject.subject_id = tbl_quiz.quiz_subject_id')
						->where('tbl_quiz.deleted','0')
						->limit($limit,$offset)
						->order_by('quiz_question','asc')
						->get()
						->result();
		}
	}
	
	public function count_all($search)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('deleted','0')
						->like('quiz_question',$search)
						//->or_like('content',$search)
						->get()
						->row()
						->numrows;
	}
	
		
	public function get_quiz($id)
	{
		$this->db	->select('*')
					->where('tbl_quiz.deleted', 0)
					->where('quiz_id', $id);
        return $this->db->get($this->table);    
    }
	
		
	public function get_all_quiz()
	{
		$this->db	->select('*')
					->where('tbl_quiz.deleted', 0);
        return $this->db->get('tbl_quiz');    
    }
	
	public function get_all_subject()
	{
		$this->db	->select('*')
					->where('tbl_subject.deleted', 0)
					->order_by('subject_name','asc');
        return $this->db->get('tbl_subject');    
    }
}