<?php

class Test extends Front_Controller{
	
	private $per_page 	= '30';
	private $title 		= 'kuis test';
	private $module 	= 'test';
	
	
	public function __construct(){
		parent::__construct();
		$this->load->model('test_model');
		$this->load->library('pagination');	
	}
	
	public function index($page='0'){
		if($this->session->userdata('logged_in'))
		{
			redirect('test/browse');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function browse($page='0'){
		if($this->session->userdata('logged_in'))
		{
			$total_rows = $this->test_model->count_all($this->input->post('q'));
			
			//pagination
			$config['base_url']     = site_url('test/browse');
			$config['total_rows']   = $total_rows;
			$config['per_page']     = $this->per_page; 
			$config['uri_segment']  = 3;
			$config['suffix'] 		= '?' . http_build_query($_GET, '', "&");
			$config['first_url'] 	= $config['base_url'] . $config['suffix'];
			$config['num_links'] = 2;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a href="#"><b>';
			$config['cur_tag_close'] = '</b></a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$no =  $this->uri->segment(3);
			
			$data = $this->test_model->get_all($config['per_page'], $no, $this->input->post('q'));		
			
			
			$this->template	->set('menu_title', 'Pengaturan Tes')
							->set('menu_environment', 'active')
							->set('total_rows',$config['total_rows'])
							->set('data', $data)
							->set('no', $no)
							->set('config', $config)
							->build('test');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
	}
	
	public function create(){
		if($this->save_quiz()){
			$this->session->set_flashdata('message', 'success|Success add new record');
			redirect('test');
		}
					
			
		$subject = $this->test_model->get_all_subject()->result();
		
		$this->template	->set('menu_title', 'Pengaturan Tes')
						->set('subject', $subject)
						->set('menu_subtitle', 'Form Penambahan Tes')
						->build('test_edit');		
	}
	
	public function edit($id){
		$id =  $this->uri->segment(3);
		
		if($this->save_quiz()){
			$this->session->set_flashdata('message', 'success|Success edit record');
			redirect('test');
		}
		
		//Get Specific Data
		$data = $this->test_model->get_quiz($id)->result();
		$data = $data[0];
		
		$subject  = $this->test_model->get_all_subject()->result();
		
		$this->template	->set('data', $data)
						->set('subject', $subject )
						->set('menu_title', 'Pengaturan Tes')
						->set('menu_subtitle', 'Form Pengaturan  Tes')
						->build('test_edit');		
	}
	
	public function view($id){
		$id =  $this->uri->segment(3);
		
		//Get Specific data
		$data = $this->test_model->get_quiz($id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'View')
						->set('menu_subtitle', 'View')
						->build('test_edit');	
	}
	
	public function delete($id = '0'){
		$id =  $this->uri->segment(3);
		if($this->test_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Data Berhasil Dihapus');
			redirect('test');
			exit;
		}
	}	
	
	private function save_quiz(){
		
		//set form validation
		$this->form_validation->set_rules('quiz_question', 'pertanyaan', 'required');
		$this->form_validation->set_rules('quiz_subject_id', 'Mata Pelajaran', 'required');
		$this->form_validation->set_rules('quiz_answer_a', 'Jawaban Pertanyaan A', 'required');
		$this->form_validation->set_rules('quiz_answer_b', 'Jawaban Pertanyaan B', 'required');
		$this->form_validation->set_rules('quiz_answer_c', 'Jawaban Pertanyaan C', 'required');
		$this->form_validation->set_rules('quiz_answer_d', 'Jawaban Pertanyaan D', 'required');
		$this->form_validation->set_rules('quiz_answer_e', 'Jawaban Pertanyaan E', 'required');
		$this->form_validation->set_rules('quiz_answer', 'Jawaban ', 'required');
		$this->form_validation->set_rules('quiz_solution', 'Quiz Pembahasan', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('quiz_id');			
			
			//process the form
			$data = array(
				'quiz_question'     => $this->input->post('quiz_question'),
				'quiz_subject_id'   => $this->input->post('quiz_subject_id'),
				'quiz_answer_a'		=> $this->input->post('quiz_answer_a'),
				'quiz_answer_b'		=> $this->input->post('quiz_answer_b'),
				'quiz_answer_c'		=> $this->input->post('quiz_answer_c'),
				'quiz_answer_d'		=> $this->input->post('quiz_answer_d'),
				'quiz_answer_e'		=> $this->input->post('quiz_answer_e'),
				'quiz_answer'		=> $this->input->post('quiz_answer'),
				'quiz_solution'		=> $this->input->post('quiz_solution'),
			);
				
			if(!$id){
				return $this->test_model->insert($data);
			}else{
				return $this->test_model->update($id, $data);
			} 
		}
	}
}